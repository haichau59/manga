<?php
/**
 * Controller.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:55 AM
 */
define('CHECK_SECURITY', '1');
define('ESPECIAL_API_KEY', '3sp3c1al_ap1_k3y');
define('IV_KEY', 'CAdvS3cur1ty@IV$');

class Controller extends CController {

	public $breadcrumbs = array();
	public $menu = array();
	
	protected $_api_key = "";
	protected $_device_id = "";
	protected $_device_token = "";
	protected $_sessionID = "";
	protected $_device_type_id = ""; // tam thoi 1 - android, 2 - iOS, 3 - Web 
	protected $_app_version_code = "";
	protected $_clientUser = NULL;
	protected $_isTablet = 0; // 0 - not tablet, 1 - is tablet
	protected $_username = "";
	protected $_password = "";
	protected $_client_ip;
	protected $_subscriber = NULL;
	protected $_format = 'json';
	protected $_time = 0;
	
	
	/**
	 *
	 * @param type $error_no
	 * @param type $error_code
	 * @param type $message
	 */
	public function responseError($error_no, $error_code, $message) {
		//        echo($error_code." *** ".$message);
		if($this->_format === 'xml'){
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";
	
	
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("app_version_code", $this->_app_version_code?$this->_app_version_code:1));
			$root->appendChild($xmlDoc->createElement("device_id", $this->_device_type_id?$this->_device_type_id:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", $error_no));
			$root->appendChild($xmlDoc->createElement("error_code", $error_code));
			$root->appendChild($xmlDoc->createElement("error_message", CHtml::encode($message)));
	
			echo $xmlDoc->saveXML();
		}
		else {
			header('Content-type: application/json; charset=utf-8');
	
			$arr['session'] = $this->_sessionID?$this->_sessionID:"";
			$arr['client_app_code'] = $this->_app_version_code?$this->_app_version_code:1;
			$arr['device_type_id'] = $this->_device_type_id?$this->_device_type_id:"";
			$arr['user_name'] = $this->_username?$this->_username:"";
			$arr['action'] = $this->action->id;
			$arr['error_no'] = $error_no;
			$arr['error_code'] = $error_code;
			$arr['error_message'] = $message;
			$content = CJSON::encode($arr);
			echo $content;
		}
		Yii::app()->end();
	}
}
