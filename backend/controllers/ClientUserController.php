<?php

class ClientUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
				array('allow', // allow all users to perform 'index' and 'view' actions
						'actions' => array('index', 'view'),
						'users' => array('*'),
				),
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions' => array('create', 'update'),
						'users' => array('@'),
				),
				array('allow', // allow admin user to perform all actions
						//'actions' => array('create', 'delete'),
						//TODO: authen, author
						//'users' => array('admin'),
				),
				array('deny', // deny all users
						'users' => array('*'),
				),
		);
	}

	public function beforeAction($action) {
		//        echo($action->id);
		$this->_format = (isset($_REQUEST['format']) && !empty($_REQUEST['format']))?$_REQUEST['format']:"json";

		$this->_sessionID = (isset($_REQUEST['session']) && !empty($_REQUEST['session']))?$_REQUEST['session']:"123456";
		//         $this->_username = (isset($_REQUEST['username']) && !empty($_REQUEST['username']))?$_REQUEST['username']:"";
		$this->_username = (isset($_REQUEST['device_id']) && !empty($_REQUEST['device_id']))?$_REQUEST['device_id']:-1;//su dung device_id lam user_name, password mac dinh la 123456
		$this->_clientUser = ClientUser::getByUsername($this->_username);
		if($this->_clientUser == NULL) {
			$this->responseError(1,1, "Cannot create clientUser ".$this->_username);
		}
		$this->_device_type_id = (isset($_REQUEST['device_type_id']) && !empty($_REQUEST['device_type_id']))?$_REQUEST['device_type_id']:1;
		$this->_app_version_code = (isset($_REQUEST['app_version_code']) && !empty($_REQUEST['app_version_code']))?$_REQUEST['app_version_code']:1;
		$this->_api_key = (isset($_REQUEST['api_key']) && !empty($_REQUEST['api_key']))?$_REQUEST['api_key']:"***";
		//         $this->_api_key = ESPECIAL_API_KEY;
		if($this->_api_key == ESPECIAL_API_KEY) {
			return parent::beforeAction($action);
		}

		$headerUserAgent = '';
		$headerSync = '';
		$headerTime = '';
// 		foreach (getallheaders() as $name => $value) {
// 			if($name == 'User-Agent') {
// 				$headerUserAgent = $value;
// 			}
// 			else if($name == 'sync') {
// 				$headerSync = $value;
// 			}
// 			else if($name == 'time') {
// 				$this->_time = $value;
// 			}
// 		}
// 		if(!$this->checkSecurity($headerUserAgent, $headerSync)) {
// 			$this->responseError(1,1, "Check security failed!");
// 		}

		return parent::beforeAction($action);
	}

	private function checkSecurity($userAgent, $sync) {
		if(CHECK_SECURITY == '0') return true;
		$md5_padding = "S3cur!ty@cCr4pt";
		$time = time();
		try {
			if($this->_api_key == '***') {
				throw  new Exception('api_key is empty');
			}
			if(strpos($userAgent, $this->_api_key) === FALSE) {
				throw  new Exception('api_key invalid');
			}

			// 			md5(api_key + time + md5_padding)
			$localSync = md5($this->_api_key . $this->_time . $md5_padding);
			// 			echo $localSync;
			if($localSync != $sync) {
				throw  new Exception('sync invalid');
			}
		} catch (Exception $e) {
			$this->responseError(1,1,"check security failed! - ".$e->getMessage());
			// 			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}

		return true;
	}

	public function actionLogin() {
		$subscriber = ClientUser::model()->findByAttributes(array("user_name"=>$this->_username));
		if($subscriber == NULL) {
			$this->responseError(1, 1, "Unknown username and/or password");
		}
		// 		if($subscriber->status == 0) {
		// 			$this->responseError(1, 1, "Tài khoản chưa được kích hoạt. Bạn vào mail $subscriber->email để kích hoạt.");
		// 		}

		//$session = SubscriberSession::model()->findByAttributes(array("subscriber_id"=>$subscriber->id));
		//if($session != NULL) {

		$pass = (isset($_REQUEST['password']) && !empty($_REQUEST['password']))?$_REQUEST['password']:"";
		if (empty ($pass) || empty ($this->_username)) {
			$this->responseError(1, 1, "Unknown username and/or password");
		}
		$loginResult = ClientAuthen::Login($this->_username, $pass);
		if ($loginResult != ClientAuthen::ERROR_NONE) {
			$this->responseError(1, 1, ClientAuthen::$errorMessage[$loginResult]);
		}
		else {
			$this->_sessionID = ClientAuthen::$sessionID;
			$this->responseError(0, 0, "Login successfully");
		}
	}

	public function actionLogout() {
		$res = ClientAuthen::LogoutSession($this->_username, $this->_sessionID);
		if ($res != ClientAuthen::ERROR_NONE) {
			$this->responseError(1, 1, ClientAuthen::$errorMessage[$res]);
		}
		else {
			$this->responseError(0, 0, "Logout successfully");
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
				'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ClientUser;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ClientUser']))
		{
			$model->attributes=$_POST['ClientUser'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
				'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ClientUser']))
		{
			$model->attributes=$_POST['ClientUser'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
				'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ClientUser');
		$this->render('index',array(
				'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ClientUser('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ClientUser']))
			$model->attributes=$_GET['ClientUser'];

		$this->render('admin',array(
				'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ClientUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='client-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionRegisterUser() {
		//        $username = isset($_GET["username"])?$_GET["username"]:"";
		$email = isset($_GET["email"])?$_GET["email"]:"";
		$username = $email;
		$password = isset($_GET["password"])?$_GET["password"]:"";
		if($username == "") {
			$this->responseError(1,1, "Địa chỉ email không hợp lệ.");
		}
		if($password == "") {
			$password = CUtils::randomString(10);
		}
		$actionStr = "Register new subscriber ";
		$valid = true; //TODO check user agent de set lai gia tri nay neu user ko fai cua vtvplus

		if(!$valid) {
			$emailObj = new Email;
			$emailObj->email =  $email;
			$emailObj->create_date = new CDbExpression("NOW()");
			$emailObj->save();
		}

		$tmpSubscriber = ClientUser::model()->findByAttributes(array("email"=>$email));
		if($tmpSubscriber != NULL) {
			$this->responseError(1,1,"Địa chỉ email $email đã được sử dụng. Xin bạn vui lòng kiểm tra lại.");
		}

		$full_name = isset($_GET["full_name"])?$_GET["full_name"]:"";
		$sex = isset($_GET["sex"])?$_GET["sex"]:"";
		$avatar_url = isset($_GET["avatar_url"])?$_GET["avatar_url"]:"";
		$yahoo_id = isset($_GET["yahoo_id"])?$_GET["yahoo_id"]:"";
		$skype_id = isset($_GET["skype_id"])?$_GET["skype_id"]:"";
		$google_id = isset($_GET["google_id"])?$_GET["google_id"]:"";
		$zing_id = isset($_GET["zing_id"])?$_GET["zing_id"]:"";
		$facebook_id = isset($_GET["facebook_id"])?$_GET["facebook_id"]:"";
		$phone_number = isset($_GET["phone_number"])?$_GET["phone_number"]:"";
		//        $status = 0; // luc moi dang ky status van =0, khi noi goi api activateSubscriber thi moi set = 1
		$status = 1; //TODO; tam thoi ko can verification nen dat status=1 // 10-03-2013
		$status_log = CUtils::randomString(10);
		$registerResult = 0;
		$registerResult = $this->registerSubscriber($username, $password, $status, $email, $full_name, $sex, $avatar_url, $yahoo_id, $skype_id, $facebook_id, $google_id, $zing_id, $phone_number, $status_log);// neu co verification thi xoa dong nay. Viec nay dc thuc hien sau khi gui email xong
		if($registerResult == 0) {
			if(!$valid) {
				$justCreatedSubscriber = ClientUser::model()->findByAttributes(array("email"=>$email));
				$justCreatedSubscriber->status = 0;
			}
			$this->responseSubscriber($username, $actionStr, $password); //TODO hien tai chua verification, khi no verify thi xoa dong nay, dung doan code ben duoi
		}
		else {
			$this->responseError(1,1,"Error when register user ".$username);
		}
	}

	public function actionActivate() {
		$email = isset($_GET["email"])?$_GET["email"]:"";
		$token = isset($_GET["token"])?$_GET["token"]:"";
		$tmpSubscriber = ClientUser::model()->findByAttributes(array("email"=>$email));
		if($tmpSubscriber == NULL) {
			$this->responseError(1,1,"User with email ".$email." is not existed.");
		}
		if($tmpSubscriber->skype_id != $token) { //dung tam skype_id de luu status_log
			$this->responseError(1,1,"Token is invalid.");
		}
		$tmpSubscriber->status = 1;
		$tmpSubscriber->update();
		$this->responseError(0,0,"Bạn đã kích hoạt thành công. Dùng tài khoản này để đăng nhập vào trang manga.vn hoặc ứng dụng manga (Android hoặc iOS)");
	}

	public function responseSubscriber($username, $actionStr, $password = '') {
		header("Content-type: text/xml; charset=utf-8");
		$xmlDoc = new DOMDocument();
		$xmlDoc->encoding = "UTF-8";
		$xmlDoc->version = "1.0";

		//TODO: authen, session, error handle
		$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
		$root->appendChild($xmlDoc->createElement("action", $this->action->id));
		$root->appendChild($xmlDoc->createElement("username", $username));
		$root->appendChild($xmlDoc->createElement("password", $password));
		$root->appendChild($xmlDoc->createElement("error_code", "0"));
		$root->appendChild($xmlDoc->createElement("message", $actionStr." ".$username." successfully!!!"));

		echo $xmlDoc->saveXML();
	}

	public function registerSubscriber($username,$password,$status,$email,$full_name,$sex,$avatar_url,$yahoo_id,$skype_id,$facebook_id,$google_id,$zing_id,$phone_number='', $status_log='') {
		$newSubscriber = new ClientUser();
		$newSubscriber->user_name = $username;
		$newSubscriber->password = md5($password);
		$newSubscriber->status = $status;
		$newSubscriber->subscriber_number = $phone_number;
		$newSubscriber->create_date = new CDbExpression("NOW()");

		$newSubscriber->email = $email;
		$newSubscriber->full_name = $full_name;
		$newSubscriber->sex = $sex;
		$newSubscriber->avatar_url = $avatar_url;
		$newSubscriber->yahoo_id = $yahoo_id;
		$newSubscriber->google_id = $google_id;
		$newSubscriber->zing_id = $zing_id;
		$newSubscriber->facebook_id = $facebook_id;
		$newSubscriber->skype_id = $status_log; //dung tam skype_id de luu status_log
		//        echo "username = ".$newSubscriber->user_name." - password = ".$newSubscriber->password;
		if(!$newSubscriber->save()) {
			//            print_r($newSubscriber->getErrors());
			$this->responseError(1,1,"Cannot register subscriber with username ".$username);
		}
		else {
			return 0;
		}
	}

	public function updateSubscriber($newSubscriber,$email,$full_name,$sex,$birthday,$avatar_url,$yahoo_id,$skype_id,$facebook_id,$google_id,$zing_id, $phone_number) {
		/* @var newSubscriber Subscriber */
		//        $newSubscriber->user_name = $username;
		//        $newSubscriber->password = md5($password);
		//        $newSubscriber->status = 1;
		if (!empty ($email)) $newSubscriber->email = $email;
		if (!empty ($full_name)) $newSubscriber->full_name = $full_name;
		if (!empty ($sex)) $newSubscriber->sex = $sex;
		if (!empty ($birthday)) $newSubscriber->birthday = $birthday;
		if (!empty ($birthday)) $newSubscriber->avatar_url = $avatar_url;
		if (!empty ($yahoo_id)) $newSubscriber->yahoo_id = $yahoo_id;
		if (!empty ($skype_id)) $newSubscriber->skype_id = $skype_id;
		if (!empty ($google_id)) $newSubscriber->google_id = $google_id;
		if (!empty ($email)) $newSubscriber->zing_id = $zing_id;
		if (!empty ($facebook_id)) $newSubscriber->facebook_id = $facebook_id;
		if (!empty ($phone_number)) $newSubscriber->subscriber_number = $phone_number;
		//        echo "username = ".$newSubscriber->user_name." - password = ".$newSubscriber->password;
		if(!$newSubscriber->update()) {
			//            print_r($newSubscriber->getErrors());
			$this->responseError(1,1,"Cannot update info for subscriber with username ".$username);
		}
		else {
			return 0;
		}
	}

	public function actionChangeUsername() {
		$newUsername = isset($_GET["newUsername"])?$_GET["newUsername"]:"";
		$username = isset($_GET["username"])?$_GET["username"]:"";
		$password = isset($_GET["password"])?$_GET["password"]:"";

		$loginResult = ClientAuthen::Login($username, $password);
		if ($loginResult != ClientAuthen::ERROR_NONE) {
			$this->responseError(1, 1, "Your username / password (".$username."/".$password.") is not valid!");
		}

		$subscriber = ClientUser::model()->findByAttributes(array("user_name"=>$username));
		$tmpSubscriber = ClientUser::model()->findByAttributes(array("user_name"=>$newUsername));
		if($tmpSubscriber != NULL) {
			$this->responseError(1,1,"username ".$newUsername." exists. Please use another username!");
		}
		$subscriber->user_name = $newUsername;
		if(!$subscriber->update()) {
			print_r($subscriber->getErrors());
			$this->responseError(1,1,"Cannot change username of subscriber $username");
		}

		header("Content-type: text/xml; charset=utf-8");
		$xmlDoc = new DOMDocument();
		$xmlDoc->encoding = "UTF-8";
		$xmlDoc->version = "1.0";

		//TODO: authen, session, error handle
		$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
		$root->appendChild($xmlDoc->createElement("action", $this->action->id));
		$root->appendChild($xmlDoc->createElement("error_code", "0"));
		$root->appendChild($xmlDoc->createElement("message", "Change user $username : new username = ".$newUsername." successfully"));

		echo $xmlDoc->saveXML();
	}

	public function actionFacebookLogin() {
		$access_token = isset($_GET['access_token'])?$_GET['access_token']:-1;
		$email = isset($_GET['email'])?$_GET['email']:NULL;
		$birthday = isset($_GET['birthday'])?$_GET['birthday']:NULL;
		if($access_token == -1) {
			$this->responseError(1,1,"Facebook access_token is empty");
		}
		// Attempt to query the graph:
		$graph_url = "https://graph.facebook.com/me?"
				. "access_token=" . $access_token;
		//        $this->redirect($graph_url);
		$response = $this->curl_get_file_contents($graph_url);
		$decoded_response = json_decode($response);
		//        print_r($decoded_response);
		//        echo("success" . $decoded_response->name);
		//        echo($access_token);
		//        return;
		//Check for errors
		if ($decoded_response->error != NULL) {
			// check to see if this is an oAuth error:
			if ($decoded_response->error->type== "OAuthException") {
				// Retrieving a valid access token.
				//                $dialog_url= "https://www.facebook.com/dialog/oauth?"."client_id=" . $app_id."&redirect_uri=" . urlencode($my_url);
				$errMesg = $decoded_response->error->message;
				$this->responseError(1,1,$errMesg);
			}
			else {
				$errMesg  = "other error has happened";
				$this->responseError(1,1,$errMesg);
			}
		}
		else {
			$facebook_id = $decoded_response->id;
			$f_subscriber = ClientUser::model()->findByAttributes(array("facebook_id"=>$facebook_id));
			$facebook_acccount = FacebookAccount::model()->findByAttributes(array("facebook_id"=>$facebook_id));
			if($facebook_acccount == NULL) {
				/* @var $facebook_acccount FacebookAccount */
				$facebook_acccount = new FacebookAccount();
				$facebook_acccount->facebook_id = $facebook_id;
				$facebook_acccount->user_name = $decoded_response->username;
				$facebook_acccount->full_name = $decoded_response->name;
				$facebook_acccount->gender = $decoded_response->gender;
				$facebook_acccount->link = $decoded_response->link;
				$facebook_acccount->timezone = $decoded_response->timezone;
				$facebook_acccount->locale = $decoded_response->locale;
				$facebook_acccount->status = 1;
				$facebook_acccount->email = $email;
				$facebook_acccount->birthday = $birthday;
				$facebook_acccount->save();
			}
			if($f_subscriber == NULL) {
				if($decoded_response->username != NULL) {
					$username = $decoded_response->username;
				}
				else {
					$username = $decoded_response->id;
				}
				$password = "123";
				$status = 1;
				$email = "";
				$full_name = $decoded_response->name;
				if($decoded_response->gender == "male") {
					$sex = 1;
				}
				else {
					$sex = 0;
				}
				$avatar_url = "";
				$yahoo_id = "";
				$google_id = ""; $zing_id = "";
				$registerResult = $this->registerSubscriber($username, $password, $status, $email, $full_name, $sex, $avatar_url, $yahoo_id, $skype_id, $facebook_id, $google_id, $zing_id);
				if($registerResult == 0) {
					$this->login($username,$password,"Facebook");
				}
			}
			else {
				/* @var $f_subscriber Subscriber */
				$username = $f_subscriber->user_name;
				$password = $f_subscriber->password;
				$this->login($username,$password,"Facebook");
			}
		}
	}

	public function actionUpdateSubscriberInfo() {
		//        $username = isset($_GET["username"])?$_GET["username"]:"";
		$email = isset($_GET["email"])?$_GET["email"]:"";
		//        $password = isset($_GET["password"])?$_GET["password"]:"";
		//        $tmpSubscriber = ClientUser::model()->findByAttributes(array("email"=>$email));
		$full_name = isset($_GET["full_name"])?$_GET["full_name"]:"";
		$sex = isset($_GET["sex"])?$_GET["sex"]:"";
		$birthday = isset($_GET["birthday"])?$_GET["birthday"]:"";
		$avatar_url = isset($_GET["avatar_url"])?$_GET["avatar_url"]:"";
		$yahoo_id = isset($_GET["yahoo_id"])?$_GET["yahoo_id"]:"";
		$skype_id = isset($_GET["skype_id"])?$_GET["skype_id"]:"";
		$google_id = isset($_GET["google_id"])?$_GET["google_id"]:"";
		$zing_id = isset($_GET["zing_id"])?$_GET["zing_id"]:"";
		$facebook_id = isset($_GET["facebook_id"])?$_GET["facebook_id"]:"";
		$phone_number = isset($_GET["phone_number"])?$_GET["phone_number"]:"";

		$subscriber = $this->_clientUser;
		/* @var $subscriber Subscriber */
		$updateResult = $this->updateSubscriber($subscriber, $email, $full_name, $sex, $birthday, $avatar_url, $yahoo_id, $skype_id, $facebook_id, $google_id, $zing_id, $phone_number);

		if($updateResult == 0) {
			$actionStr = "Update info for ";
			$this->responseSubscriber($username, $actionStr);
		}
		else {
			$this->responseError(1,1,"Update information for ".$username." unsuccessfully");
		}
	}

	public function actionGetSubscriberInfo() {
		header("Content-type: text/xml; charset=utf-8");
		$xmlDoc = new DOMDocument();
		$xmlDoc->encoding = "UTF-8";
		$xmlDoc->version = "1.0";

		//TODO: authen, session, error handle
		$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
		$root->appendChild($xmlDoc->createElement("session", $this->_sessionID));
		$root->appendChild($xmlDoc->createElement("action", $this->action->id));
		$root->appendChild($xmlDoc->createElement("error_code", 0));
		$result = $root->appendChild($xmlDoc->createElement("result"));

		/* @var $subscriber Subscriber */
		$subscriber = $this->_clientUser;
		//        $username, $password, $status, $email, $full_name, $sex, $avatar_url, $yahoo_id, $skype_id, $facebook_id, $google_id, $zing_id
		$result->appendChild($xmlDoc->createElement("id"))
		->appendChild($xmlDoc->createTextNode($subscriber->id));
		$result->appendChild($xmlDoc->createElement("username"))
		->appendChild($xmlDoc->createTextNode($subscriber->user_name));
		$result->appendChild($xmlDoc->createElement("email"))
		->appendChild($xmlDoc->createTextNode($subscriber->email));
		$result->appendChild($xmlDoc->createElement("full_name"))
		->appendChild($xmlDoc->createTextNode($subscriber->full_name));
		$result->appendChild($xmlDoc->createElement("sex"))
		->appendChild($xmlDoc->createTextNode($subscriber->sex));
		$result->appendChild($xmlDoc->createElement("birthday"))
		->appendChild($xmlDoc->createCDATASection($subscriber->birthday));
		$result->appendChild($xmlDoc->createElement("avatar_url"))
		->appendChild($xmlDoc->createTextNode($subscriber->avatar_url));
		$result->appendChild($xmlDoc->createElement("yahoo_id"))
		->appendChild($xmlDoc->createTextNode($subscriber->yahoo_id));
		$result->appendChild($xmlDoc->createElement("skype_id"))
		->appendChild($xmlDoc->createTextNode($subscriber->skype_id));
		$result->appendChild($xmlDoc->createElement("facebook_id"))
		->appendChild($xmlDoc->createTextNode($subscriber->facebook_id));
		$result->appendChild($xmlDoc->createElement("google_id"))
		->appendChild($xmlDoc->createTextNode($subscriber->google_id));
		$result->appendChild($xmlDoc->createElement("zing_id"))
		->appendChild($xmlDoc->createTextNode($subscriber->zing_id));

		echo $xmlDoc->saveXML();
		Yii::app()->end();
	}

	public function actionGetBookmark() {
		$manga_id = isset($_REQUEST['manga']) ? $_REQUEST['manga'] : NULL;
		if($manga_id == NULL) {
			$this->responseError(1,1, "manga id is null");
		}
		$clientUser = $this->_clientUser;
		$bookmark = MangaUserBookmark::model()->findByAttributes(array('manga_id'=>$manga_id, 'user_id'=>$clientUser->id));
		if($bookmark == NULL) {
			$this->responseError(1,1, "Have no bookmark of user in this manga");
		}
		$page = $bookmark->manga_page_id;
		$chapter = $bookmark->manga_chapter_id;

		if($this->_format == 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";

			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));
			$result->appendChild($xmlDoc->createElement('manga',$manga_id));
			$result->appendChild($xmlDoc->createElement('chapter',$chapter));
			$result->appendChild($xmlDoc->createElement('page',$page));
			echo $xmlDoc->saveXML();
		}
		else {
			$arr['session'] = $this->_sessionID?$this->_sessionID:"";
			$arr['client_app_code'] = $this->_app_version_code?$this->_app_version_code:1;
			$arr['device_type_id'] = $this->_device_type_id?$this->_device_type_id:"";
			$arr['user_name'] = $this->_username?$this->_username:"";
			$arr['action'] = $this->action->id;
			$arr['error_no'] = 0;

			$result = array();
			$result['manga'] = $manga_id;
			$result['chapter'] = $chapter;
			$result['page'] = $page;
			$arr['result'] = $result;
			$content = CJSON::encode($arr);
			echo $content;
		}
		Yii::app()->end();
	}

	public function actionAddBookmark() {
// 		$chapter_id = isset($_REQUEST['chapter']) ? $_REQUEST['chapter'] : NULL;
// 		$chapter = MangaChapter::model()->findByPk($chapter_id);
// 		if($chapter == NULL) {
// 			$this->responseError(1,1, "Chapter $chapter_id is not existed. Add bookmark failed");
// 		}

		$page_id = isset($_REQUEST['page']) ? $_REQUEST['page'] : -1;
		$page = MangaPage::model()->findByPk($page_id);
		if($page == NULL) {
			$this->responseError(1,1, "Page is not existed");
		}
		$chapter_id = $page->manga_chapter_id;
		$manga_id = $page->mangaChapter->manga_id;

		$clientUser = $this->_clientUser;
		$bookmark = MangaUserBookmark::model()->findByAttributes(array('user_id' => $clientUser->id, 'manga_id'=>$manga_id));
		if($bookmark == NULL) {
			$bookmark = new MangaUserBookmark();
		}
		$bookmark->user_id = $clientUser->id;
		$bookmark->manga_chapter_id = $chapter_id;
		$bookmark->manga_page_id = $page_id;
		$bookmark->manga_id = $manga_id;
		$bookmark->create_date  = new CDbExpression('NOW()');
		if(!$bookmark->save()) {
			print_r($bookmark->getErrors());
			$this->responseError(1,1, "Add bookmark failed");
		}
		$this->responseError(0,0, "Add bookmark successfully");
	}

	public function actionAddSubscription() {
		$manga_id = isset($_REQUEST['manga']) ? $_REQUEST['manga'] : NULL;
		$manga = Manga::model()->findByPk($manga_id);
		if($manga == NULL) {
			$this->responseError(1,1, "Manga $manga_id is not existed. Add subscription failed");
		}

		$newSubscription = $this->_clientUser->addSubscription($manga, 2);
		if($newSubscription == NULL) {
			$this->responseError(1,1, "Add subscription failed");
		}
		$this->responseError(0,0, "Add subscription successfully");
	}

	public function actionGetSubscription() {
		$subType = isset($_REQUEST['sub_type']) ? $_REQUEST['sub_type'] : 2; //1 : truyen dang doc, 2 : subscription

		$clientUser = $this->_clientUser;
		$listSubs = MangaUserSubscription::model()->findAllByAttributes(array('user_id'=>$clientUser->id, 'sub_type'=>$subType), array('order'=>'id desc'));

		if($this->_format == 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";

			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));
			foreach($listSubs as $sub) {
				$result->appendChild($xmlDoc->createElement('manga', $sub->manga_id));
			}
			echo $xmlDoc->saveXML();
		}
		else {
			$arr['session'] = $this->_sessionID?$this->_sessionID:"";
			$arr['client_app_code'] = $this->_app_version_code?$this->_app_version_code:1;
			$arr['device_type_id'] = $this->_device_type_id?$this->_device_type_id:"";
			$arr['user_name'] = $this->_username?$this->_username:"";
			$arr['action'] = $this->action->id;
			$arr['error_no'] = 0;
				
			$result = array();
			foreach($listSubs as $sub) {
				$result['manga'] = $sub->manga_id;
			}
			$arr['result'] = $result;
			$content = CJSON::encode($arr);
			echo $content;
		}
		Yii::app()->end();
	}
}
