<?php
//toan bo api de tra xml data cho client
class DataXmlController extends Controller
{
	/**
	* @return array action filters
	*/
	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
				array('allow', // allow all users to perform 'index' and 'view' actions
						'actions' => array('index', 'view'),
						'users' => array('*'),
				),
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions' => array('create', 'update'),
						'users' => array('@'),
				),
				array('allow', // allow admin user to perform all actions
						//'actions' => array('create', 'delete'),
						//TODO: authen, author
						//'users' => array('admin'),
				),
				array('deny', // deny all users
						'users' => array('*'),
				),
		);
	}

	public function beforeAction($action) {
		//        echo($action->id);
		$this->_format = (isset($_REQUEST['format']) && !empty($_REQUEST['format']))?$_REQUEST['format']:"xml";

		$needAuth = 1; // tru nhung function nhu comment, get favorite..., nhung function con lai ko can authen
        if((strtolower($action->id) == 'getmanga') || (strtolower($action->id) == 'getchapters')  || (strtolower($action->id) == 'getchapterdetail')) {
            $needAuth = 0;
        }

		$this->_sessionID = (isset($_REQUEST['session']) && !empty($_REQUEST['session']))?$_REQUEST['session']:"";
		$this->_username = (isset($_REQUEST['username']) && !empty($_REQUEST['username']))?$_REQUEST['username']:"";
		$this->_clientUser = ClientUser::model()->findByAttributes(array('user_name'=>$this->_username)); //NULL or nor NULL
		$this->_device_type_id = (isset($_REQUEST['device_type_id']) && !empty($_REQUEST['device_type_id']))?$_REQUEST['device_type_id']:1;
		$this->_device_id = (isset($_REQUEST['device_id']) && !empty($_REQUEST['device_id']))?$_REQUEST['device_id']:'';
		$this->_device_id = str_replace('-', '_', $this->_device_id);
		$this->_device_token = (isset($_REQUEST['device_token']) && !empty($_REQUEST['device_token']))?$_REQUEST['device_token']:'';
		$this->_app_version_code = (isset($_REQUEST['app_version_code']) && !empty($_REQUEST['app_version_code']))?$_REQUEST['app_version_code']:"";
		$this->_isTablet = (isset($_REQUEST['tablet']) && !empty($_REQUEST['tablet']))?$_REQUEST['tablet']:0;

		if ($action->id != 'login' 
                && $action->id != 'registerUser'
                && $action->id != 'index'
                && $action->id != 'error'
                && $action->id != 'facebookLogin') {
            if ( empty ($this->_username) || empty ($this->_sessionID)
            ) {
                if ($needAuth == 1) $this->responseError(1, 1, "Unknown username and/or session id");
            }
            else {
                 // try to authenticate by session id and username
                $loginResult = ClientAuthen::LoginSession($this->_username, $this->_sessionID);
                if ($loginResult != ClientAuthen::ERROR_NONE) {
                    $this->responseError(1, 1, ClientAuthen::$errorMessage[$loginResult]);
                    return false;
                }
                else {
                    $this->_sessionID = ClientAuthen::$sessionID;
                    return parent::beforeAction($action);
                }   
            }
        }
        
        return parent::beforeAction($action);
	}

	/**
	 *
	 * @param type $error_no
	 * @param type $error_code
	 * @param type $message
	 */
	public function responseError($error_no, $error_code, $message) {
		header("Content-type: text/xml; charset=utf-8");
		$xmlDoc = new DOMDocument();
		$xmlDoc->encoding = "UTF-8";
		$xmlDoc->version = "1.0";

		//TODO: authen, session, error handle
		$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
		$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
		$root->appendChild($xmlDoc->createElement("action", $this->action->id));
		$root->appendChild($xmlDoc->createElement("error_no", $error_no));
		$root->appendChild($xmlDoc->createElement("error_code", $error_code));
		$root->appendChild($xmlDoc->createElement("error_message", CHtml::encode($message)));

		echo $xmlDoc->saveXML();
		Yii::app()->end();
	}

	/**
	 *
	 * @param type $error_no
	 * @param type $error_code
	 * @param type $message
	 */
	public function responseAction($error_no, $arrMessages) {
		header("Content-type: text/xml; charset=utf-8");
		$xmlDoc = new DOMDocument();
		$xmlDoc->encoding = "UTF-8";
		$xmlDoc->version = "1.0";

		//TODO: authen, session, error handle
		$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
		$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
		$root->appendChild($xmlDoc->createElement("action", $this->action->id));
		$root->appendChild($xmlDoc->createElement("error_no", $error_no));
		$result = $root->appendChild($xmlDoc->createElement("result"));
		$msgNode = $result->appendChild($xmlDoc->createElement("messages"));
		foreach($arrMessages as $msg) {
			$msgNode->appendChild($xmlDoc->createElement("message"))
			->appendChild($xmlDoc->createCDATASection($msg));
		}
		echo $xmlDoc->saveXML();
		Yii::app()->end();
	}

	public function actionGetManga() {
		$order = isset($_REQUEST['order'])?$_REQUEST['order']:"";

		$page = isset($_REQUEST['page'])?$_REQUEST['page']:0;
		$page_size = isset($_REQUEST['page_size'])?$_REQUEST['page_size']:10;
		$image_width = isset($_REQUEST['image_width'])?$_REQUEST['image_width']:NULL;
		$keyword = isset($_REQUEST['keyword'])?$_REQUEST['keyword']:"";

		// add keyword to history
		if (!empty($keyword)) {
			$keyword = CVietnameseTools::makeSearchableStr($keyword);
		}

		$db_order = "";
		switch($order)
		{
			case "top_new":
				$db_order = 't.id DESC';
				break;
			case "most_viewed":
				$db_order = 't.view_count DESC';
				break;
			case "top_rated":
				//$db_order = '(rating_count*3 + rating*7) DESC';
				$db_order = 't.rating_count DESC';
				break;
			case "most_discussed":
				$db_order = 't.comment_count DESC';
				break;
			case "featured"://not support now
				// $models = Asset::model()->findAll();
				//break;
			case "order_number":
				$db_order = 't.order_number ASC';
				break;
			default ://case "default":
				$order = 'default';
				//                $db_order = "t.modify_date DESC"; // tam thoi comment lai, sap xep theo ten phim
				$db_order = "t.id DESC, t.display_name_ascii";
				break;
		}

		//echo $this->_format;

		if ($this->_format === 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";

			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));

			$res = Manga::findManga('', $db_order, $page, $page_size, $keyword);

			$result->appendChild($xmlDoc->createElement('keyword', CHtml::encode($res['keyword'])));
			$result->appendChild($xmlDoc->createElement('page_number', CHtml::encode($res['page_number'])));
			$result->appendChild($xmlDoc->createElement('page_size', CHtml::encode($res['page_size'])));
			$result->appendChild($xmlDoc->createElement('total_page', CHtml::encode($res['total_page'])));
			$result->appendChild($xmlDoc->createElement('total_result', CHtml::encode($res['total_result'])));

			foreach ($res['data'] as $manga) {
				$mangaNode = $xmlDoc->createElement("manga");
				$result->appendChild($mangaNode);
				$mangaNode->appendChild($xmlDoc->createAttribute("id"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['id'])));

				$mangaNode->appendChild($xmlDoc->createAttribute("create_date"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['create_date'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("update_date"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['update_date'])));
				//                $mangaNode->appendChild($xmlDoc->createAttribute("modify_date"))
				//                        ->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['modify_date'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("view_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['view_count'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("comment_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['comment_count'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("like_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['like_count'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("dislike_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['dislike_count'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("rating"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['rating'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("rating_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['rating_count'])));
				$mangaNode->appendChild($xmlDoc->createAttribute("chapter_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($manga['chapter_count'])));
				 
				$mangaNode->appendChild($xmlDoc->createElement('name'))
				->appendChild($xmlDoc->createCDATASection($manga['display_name']));

				$author = Author::model()->findByPk($manga['author_id']);
				$authorStr = "";
				if($author != NULL) {
					$authorStr = $author->display_name;
				}
				$mangaNode->appendChild($xmlDoc->createElement('author'))
				->appendChild($xmlDoc->createCDATASection($authorStr));

				$coverUrl = "";
				$cover = MangaCover::model()->findByAttributes(array("manga_id"=>$manga["id"]));
				if($cover != NULL) {
					$coverUrl = $cover->url;
				}
				$mangaNode->appendChild($xmlDoc->createElement('cover'))
				->appendChild($xmlDoc->createCDATASection($coverUrl));

				$mangaNode->appendChild($xmlDoc->createElement('short_description'))
				->appendChild($xmlDoc->createCDATASection($manga['short_description']));
				$mangaNode->appendChild($xmlDoc->createElement('description'))
				->appendChild($xmlDoc->createCDATASection($manga['description']));
			}
				
			echo $xmlDoc->saveXML();
			Yii::app()->end();
		}
	}

	public function actionGetChapters() {
		$manga_id = isset($_REQUEST['manga'])?$_REQUEST['manga']:"";
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:0;
		$page_size = isset($_REQUEST['page_size'])?$_REQUEST['page_size']:10;
		//echo $this->_format;

		if ($this->_format === 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";

			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));

			$criteria = new CDbCriteria();
			$criteria->addCondition("t.manga_id = ".$manga_id);
			$criteria->addCondition("t.status = 1");
			$criteria->order = "t.chapter_order desc";
			$res = MangaChapter::model()->findAll($criteria);

			$result->appendChild($xmlDoc->createElement('page_number', CHtml::encode($page)));
			$result->appendChild($xmlDoc->createElement('page_size', CHtml::encode($page_size)));
			$result->appendChild($xmlDoc->createElement('total_result', CHtml::encode(sizeof($res))));
			if($page_size == 0) {
				$page_size = 1;
			}
			$total_page = floor(sizeof($res)/$page_size) + 1;
			$result->appendChild($xmlDoc->createElement('total_page', CHtml::encode($total_page)));

			$i = 0;
			$startIndex = $page*$page_size;
			$endIndex = $startIndex + $page_size;
			foreach ($res as $chapter) {
				$i++;
				if($i <= $startIndex) {
					continue;
				}
				if($i > $endIndex) {
					break;
				}
				$chapterNode = $xmlDoc->createElement("chapter");
				$result->appendChild($chapterNode);
				$chapterNode->appendChild($xmlDoc->createAttribute("id"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['id'])));

				$chapterNode->appendChild($xmlDoc->createAttribute("create_date"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['create_date'])));
				//                $chapterNode->appendChild($xmlDoc->createAttribute("modify_date"))
				//                        ->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['modify_date'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("view_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['view_count'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("comment_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['comment_count'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("like_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['like_count'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("dislike_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['dislike_count'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("rating"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['rating'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("rating_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['rating_count'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("chapter_order"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['chapter_order'])));
				$chapterNode->appendChild($xmlDoc->createAttribute("page_count"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($chapter['page_count'])));
				 
				$chapterNode->appendChild($xmlDoc->createElement('name'))
				->appendChild($xmlDoc->createCDATASection($chapter['display_name']));

			}
				
			echo $xmlDoc->saveXML();
			Yii::app()->end();
		}
	}
	
	//tra lai manga page list of next||prev chapter
	//delta == 1 -> next chapter, delta == -1 -> prev chapter
	public function actionGetChapterDetail($chapter_id, $delta = 0) {
		$chapter = MangaChapter::model()->findByPk($chapter_id);
		if($chapter == NULL) {
			$this->responseError(1,1, "Not exist chapter $chapter_id");
		}
		
		if($delta != 0) {
			$manga = Manga::model()->findByPk($chapter->manga_id);
			if($manga == NULL) {
				$this->responseError(1,1, "Not exist manga of chapter $chapter_id");
			}
			$currentOrder = $chapter->chapter_order;
			$otherChapter = NULL;
			if($delta > 0) {
				$otherChapter = MangaChapter::model()->findBySql("select * from manga_chapter where chapter_order > $currentOrder limit 1");
			}
			else {
				$otherChapter = MangaChapter::model()->findBySql("select * from manga_chapter where chapter_order < $currentOrder limit 1");
			}
			if($otherChapter == NULL) {
				$this->responseError(1,1, "Not existed next/prev chapter");	
			}
			
			$this->getChapterDetail($otherChapter->id);
		}
		else {
			$this->getChapterDetail($chapter->id);
		}
	}

	private function getChapterDetail($chapter_id) {
		$chapter = MangaChapter::model()->findByPk($chapter_id);
		if($chapter == NULL) {
			$this->responseError(1,1, "Not exist chapter $chapter_id");
		}
		$manga = Manga::model()->findByPk($chapter->manga_id);
		if($manga == NULL) {
			$this->responseError(1,1, "Not exist manga has chapter $chapter_id");
		}
		if($this->_clientUser != NULL) {
			$this->_clientUser->addSubscription($manga, 1);
		}
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:0;
		$page_size = isset($_REQUEST['page_size'])?$_REQUEST['page_size']:10;
		//echo $this->_format;
		
		if ($this->_format === 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";
		
			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));
		
			$criteria = new CDbCriteria();
			$criteria->addCondition("t.manga_chapter_id = ".$chapter_id);
			$criteria->addCondition("t.status = 1");
			$criteria->order = "t.page_number";
			$res = MangaPage::model()->findAll($criteria);
		
			$result->appendChild($xmlDoc->createElement('page_number', CHtml::encode($page)));
			$result->appendChild($xmlDoc->createElement('page_size', CHtml::encode($page_size)));
			$result->appendChild($xmlDoc->createElement('total_result', CHtml::encode(sizeof($res))));
			if($page_size == 0) {
				$page_size = 1;
			}
			$total_page = floor(sizeof($res)/$page_size) + 1;
			$result->appendChild($xmlDoc->createElement('total_page', CHtml::encode($total_page)));
		
			$i = 0;
			$startIndex = $page*$page_size;
			$endIndex = $startIndex + $page_size;
			foreach ($res as $page) {
				$i++;
				if($i <= $startIndex) {
					continue;
				}
				if($i > $endIndex) {
					break;
				}
				$pageNode = $xmlDoc->createElement("page");
				$result->appendChild($pageNode);
				$pageNode->appendChild($xmlDoc->createAttribute("id"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($page['id'])));
				$pageNode->appendChild($xmlDoc->createAttribute("page_number"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($page['page_number'])));
		
				$pageNode->appendChild($xmlDoc->createElement("image_url1"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($page['image_url1'])));
				$pageNode->appendChild($xmlDoc->createElement("image_url2"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($page['image_url2'])));
				$pageNode->appendChild($xmlDoc->createElement("image_url3"))
				->appendChild($xmlDoc->createTextNode(CHtml::encode($page['image_url3'])));
			}
		
			echo $xmlDoc->saveXML();
			Yii::app()->end();
		}	
	}
	
	public function actionSendFeedbackManga() {
		$manga_id = isset($_REQUEST['manga']) ? $_REQUEST['manga'] : NULL;
		if ($manga_id=="") $manga_id = NULL;
		$chapter_id = isset($_REQUEST['chapter']) ? $_REQUEST['chapter'] : NULL;
		if ($chapter_id=="") $chapter_id = NULL;
		$like = isset($_REQUEST['like']) ? $_REQUEST['like'] : NULL;
		if ($like=="") $like = NULL;
		$rating = isset($_REQUEST['rate']) ? $_REQUEST['rate'] : NULL;
		if ($rating=="") $rating = NULL;
		$favorite = isset($_REQUEST['favorite']) ? $_REQUEST['favorite'] : NULL;
		if ($favorite=="") $favorite = NULL;
		$comment_title = isset($_REQUEST['comment_title']) ? $_REQUEST['comment_title'] : NULL;
		if ($comment_title=="") $comment_title = NULL;
		$comment_content = isset($_REQUEST['comment_content']) ? $_REQUEST['comment_content'] : NULL;
		if ($comment_content=="") $comment_content = NULL;

		$subscriber = $this->_clientUser;
		$user_id = $subscriber->id;

		if (is_NULL($like) && is_NULL($rating)
		&& is_NULL($favorite)&& is_NULL($comment_content)) {
			$this->responseError(1,"ERROR_","No actions specified!");
		}

		if ($manga_id === NULL) {
			//            $this->responseError(1,"ERROR_","No manga specified!");
		}
		else {
			$manga = Manga::model()->findByPk($manga_id);
			/* @var $manga Manga */

			if ($manga == NULL) {
				$this->responseError(1, "ERROR_", "Requested manga not found.");
			}
		}

		if ($chapter_id === NULL) {
			//            $this->responseError(1,"ERROR_","No chapter specified!");
		}
		else {
			$chapter = MangaChapter::model()->findByPk($chapter_id);
			/* @var $chapter chapterAsset */

			if ($chapter == NULL) {
				$this->responseError(1, "ERROR_", "Requested chapter not found.");
			}
		}

		if ($this->_format === 'xml') {
			header("Content-type: text/xml; charset=utf-8");
			$xmlDoc = new DOMDocument();
			$xmlDoc->encoding = "UTF-8";
			$xmlDoc->version = "1.0";

			//TODO: authen, session, error handle
			$root = $xmlDoc->appendChild($xmlDoc->createElement("response"));
			$root->appendChild($xmlDoc->createElement("session", $this->_sessionID?$this->_sessionID:""));
			$root->appendChild($xmlDoc->createElement("action", $this->action->id));
			$root->appendChild($xmlDoc->createElement("error_no", "0"));
			$result = $root->appendChild($xmlDoc->createElement("result"));

			$message = array();

			if (!is_NULL($like)) {
				$like = $like==0?0:1;
				// kiem tra da vote chua
				$old_val = MangaLikeDislike::model()->findByAttributes(
						array(
								'user_id' => $subscriber->id,
								'manga_id' => $manga->id,
						));

				// trong giai doan chay thu, chi co 1 account (anonymous) nen cho like, dislike thoai mai
				$new_val = new MangaLikeDislike();
				$new_val->create_date = new CDbExpression('NOW()');
				$new_val->like = $like;
				$new_val->user_id = $subscriber->id;
				$new_val->manga_id = $manga_id;
				if ($new_val->like != 0) {
					$manga->like_count++;
					$tmp = 'You\'ve liked this manga. Thank you for feedback.';
				}
				else {
					$manga->dislike_count++;
					$tmp = 'You\'ve disliked this manga. Thank you for feedback.';
				}
				if ($new_val->save()) {
					$manga->update();
					$message[] = $tmp;
				}
			}

			if (!is_NULL($rating)) {
				$rating = $rating % 6;
				// kiem tra da vote chua
				$old_val = MangaRating::model()->findByAttributes(
						array(
								'user_id' => $subscriber->id,
								'manga_id' => $manga->id,
						));
				// trong giai doan chay thu, chi co 1 account (anonymous) nen cho rating thoai mai
				$new_val = new MangaRating();
				$new_val->create_date = new CDbExpression('NOW()');
				$new_val->rating = $rating;
				$new_val->user_id = $subscriber->id;
				$new_val->manga_id = $manga_id;
				$manga->rating = ($manga->rating*$manga->rating_count + $rating)/($manga->rating_count +1);
				$manga->rating_count++;

				if ($new_val->save()) {
					$manga->update();
					$message[] = "You've rated this manga at $rating star(s). Thank you for feedback.";
				}
			}

			if (!is_NULL($favorite)) {
				// kiem tra da vote chua
				$old_val = MangaUserSubscription::model()->findByAttributes(
						array(
								'user_id' => $subscriber->id,
								'manga_id' => $manga->id,
						));
				if ($old_val != NULL) {
					if ($favorite!=0) {
						$message[] = "This manga is already in your favorite list!";
					}
					else {
						$manga->subscription_count--;
						if ($old_val->delete()) {
							$manga->update();
							$message[] = "This manga has been removed from your favorite list!"; ;
						}
					}
				}
				else {
					if ($favorite!=0) {
						$new_val = new MangaUserSubscription();
						$new_val->create_date = new CDbExpression('NOW()');
						$new_val->user_id = $subscriber->id;
						$new_val->manga_id = $manga_id;
						$manga->subscription_count++;

						if ($new_val->save()) {
							$manga->update();
							$message[] = "Successfully add to your favorite!";
						}
					}
					else {
						$message[] = "This manga is not in your favorite yet!";
					}
				}
			}

			if (!is_NULL($comment_content)) {
				$new_val = new VodComment();
				$new_val->create_date = new CDbExpression('NOW()');

				if($user_id > -1) {
					$new_val->user_id = $subscriber->id;
				}
				if($manga_id != NULL) {
					$new_val->manga_id = $manga_id;
					$manga->comment_count++;
				}
				if($chapter_id != NULL) {
					$new_val->chapter_id = $chapter_id;
					$chapter->comment_count++;
				}

				$new_val->comment = $comment_content;
				$new_val->title = $comment_title;


				if ($new_val->save()) {
					if($manga_id != NULL) {
						$manga->update();
					}
					if($chapter_id != NULL) {
						$chapter->update();
					}

					$message[] = "Thanks for your feedback!";
				}
				else {
					$this->responseError(1,1,"Unable to save your comment");
				}
			}

			$msgs = $result->appendChild($xmlDoc->createElement("messages"));
			foreach ($message as $msg) {
				$msgs->appendChild($xmlDoc->createElement("message"))
				->appendChild($xmlDoc->createCDATASection($msg));
			}

			$xmlDoc->formatOutput = true;

			$content = $xmlDoc->saveXML();

			echo $content;
		}
		else {
			//TODO
			$this->responseError(1,"ERROR_","not supported output format");
		}
	}
	
	public function actionGetCategories() {
		
	}
}