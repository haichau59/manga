<?php
$this->breadcrumbs=array(
	'Client Users',
);

$this->menu=array(
	array('label'=>'Create ClientUser','url'=>array('create')),
	array('label'=>'Manage ClientUser','url'=>array('admin')),
);
?>

<h1>Client Users</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
