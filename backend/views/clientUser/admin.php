<?php
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ClientUser','url'=>array('index')),
	array('label'=>'Create ClientUser','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('client-user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Client Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'client-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'subscriber_number',
		'user_name',
		'status',
		'email',
		'full_name',
		/*
		'password',
		'last_login_time',
		'last_login_session',
		'birthday',
		'sex',
		'avatar_url',
		'yahoo_id',
		'skype_id',
		'google_id',
		'zing_id',
		'facebook_id',
		'create_date',
		'account_type',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
