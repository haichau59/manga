<?php
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ClientUser','url'=>array('index')),
	array('label'=>'Create ClientUser','url'=>array('create')),
	array('label'=>'View ClientUser','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ClientUser','url'=>array('admin')),
);
?>

<h1>Update ClientUser <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>