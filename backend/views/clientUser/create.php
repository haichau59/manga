<?php
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ClientUser','url'=>array('index')),
	array('label'=>'Manage ClientUser','url'=>array('admin')),
);
?>

<h1>Create ClientUser</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>