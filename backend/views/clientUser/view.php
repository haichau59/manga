<?php
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ClientUser','url'=>array('index')),
	array('label'=>'Create ClientUser','url'=>array('create')),
	array('label'=>'Update ClientUser','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ClientUser','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ClientUser','url'=>array('admin')),
);
?>

<h1>View ClientUser #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'subscriber_number',
		'user_name',
		'status',
		'email',
		'full_name',
		'password',
		'last_login_time',
		'last_login_session',
		'birthday',
		'sex',
		'avatar_url',
		'yahoo_id',
		'skype_id',
		'google_id',
		'zing_id',
		'facebook_id',
		'create_date',
		'account_type',
	),
)); ?>
