<?php
$this->breadcrumbs=array(
	'Manga Chapters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MangaChapter','url'=>array('index')),
	array('label'=>'Create MangaChapter','url'=>array('create')),
	array('label'=>'Update MangaChapter','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MangaChapter','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MangaChapter','url'=>array('admin')),
);
?>

<h1>View MangaChapter #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code_name',
		'display_name',
		'tags',
		'short_description',
		'description',
		'chapter_order',
		'create_date',
		'modify_date',
		'view_count',
		'like_count',
		'dislike_count',
		'comment_count',
		'page_count',
		'rating',
		'rating_count',
		'status',
		'manga_id',
		'uploader_id',
		'download_url1',
		'download_url2',
		'download_url3',
	),
)); ?>
