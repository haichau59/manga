<?php
$this->breadcrumbs=array(
	'Manga Chapters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MangaChapter','url'=>array('index')),
	array('label'=>'Manage MangaChapter','url'=>array('admin')),
);
?>

<h1>Create MangaChapter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>