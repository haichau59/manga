<?php
$this->breadcrumbs=array(
	'Manga Chapters',
);

$this->menu=array(
	array('label'=>'Create MangaChapter','url'=>array('create')),
	array('label'=>'Manage MangaChapter','url'=>array('admin')),
);
?>

<h1>Manga Chapters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
