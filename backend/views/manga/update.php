<?php
$this->breadcrumbs=array(
	'Mangas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Manga','url'=>array('index')),
	array('label'=>'Create Manga','url'=>array('create')),
	array('label'=>'View Manga','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Manga','url'=>array('admin')),
);
?>

<h1>Update Manga <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>