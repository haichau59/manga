<?php
$this->breadcrumbs=array(
	'Mangas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Manga','url'=>array('index')),
	array('label'=>'Manage Manga','url'=>array('admin')),
);
?>

<h1>Create Manga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>