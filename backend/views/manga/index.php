<?php
$this->breadcrumbs=array(
	'Mangas',
);

$this->menu=array(
	array('label'=>'Create Manga','url'=>array('create')),
	array('label'=>'Manage Manga','url'=>array('admin')),
);
?>

<h1>Mangas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
