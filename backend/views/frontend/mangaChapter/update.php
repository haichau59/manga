<?php
$this->breadcrumbs=array(
	'Manga Chapters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MangaChapter','url'=>array('index')),
	array('label'=>'Create MangaChapter','url'=>array('create')),
	array('label'=>'View MangaChapter','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MangaChapter','url'=>array('admin')),
);
?>

<h1>Update MangaChapter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>