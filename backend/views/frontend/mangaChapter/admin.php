<?php
$this->breadcrumbs=array(
	'Manga Chapters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MangaChapter','url'=>array('index')),
	array('label'=>'Create MangaChapter','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('manga-chapter-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Manga Chapters</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'manga-chapter-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'code_name',
		'display_name',
		'tags',
		'short_description',
		'description',
		/*
		'chapter_order',
		'create_date',
		'modify_date',
		'view_count',
		'like_count',
		'dislike_count',
		'comment_count',
		'page_count',
		'rating',
		'rating_count',
		'status',
		'manga_id',
		'uploader_id',
		'download_url1',
		'download_url2',
		'download_url3',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
