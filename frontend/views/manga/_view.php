<?php
/* @var $this MangaController */
/* @var $data Manga */
?>

<div class="view">

<table border="0">
<td>
</td>
<td> 
	<tr>
		<td>
		<?php echo CHtml::link(CHtml::encode($data->display_name), array('view', 'id'=>$data->id)); ?>
		</td>
		<td></td>
	<tr>
		<td align="left">
			<b><?php echo CHtml::encode($data->getAttributeLabel('view_count')); ?>:</b>
			<?php echo CHtml::encode($data->view_count); ?>
		</td>
		<td align="right">
			<b><?php echo CHtml::encode($data->getAttributeLabel('like_count')); ?>:</b>
			<?php echo CHtml::encode($data->like_count); ?>
		</td>
	</tr>
</td>
</table>
	<br />
	
	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dislike_count')); ?>:</b>
	<?php echo CHtml::encode($data->dislike_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating_count')); ?>:</b>
	<?php echo CHtml::encode($data->rating_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment_count')); ?>:</b>
	<?php echo CHtml::encode($data->comment_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_count')); ?>:</b>
	<?php echo CHtml::encode($data->image_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_number')); ?>:</b>
	<?php echo CHtml::encode($data->order_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uploader_id')); ?>:</b>
	<?php echo CHtml::encode($data->uploader_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_series')); ?>:</b>
	<?php echo CHtml::encode($data->is_series); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chapter_count')); ?>:</b>
	<?php echo CHtml::encode($data->chapter_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_name')); ?>:</b>
	<?php echo CHtml::encode($data->other_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subscription_count')); ?>:</b>
	<?php echo CHtml::encode($data->subscription_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content_status')); ?>:</b>
	<?php echo CHtml::encode($data->content_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_translation')); ?>:</b>
	<?php echo CHtml::encode($data->other_translation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('author_id')); ?>:</b>
	<?php echo CHtml::encode($data->author_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_free')); ?>:</b>
	<?php echo CHtml::encode($data->is_free); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manga_type')); ?>:</b>
	<?php echo CHtml::encode($data->manga_type); ?>
	<br />

	*/ ?>

</div>