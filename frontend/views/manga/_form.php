<?php
/* @var $this MangaController */
/* @var $model Manga */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manga-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'display_name'); ?>
		<?php echo $form->textField($model,'display_name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'display_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'short_description'); ?>
		<?php echo $form->textField($model,'short_description',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'short_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'view_count'); ?>
		<?php echo $form->textField($model,'view_count'); ?>
		<?php echo $form->error($model,'view_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'like_count'); ?>
		<?php echo $form->textField($model,'like_count'); ?>
		<?php echo $form->error($model,'like_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dislike_count'); ?>
		<?php echo $form->textField($model,'dislike_count'); ?>
		<?php echo $form->error($model,'dislike_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating'); ?>
		<?php echo $form->textField($model,'rating'); ?>
		<?php echo $form->error($model,'rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating_count'); ?>
		<?php echo $form->textField($model,'rating_count'); ?>
		<?php echo $form->error($model,'rating_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comment_count'); ?>
		<?php echo $form->textField($model,'comment_count'); ?>
		<?php echo $form->error($model,'comment_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_count'); ?>
		<?php echo $form->textField($model,'image_count'); ?>
		<?php echo $form->error($model,'image_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date'); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'order_number'); ?>
		<?php echo $form->textField($model,'order_number'); ?>
		<?php echo $form->error($model,'order_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uploader_id'); ?>
		<?php echo $form->textField($model,'uploader_id'); ?>
		<?php echo $form->error($model,'uploader_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_series'); ?>
		<?php echo $form->textField($model,'is_series'); ?>
		<?php echo $form->error($model,'is_series'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chapter_count'); ?>
		<?php echo $form->textField($model,'chapter_count'); ?>
		<?php echo $form->error($model,'chapter_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_name'); ?>
		<?php echo $form->textField($model,'other_name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'other_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subscription_count'); ?>
		<?php echo $form->textField($model,'subscription_count'); ?>
		<?php echo $form->error($model,'subscription_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date'); ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content_status'); ?>
		<?php echo $form->textField($model,'content_status'); ?>
		<?php echo $form->error($model,'content_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_translation'); ?>
		<?php echo $form->textField($model,'other_translation',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'other_translation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'author_id'); ?>
		<?php echo $form->textField($model,'author_id'); ?>
		<?php echo $form->error($model,'author_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_free'); ?>
		<?php echo $form->textField($model,'is_free'); ?>
		<?php echo $form->error($model,'is_free'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'manga_type'); ?>
		<?php echo $form->textField($model,'manga_type'); ?>
		<?php echo $form->error($model,'manga_type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->