<?php
/* @var $this MangaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mangas',
);

$this->menu=array(
	array('label'=>'Create Manga', 'url'=>array('create')),
	array('label'=>'Manage Manga', 'url'=>array('admin')),
);
?>

<h1>Mangas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
