<?php
/* @var $this MangaController */
/* @var $model Manga */

$this->breadcrumbs=array(
	'Mangas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Manga', 'url'=>array('index')),
	array('label'=>'Create Manga', 'url'=>array('create')),
	array('label'=>'Update Manga', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Manga', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manga', 'url'=>array('admin')),
);
?>

<h1>View Manga #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'display_name',
		'tags',
		'short_description',
		'description',
		'view_count',
		'like_count',
		'dislike_count',
		'rating',
		'rating_count',
		'comment_count',
		'image_count',
		'status',
		'create_date',
		'type',
		'order_number',
		'uploader_id',
		'is_series',
		'chapter_count',
		'other_name',
		'subscription_count',
		'update_date',
		'content_status',
		'other_translation',
		'author_id',
		'is_free',
		'price',
		'manga_type',
	),
)); ?>
