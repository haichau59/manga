<?php
/* @var $this MangaController */
/* @var $model Manga */

$this->breadcrumbs=array(
	'Mangas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Manga', 'url'=>array('index')),
	array('label'=>'Create Manga', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('manga-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Mangas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manga-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'display_name',
		'tags',
		'short_description',
		'description',
		'view_count',
		/*
		'like_count',
		'dislike_count',
		'rating',
		'rating_count',
		'comment_count',
		'image_count',
		'status',
		'create_date',
		'type',
		'order_number',
		'uploader_id',
		'is_series',
		'chapter_count',
		'other_name',
		'subscription_count',
		'update_date',
		'content_status',
		'other_translation',
		'author_id',
		'is_free',
		'price',
		'manga_type',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
