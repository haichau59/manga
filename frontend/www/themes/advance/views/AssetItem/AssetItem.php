<!--<div class="content-items">
	<div class="emborder"><a href="<?php echo Yii::app()->baseUrl . "/video/" . $asset['id'];?>"><?php if (isset($posterUrl) && $posterUrl != "") { ?><img src="<?php echo $posterUrl;?>" alt="<?php echo CHtml::encode($asset['display_name']);?>" class="poster-img" /><?php } ?></a></div>
	<div>
		<h3><a href="<?php echo Yii::app()->baseUrl . "/video/" . $asset['id'];?>" style="color:#000;"><?php echo CHtml::encode($asset['display_name']);?></a></h3>
		<p style="font-style:italic;color:#000;font-weight:normal;margin-right:20px;"><?php echo $asset['chapter_count'] . " chương, " . $asset['view_count'] . " lượt xem"?></p>
		<p style="margin-left:140px;"><a href="<?php echo Yii::app()->baseUrl . "/video/purchase?type=watch&id=" . urlencode($asset['id']);?>"><span class="icon-film"> </span>Xem</a></p>
	</div>
	<span class="right-arrow"></span>
</div>-->

    <div class="img_thumbnail">
        <a href="<?php echo Yii::app()->baseUrl . "/manga/" . $asset['id'];?>" data-ajax="false">
            <img style="width: 80px; height: 120px" src="<?php echo $posterUrl;?>" alt="<?php echo CHtml::encode($asset['display_name']);?>">
        </a>
        <span class="film_title"><?php echo CHtml::encode($asset['display_name']);?></span>
        <span class="film_title_normal"><?php echo $asset['chapter_count'] . " chương"?></span>
        <span id="star_<?php echo $asset['id']; ?>"></span>
    </div>


<script type="text/javascript">
$('#star_<?php echo $asset['id']; ?>').raty({ readOnly: true, score: <?php echo $asset['rating'] ?>,
                                              path: "<?php echo Yii::app()->theme->baseUrl;?>/images/"
                                            });
</script>
