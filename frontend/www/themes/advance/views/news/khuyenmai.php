<?php
$this->pageTitle = "mobiphim - Khuyến mại";
?>
<style type="text/css">
    p { text-align:justify;font-weight:normal;margin-bottom:10px;}
    li { list-style:disc; }
    strong, b { font-weight:bold; }
    .img_content {
    	width:100%;
    	height: auto;
    }
    
    .info_text{
    	color: #FFF;
		font-size: 16px;
		font-weight:bold;
		display: block;
		margin: 15px;
    }
    .box_huongdan{
    	width: 70%;
    	min-width: 320px;
    	border: 2px solid #000;
    	background: linear-gradient(to bottom, #404040 0%,#404040 50%,#2A2A2A 51%,#2A2A2A 100%);
		-webkit-box-shadow: 0px 1px 0px 0px rgba(255, 255, 255, 0.22);
		border: 2px solid #000;
		box-shadow: 0px 1px 0px 0px rgba(255, 255, 255, 0.22);
		border-radius: 5px 5px 5px 5px;
		padding: 5px;
    }
    .box_huongdan p{
    	font-size: 1.5em; 
    	color: #969696; 
    	text-align: center;
    	margin: 0px;
    	border: 10px;
    }
    .button_dk{
		background: -moz-radial-gradient(center, ellipse cover,  #edfc46 0%, #f75904 99%); /* FF3.6+ */
		background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#edfc46), color-stop(99%,#f75904)); /* Chrome,Safari4+ */
		background: -webkit-radial-gradient(center, ellipse cover,  #edfc46 0%,#f75904 99%); /* Chrome10+,Safari5.1+ */
		background: -o-radial-gradient(center, ellipse cover,  #edfc46 0%,#f75904 99%); /* Opera 12+ */
		background: -ms-radial-gradient(center, ellipse cover,  #edfc46 0%,#f75904 99%); /* IE10+ */
		background: radial-gradient(ellipse at center,  #edfc46 0%,#f75904 99%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#edfc46', endColorstr='#f75904',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
		-webkit-box-shadow: 0px 1px 0px 0px rgba(255, 255, 255, 0.22);
		border: 1px solid #000;
		box-shadow: 0px 1px 0px 0px rgba(255, 255, 255, 0.22);
		display: inline-block;
		height: 30px;
		line-height: 30px;
		padding: 5px 10px;
		text-decoration: none;
		color: #000;
		width:160px;
		font-family: arial;
		font-size: 14px;
		line-height: 30px;
		cursor: pointer;
		border-radius: 5px 5px 5px 5px;
    }
</style>
<?php $this->widget("application.widgets.SearchBox", array('searchUrl' => "#")); ?>
<?php $this->widget("application.widgets.Header", array('msisdn' => $this->msisdn, 'subscriber' => $this->subscriber, 'usingServices' => $this->usingServices)); ?>
<div  id="main_page" data-theme="a" class="box_content">
            <div id="slide">
                <h3 id="title_slide">CHƯƠNG TRÌNH KHUYẾN MẠI</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
            <div class="content-items"  style="color:black;" align="center">
               <img class="img_content" alt="Chương trình khuyên mại" src="<?php echo Yii::app()->theme->baseUrl;?>/images/km_mobiphim.png" />
               <span class="info_text">Cách thức tham gia:</span>
               <a class="button_dk" href="<?php echo Yii::app()->baseUrl; ?>/account">Đăng ký ngay</a>
               <span class="info_text">Hoặc</span>
               <div align="center" class="box_huongdan">
               		<p >SOẠN <span class="color_dacam"">DK PHIM</span> GỬI <span class="color_dacam">9033</span></p>
               		<br/>
               		<p style="font-size: 1em">Chỉ với <span class="color_dacam">2000đ</span>/1ngày</p>
               </div>
            </div>
            
            <div align="center" class="content-items" style="color: #969696; margin: 40px;">
               		<p style="text-align: center;">Miễn phí cước data</p>
               		<p style="text-align: center;">(GPRS/3G khi sử dụng dịch vụ Mobiphim)</p>
					<a class="button" href="<?php echo Yii::app()->baseUrl; ?>/news/thongtinkhuyenmai">Thông tin chi tiết</a>               		
            </div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
