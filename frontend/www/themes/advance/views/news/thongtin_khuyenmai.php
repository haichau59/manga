<?php
$this->pageTitle = "mobiphim - Thông tin khuyến mại";
?>
<style type="text/css">
    p { text-align:justify;font-weight:normal;margin-bottom:10px;}
    li { list-style:disc; }
    strong, b { font-weight:bold; }
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        font-size: 14px;
    }
    caption { font-size: larger; margin: 1em auto; }
    th, td { padding: .75em; }
    td { color: #000; font-weight: normal; width: 0%; }
    th {
        background: -webkit-linear-gradient(#3f3f3f,#777);
        background: -moz-linear-gradient(#3f3f3f,#777);
        background: -o-linear-gradient(#3f3f3f,#777);
        background: -ms-linear-gradient(#3f3f3f,#777);
        background: linear-gradient(#3f3f3f,#777);
        color: #fff;
    }
    th:first-child { border-radius: 9px 0 0 0; }
    th:last-child { border-radius: 0 9px 0 0; }
/*     tr:last-child td:first-child { border-radius: 0 0 0 9px; } */
/*     tr:last-child td:last-child { border-radius: 0 0 9px 0; } */
    tr:nth-child(odd) { background: #dedede; }
    tr:nth-child(even) { background: #f0f0f0; }
</style>
<?php $this->widget("application.widgets.SearchBox", array('searchUrl' => "#")); ?>
<?php $this->widget("application.widgets.Header", array('msisdn' => $this->msisdn, 'subscriber' => $this->subscriber, 'usingServices' => $this->usingServices)); ?>
<div  id="main_page" data-theme="a" class="box_content">
            <div id="slide">
                <h3 id="title_slide">Thông tin khuyến mại</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
               <div class="content" style="margin: 0px;  color: white;">
                <div class="content-items">
                	<div class="clear"></div>
                    <p class="style2" align="center" style="text-align:center;">
						<font color="#FFFFFF" size="4">
						QUY ĐỊNH TÍNH ĐIỂM THAM GIA CHƯƠNG TRÌNH
						<br>“Xem phim đỉnh cao - Trúng giải thưởng lớn”  
				
						</font></p>
                    <p>
                    <br />
                    <p>1 - Trong thời gian diễn ra chương trình, các thuê bao Vinaphone khi thực hiện Đăng ký mới/Gia hạn gói cước hoặc Xem/Tải/Tặng Phim trên wapsite http://mobiphim.vn (có tính cước thành công) sẽ nhận được số điểm tích lũy tương ứng với giá cước, cụ thể:</p>
					<div align="center">
					<table>
						<thead>
							<tr>
								<th>TT</th>
								<th>Giao dịch</th>
								<th>Giá cước</th>
								<th>Số điểm tích lũy</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Xem/Tặng Phim</td>
								<td>2000đ</td>
								<td>02</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Đăng ký/Gia hạn PHIM</td>
								<td>2000đ</td>
								<td>02</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Đăng ký/Gia hạn PHIM7</td>
								<td>10.000 đ</td>
								<td>10</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Đăng ký/Gia hạn PHIM30</td>
								<td>20.000 đ</td>
								<td>20</td>
							</tr>
						</tbody>
					</table>
				</div>
				<p><strong>Ghi chú:</strong> Thuê bao thực hiện thành công giao dịch có phát sinh cước X.000 đồng, khách hàng sẽ nhận được X điểm tích lũy tương ứng.</p>
				<p>- Cuối chương trình, thuê bao tích lũy nhiều điểm thưởng nhất sẽ nhận được giải thưởng theo cơ cấu giải thưởng hàng tuần trên mobiphim từ Vinaphone.</p>
				<p>- Trường hợp các thuê bao có số điểm như nhau, giải thưởng được trao cho thuê bao có lượt giao dịch sớm nhất (tính theo lượt giao dịch thành công cuối cùng).</p>
				<p>2-     Các giải thưởng sẽ được xác định theo từng Tuần, cơ cấu giải thưởng và thời gian được qui định như sau:</p>
				<div align="center">
					<table>
						<thead>
							<tr>
								<th>Thời gian khuyến mại</th>
								<th>Loại giải</th>
								<th>Số lượng</th>
								<th>Giá trị mỗi giải thưởng</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td rowspan="3">Tuần 1: Từ 15/04 đến 21/04/2013</td>
								<td>Giải Nhất</td>
								<td>01</td>
								<td>5.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Nhì</td>
								<td>02</td>
								<td>1.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Ba</td>
								<td>15</td>
								<td>01 thẻ cào Vinaphone mệnh giá 200.000 đồng</td>
							</tr>
							<tr>
								<td rowspan="3">Tuần 2: Từ 22/04 đến 28/04/2013</td>
								<td>Giải Nhất</td>
								<td>01</td>
								<td>5.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Nhì</td>
								<td>02</td>
								<td>1.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Ba</td>
								<td>15</td>
								<td>01 thẻ cào Vinaphone mệnh giá 200.000 đồng</td>
							</tr>
							<tr>
								<td rowspan="3">Tuần 3: Từ 29/04 đến 05/05/2013</td>
								<td>Giải Nhất</td>
								<td>01</td>
								<td>5.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Nhì</td>
								<td>02</td>
								<td>1.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Ba</td>
								<td>15</td>
								<td>01 thẻ cào Vinaphone mệnh giá 200.000 đồng</td>
							</tr>
							<tr>
								<td rowspan="3">Tuần 4: Từ 06/05 đến 15/05/2013</td>
								<td>Giải Nhất</td>
								<td>01</td>
								<td>5.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Nhì</td>
								<td>02</td>
								<td>1.000.000 đồng</td>
							</tr>
							<tr>
								<td>Giải Ba</td>
								<td>15</td>
								<td>01 thẻ cào Vinaphone mệnh giá 200.000 đồng</td>
							</tr>
						</tbody>
					</table>
				</div>
                </div>
            </div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>

