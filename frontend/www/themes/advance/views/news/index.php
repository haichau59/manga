<?php
$this->pageTitle = "mobiphim - Tin Tức";
?>
<style type="text/css">
    p { text-align:justify;font-weight:normal;margin-bottom:10px;}
    li { list-style:disc; }
    strong, b { font-weight:bold; }
</style>
<?php $this->widget("application.widgets.SearchBox", array('searchUrl' => "#")); ?>
<?php $this->widget("application.widgets.Header", array('msisdn' => $this->msisdn, 'subscriber' => $this->subscriber, 'usingServices' => $this->usingServices)); ?>
<div  id="main_page" data-theme="a">
            <div id="slide">
                <h3 id="title_slide">Tin Tức</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
            <div class="content-items"  style="color:black; background-color: #FFF;">
               <?php echo $content; ?>
            </div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
