<div data-role="panel" class="jqm-nav-panel jqm-navmenu-panel"
     data-position="left" data-display="push" data-theme="b"
     id="sliding_menu" data-swipe-close="false" >
    <ul data-role="listview" data-theme="b" data-divider-theme="b"
        data-icon="false" data-global-nav="docs" class="jqm-list">
        <li>
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_mobiphone_small.png" style="width: auto;height: 30px; float: left; margin-left: 5px; margin-top: 5px;">
            <a href="<?php echo Yii::app()->baseUrl; ?>/account" >TÀI KHOẢN CỦA BẠN</a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->baseUrl ?>/" data-ajax="false">Trang chủ</a>
        </li>
        
        <li data-role="list-divider">TOP TRUYỆN</li>
        <li><?php echo CHtml::link("Truyện mới nhất", array('/manga/browse/order/newest')); ?></li>
        <li><?php echo CHtml::link("Xem nhiều nhất", array('/manga/browse/order/most_viewed')); ?></li>

        <li data-role="list-divider">TRUYỆN THEO THỂ LOẠI</li>

        <?php
        /* @var $cat VodCategory */
        $i = 0;
        $lengthCat = count($categories);
        foreach ($categories as $cat) {
            $i++;
            ?>
            <li><?php echo CHtml::link($cat->display_name, array("/manga/browse/category/" . $cat->id), array('data-ajax' => "false")); ?></li>
            <?php if ($i < $lengthCat) { ?> 
            <?php } ?>
<?php } ?>

<!--         <li data-role="list-divider">LIÊN KẾt KHÁC</li> -->
<?php if ($moibleNumber != '') { ?>
            <li><?php echo CHtml::link("Gửi phản hồi", array('/site/feedback')) ?></li>
<?php } ?>
        <!--<li><a href="#">Ứng Dụng mobiphim</a></li>-->
        <!--<li><a href="<?php echo Yii::app()->baseUrl ?>/account/service" >Giới thiệu dịch vụ</a></li>-->
        <!-- <li><a href="<?php echo Yii::app()->baseUrl ?>/news" >Tin tức</a></li> -->
<!--         <li><a href="http://www.facebook.com/Mobiphim" target="blank">mobiphim facebook</a></li> -->
<!--         <li><a href="http://wap.Vinaphone.com.vn" target="blank">Vinaphone portal</a></li> -->
    </ul>
</div>
