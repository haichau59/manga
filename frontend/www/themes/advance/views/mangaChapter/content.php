<?php
/** @var $this DefaultController */
$this->pageTitle = "mobiphim - All pages";
// $hasService = count($this->usingServices) > 0 ? true : false;

// print_r($allPages); //from MangaChapterController - actionGetAllPage

// $allPages = '{
//   "one": "Singular sensation",
//   "two": "Beady little eyes",
//   "three": "Little birds pitch by my doorstep"
// }';
?>

<!DOCTYPE body PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>

<?php $this->widget("application.widgets.Header", array('subscriber' => $this->subscriber, 'msisdn' => $this->msisdn)); ?>

<div style ="text-shadow:none; margin-top: 10px;" id="main_page" data-theme="a">
        <div data-role="content" class="jqm-content" >
            <div class="content-top">
	                            <h3 class="name" align="center">
					<?php echo CHtml::encode($chapter->display_name); ?>
	                            </h3>
			</div>
		</div>
	</div>
	<?php 
		foreach ($allPages as $page) {
    ?>
	<div>
			<p id = <?php echo $page->page_number; ?>><img class="lazy" src="img/grey.gif" data-original="<?php echo $page->image_url1; ?>" width="100%"></p>
	</div>
	<?php }	?>
</body>
</html>

<script>
//$("img.lazy").lazyload({ threshold : 500 });
$("img.lazy").show().lazyload();
</script>