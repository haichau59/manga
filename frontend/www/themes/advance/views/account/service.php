<?php
$this->pageTitle = "mobiphim - Giới thiệu dịch vụ";
?>
<style type="text/css">
    table, tr, th, td {
        text-align: center;
    }
</style>

<div style="text-shadow:none; color: #FFF;" id="main_page" data-theme="a">
        <div id="slide">
                <h3 id="title_slide">Giới thiệu dịch vụ</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
        </div>
        <div class="tab-content fnRTAccountService" id="fnRTAccountService" >
            <div class="content">
                <div class="content-items">
                    <p><b>I.&nbsp;Giới thiệu</b><br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mobiphim&nbsp;l&agrave; dịch vụ gi&uacute;p bạn xem hoặc tặng c&aacute;c bộ phim hay trực tiếp tr&ecirc;n m&aacute;y điện thoại di động qua mạng Vinaphone.<br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu số dịch vụ:&nbsp;<span class="highlight">1556</span><br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wapsite dịch vụ : <a style="color: red; href="http://mobiphim.vn">http://mobiphim.vn</a></p>

                    <p><br />
                        <b>II.&nbsp;Gi&aacute; cước</b><br />
                        <b><em>2.1.&nbsp;&nbsp;Bảng cước</em></b><br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cước gửi tin đến<strong>&nbsp;1556</strong>:&nbsp;<b>MIỄN PH&Iacute;</b><br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cước dữ liệu (data) khi sử dụng dịch vụ:&nbsp;<b>MIỄN PH&Iacute;</b>.<br />
                        -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cước dịch vụ = Cước Thu&ecirc; bao + Cước Nội dung</b><br />
                        Cước thu&ecirc; bao, cước nội dung được quy định cụ thể như sau:</p>

                    <table align="center" border="1" cellpadding="0" cellspacing="0" style="width:100%;">
                        <tbody>
                            <tr>
                                <td rowspan="2">
                                    <p>Nội dung</p>
                                </td>
                                <td colspan="3">
                                    <p align="center"><strong>G&oacute;i dịch vụ</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>Mặc định</td>
                                <td>
                                    <p><strong>PH7</strong></p>
                                </td>
                                <td>
                                    <p><strong>PH30</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Cước thu&ecirc; bao</p>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <p>5.000đ</p>
                                </td>
                                <td>
                                    <p>15.000đ</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <p>Cước nội dung</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Xem phim</p>
                                </td>
                                <td>
                                    <p><strong>0-5000đ</strong></p>
                                </td>
                                <td>
                                    <p><strong>Miễn ph&iacute;</strong></p>
                                </td>
                                <td>
                                    <p><strong>Miễn ph&iacute;</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Thời gian sử dụng của g&oacute;i cước</p>
                                </td>
                                <td>
                                    <p>&nbsp;</p>
                                </td>
                                <td>
                                    <p>07 ng&agrave;y</p>
                                </td>
                                <td>
                                    <p>30 ng&agrave;y</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Đăng k&yacute;</p>
                                </td>
                                 <td>
                                    <p>&nbsp;</p>
                                </td>
                                <?php foreach ($this->services as $s) { ?>
                                <td>
                                    <p><a style="color: red; "href="#" onclick="confirmPackage('<?php echo Yii::app()->baseUrl ?>/account/subscribe?id=<?php echo urlencode($this->crypt->encrypt($s->id)); ?>', '' )">Tại đ&acirc;y</a></p>
                                </td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>

                    <div style="clear:both;">&nbsp;</div>
                    Ghi ch&uacute;:&nbsp;<br/>
                    -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Các gói cước khi hết thời gian sử dụng sẽ được tự động gia hạn<br/>
                    -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giá cước của từng nội dung (Phim) sẽ được ghi rõ trên wapsite dịch vụ.<br/>
                    &nbsp;<br />
                    <b><em>2.2.&nbsp;&nbsp;Phương thức t&iacute;nh cước</em></b><br />
                    -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cước sử dụng dịch vụ sẽ được cộng v&agrave;o h&oacute;a đơn cước h&agrave;ng th&aacute;ng đối với thu&ecirc; bao trả sau v&agrave; trừ v&agrave;o t&agrave;i khoản ch&iacute;nh đối với thu&ecirc; bao trả trước.<br />
                    -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cước đăng k&yacute; g&oacute;i dịch vụ, mua nội dung được trừ ngay khi bạn x&aacute;c nhận giao dịch.<br />
                    -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đối với trường hợp thu&ecirc; bao đang sử dụng c&aacute;c g&oacute;i cước &nbsp;PH7, PH30:<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;Khi hết thời gian sử dụng, g&oacute;i cước sẽ được tự động gia hạn nếu bạn kh&ocirc;ng y&ecirc;u cầu hủy g&oacute;i cước.<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;Bạn được miễn cước khi Xem phim.<br />

                </div>
            </div>


        </div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
