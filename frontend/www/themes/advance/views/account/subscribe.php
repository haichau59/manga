<?php
/** @var $this DefaultController */
$this->pageTitle = "mobiphim - Đăng ký";
?>
<style type="text/css">
    .content-items p{
        color: white !important;
    }
</style>

<div  style =" text-shadow: none; "id="main_page" data-theme="a">
            
            <div id="slide" style="text-align: center;">
                <h3 id="title_slide">Thông tin tài khoản</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
            <div id="detail" align="center">
            <?php if ($this->msisdn != '') { ?>
                <div class="box_account">
                    <div class="thongbao">Xin chào thuê bao  
                           <span style ="color: #2489ce;"class="mau"><?php echo $this->msisdn ?></span>
                    </div>
                    
                    <?php if ($responseToUser != '') {
                        echo '<div class="content-items" style="color:white;">' . $responseToUser . '</div>';
                    } ?>

                    <?php
                        if ($this->subscriber == null || count($usingServices) == 0) {
                            ?>
                            <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/buy_icon.png" />
                            <div class="thongbao">
                                <span><b>Quý khách chưa đăng ký gói cước<br />
                                         Quý khách có thể chọn một trong những gói cước sau: 
                                    </b>
                                </span>
                            </div>
                            <?php
                        } else {
                            $service = $usingServices[0];
                            $expdate = new DateTime($service->expiry_date); ?>
                            <div id="ticket"><span><?php echo CHtml::encode($service->service->display_name); ?></span></div>
                            <div class="thongbao">
                            <?php if ($service->isExpired()) {
                                echo "Gói cước đang sử dụng: <span class='mau'>" . CHtml::encode($service->service->display_name) . "</span><br/>Đã hết hạn ngày: <span class='mau'>" . $expdate->format('d/m/Y h:i:s') . "</span><br/>";
                            } else {
                                echo "Gói cước đang sử dụng: <span class='mau'>" . CHtml::encode($service->service->display_name) . "</span><br/>Ngày hết hạn: <span class='mau'>" . $expdate->format('d/m/Y h:i:s') . "</span><br/>";
                            } ?>
                            </div>
                  <?php } ?>      
               </div>
                <?php if ($this->subscriber == null || count($usingServices) == 0) { ?>
                        <div class="box_ticket_1">
                                 <div class="ui-grid-a" style =" margin-bottom: 15px; margin-top: 15px;" >
                                <?php foreach ($this->services as $s) { ?>
                                        <?php if($s->display_name == 'PH7') { ?>
                                        <div class="ui-block-a" align="center">
                                        <div class="service_block">
                                            <a  href="#" onclick="confirmPackage('<?php echo Yii::app()->baseUrl ?>/account/subscribe?id=<?php echo urlencode($this->crypt->encrypt($s->id)); ?>', <?php echo CJavaScript::encode($s->description); ?> )" class=".ui-grid-a" title="<?php echo CHtml::encode($s->description); ?>">

                                                <div class="ui-bar ui-bar-d jqm-service-bar" >
                                                        <h3><b><?php echo CHtml::encode($s->display_name); ?></b></h3>
                                                </div>

                                                <div class="time_t"><span style="color: #96a7bb; margin-top: 5px;">Gói</span> <?php echo $s->using_days; ?> Ngày</div>
                                                <div class="cost_t" style="color:#009feb; margin-top: 5px; margin-bottom: 10px;"><?php echo intval($s->price); ?> d</div>
                                            </a>
                                        </div>
                                        </div>
                                         <?php } else {?>
                                         <div class="ui-block-b" align="center">
                                           <div class="service_block">
                                            <a  href="#" onclick="confirmPackage('<?php echo Yii::app()->baseUrl ?>/account/subscribe?id=<?php echo urlencode($this->crypt->encrypt($s->id)); ?>', <?php echo CJavaScript::encode($s->description); ?> )" class=".ui-grid-a" title="<?php echo CHtml::encode($s->description); ?>">

                                                <div class="ui-bar ui-bar-d jqm-service-bar" >
                                                        <h3><b><?php echo CHtml::encode($s->display_name); ?></b></h3>
                                                </div>

                                                <div class="time_t"><span style="color: #96a7bb; margin-top: 5px;">Gói</span> <?php echo $s->using_days; ?> Ngày</div>
                                                <div class="cost_t" style="color:#009feb; margin-top: 5px; margin-bottom: 10px;"><?php echo intval($s->price); ?> d</div>
                                            </a>
                                        </div>
                                         </div>

                                        <?php }?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } 
                   } else { ?>
                        <div class="box_account">
                            <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/dangky_icon.png" />
                            <div class="thongbao">
                                <span style="color: red; text-shadow:none;">
                                    <b >Không nhận diện được thuê bao. Xin vui lòng truy cập dịch vụ bằng 3G/EDGE của Vinaphone, hoặc đăng nhập bằng
                                        <a style="color: white;" href="<?php echo Yii::app()->request->baseUrl; ?>/account/login">wifi tại đây.</a>
                                    </b>
                                </span>
                            </div>
                        </div>
            <?php } ?>
            </div>
        <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
