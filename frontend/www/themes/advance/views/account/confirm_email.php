<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Confirm email';
$this->breadcrumbs = array(
    'Confirm',
);
?>
<style>
.radio_select_type {
	margin: 5px;
}
</style>
<?php $this->widget("application.widgets.SearchBox", array('searchUrl' => "#")); ?>
<?php $this->widget("application.widgets.Header", array('noAccountInfo' => true, 'msisdn' => $this->msisdn, 'subscriber' => $this->subscriber)); ?>
<div  id="main_page" data-theme="a">
    <div id="detail" align="center">
        <img width="115" src="<?php echo $status_img ?>" />
        <p style="margin-top: 10px; color: white;"><?php echo $message ?></p>
    </div>
    <div id="detail" align="center"></div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>


