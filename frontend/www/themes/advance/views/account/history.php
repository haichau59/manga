<?php
/** @var $this AccountController */
$this->pageTitle = "mobiphim - Chi tiết giao dịch";
?>
<style type="text/css">
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        font-size: 14px;
    }
    caption { font-size: larger; margin: 1em auto; }
    th, td { padding: .75em; }
    td { color: #000; font-weight: normal; width: 100%; }
    th {
        background: -webkit-linear-gradient(#3f3f3f,#777);
        background: -moz-linear-gradient(#3f3f3f,#777);
        background: -o-linear-gradient(#3f3f3f,#777);
        background: -ms-linear-gradient(#3f3f3f,#777);
        background: linear-gradient(#3f3f3f,#777);
        color: #fff;
    }
    th:first-child { border-radius: 9px 0 0 0; }
    th:last-child { border-radius: 0 9px 0 0; }
    tr:last-child td:first-child { border-radius: 0 0 0 9px; }
    tr:last-child td:last-child { border-radius: 0 0 9px 0; }
    tr:nth-child(odd) { background: #dedede; }
    tr:nth-child(even) { background: #f0f0f0; }
</style>

<div  style =" text-shadow:none;"id="main_page" data-theme="a">
            <div id="slide" style ="text-align: center;">
                <h3 id="title_slide">Tra cứu giao dịch</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
            <div  class="lyric" style="margin-bottom:10px; width: 100%">
                <form method="get" action="" name="transactionSearch" id="transactionSearch">
                    <div style="margin: 3px;">
                        <div style="float: left; width: 80px; color:white;margin-left: 1%;"><label>Từ ngày</label></div>
                        <div style="margin-left:90px; width: 40%;">
                            <input style="color: #333;" type="text" value="<?php echo $from_date; ?>" style="max-width: 160px;" name="from_date"/>
                        </div>
                    </div>
                    <div style="margin: 3px;">
                        <div style="float: left; width: 80px; color:white; margin-left: 1%;"><label>Đến ngày</label></div>
                        <div style="margin-left:90px; width: 40%;">
                            <input style="color: #333;" type="text" value="<?php echo $to_date; ?>" style="max-width: 160px;" name="to_date"/>
                        </div>
                    </div>
                    <div style="margin: 5px 10px 5px 90px; width: 40%; "><input class="button" type="submit" value="Xem" /></div>
                </form>
                <table>
<div class="content-midview">
                    <ul data-role="listview" data-inset="true" class="ui-listview ui-listview-inset ui-corner-all ui-shadow vina-class">
                        <?php
                        $records = count($transaction);
                        if ($records > 0) {
                            
                            $pt = array("", "Đăng ký", "GIA HẠN", "HỦY", "BỊ HỦY");
                            foreach ($transaction as $t) { ?>
                                <?php
                                $type = isset($pt[$t['purchase_type']]) ? $pt[$t['purchase_type']] : "";
                                if ($t['using_type'] > USING_TYPE_REGISTER && $t['purchase_type'] == PURCHASE_TYPE_NEW)
                                    $type = "Mua";
                                $tDate = DateTime::createFromFormat('Y-m-d H:i:s', $t['create_date']);
                                ?>
				<li class="ui-li ui-li-static ui-btn-up-d ui-li-has-thumb ui-corner-top">
				<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/mua.png" class="ui-li-thumb ui-corner-tl">
				<span style="text-shadow:none; margin-left: 10px;"><?php echo CHtml::encode($type); ?>: Gói cước <?php echo CHtml::encode($t['description']); ?></span>
                                <p><span><?php echo CHtml::encode($tDate->format('d/m/Y H:i:s')); ?></span>
                               
				
                                    <span style="float:right; color: #009feb; text-shadow: none;"><?php echo intval($t['cost']) . 'đ'; ?></span>

                                </p>
			</li>

                               
                                <?php
                            }
                        } else {
                            ?>
                            <tr><td colspan="4">Không tìm thấy giao dịch nào.</td></tr>
                            <?php
                        }
                        ?>
                    </ul>
</div>
                </table>
                <br/>
                <?php if ($records > 0) {
                    $this->widget("application.widgets.Pager", array('pager' => $pager, 'baseUrl' => Yii::app()->request->url, 'delimiter' => '='));
                } ?>
            </div>

<?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
