<?php
/** @var $this AccountController */
$this->pageTitle = "mobiphim - Tài khoản";
?>
<script type="text/javascript">
function updateEmail(show){
	var content = document.getElementById('account_infomation');
	var updateEmail = document.getElementById('update_email');
	if(show){
		content.style.display = 'none';
		updateEmail.style.display = 'block';
		closeNofiticate();
	}else{
		content.style.display = 'block';
		updateEmail.style.display = 'none';
	}
	return false;
}
</script>

<div  style="text-shadow: none; "id="main_page" data-theme="a">
            <div id="slide" style="text-align: center;">
                <h3 id="title_slide">Quản lý tài khoản</h3>
                <div class="line_c"  align="center" > 
                    <div class="line_c1"></div>
                </div>
            </div>
            <div id="account_infomation" class="user_detail" align="center">
            <?php if (($this->msisdn != '') || ($this->msisdn == '')){ ?>
                    <div class="box_account">
                        <div class="jqm-content-center">
      
                        <div class="thongbao">
                           
                            Xin chào thuê bao
                            <span style ="color: #2489ce;"class="mau"><?php echo $this->msisdn ?></span>
                            <?php if($email == ''){?>
                            <br/>
                            Xin vui lòng cập nhật thông tin email của bạn
                            <a href="#" onclick="return updateEmail(true);" class="mau">tại đây</a>
                            <?php } else { ?>
                            <br/>
                            Email của bạn
                            <a href="#" onclick="return updateEmail(true);" class="mau"><?php echo $email; ?></a>
                            <?php } ?>
                        </div>
                        <?php
                        if ($this->subscriber == null || count($usingServices) == 0) {
                            ?>
                            <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/buy_icon.png" />
                            <div class="thongbao">
                                <span><b>Quý khách chưa đăng ký gói cước<br />
                                Quý khách có thể chọn một trong những gói cước sau:</b></span>
                            </div>
                            <?php
                        } else {
                            $service = $usingServices[0];
                            $expdate = new DateTime($service->expiry_date);
                            ?>
                             <?php if($service->service->display_name == 'PH30') {?>
                            <img style =" width: 300px; height: auto;" src="<?php echo Yii::app()->theme->baseUrl ?>/images/account_goi_cuoc_ph30_big.png" />
                            <?php } else { ?>
                             <img style =" width: 300px; height: auto;" src="<?php echo Yii::app()->theme->baseUrl ?>/images/account_goi_cuoc_ph7_big.png" />
                             <?php } ?>
                            
                            <div class="thongbao">Quý khách đang dùng gói cước: <span style ="color: #2489ce;"><?php echo CHtml::encode($service->service->display_name); ?></span><br />
                                        Hạn sử dụng cho đến: <span style ="color: #2489ce;"><?php echo $expdate->format("d/m/Y H:i:s") ?></span>
                            </div>
                            <div class="line_acount">&nbsp;</div>
                            <div class="jqm-content-right">
                                  - Gói cước sẽ được tự động gia hạn ngay sau khi hết thời gian<br />
                                  - Miễn phí cước data (GPRS/3G khi sử dụng dịch vụ Mobiphim)
                                  <?php if ($this->subscriber != null && count($usingServices) > 0) { ?>
                                  <br/>
                                  - Bạn có thể hủy dịch vụ <a style="color:#02EED9;" href="javascript:confirmCancel('<?php echo Yii::app()->baseUrl . "/account/cancel"; ?>');" >tại đây</a>, sau khi huỷ thì gói cước của quý khách sẽ là gói mặc định.
                                   <?php } ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                       	<div class="deliver"></div>
			<div class="ui-grid-a" align="center">


                            <div style="align: center">
                                <a id="round" href="<?php echo Yii::app()->request->baseUrl; ?>/account/history" data-role="button" style="text-shadow:none;"  data-theme="a">Lịch sử giao dịch</a>
				</div>
			</div>
                        
            <?php  }else { ?>
                        <div class="box_account">
                            <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/dangky_icon.png" />
                            <div class="thongbao">
                                <span style="color: red; text-shadow:none;">
                                    <b >Không nhận diện được thuê bao. Xin vui lòng truy cập dịch vụ bằng 3G/EDGE của Vinaphone, hoặc đăng nhập bằng
                                        <a style="color: white;" href="<?php echo Yii::app()->request->baseUrl; ?>/account/login">wifi tại đây.</a>
                                    </b>
                                </span>
                            </div>
                        </div>
            <?php } ?>
                    </div>
            </div>
            
            <div id="update_email" class="user_detail" align="center" style="display:none;">
				<form name="update_email_form" id="update_email_form" method="POST" style="margin-top: 50px;"
					action="<?php echo $this->createUrl("/account"); ?>">
					<p style="color: white;">Nhập email</p>
					<input style ="color: black;"type="text" name="email" id="email"
						value="<?php echo $email; ?>" />
					<div style="margin-top: 10px;">
						<input type="submit" name="cancel" value="Thoát" class="button" onclick="return updateEmail(false);" style="min-width: 90px;margin: 0 5px;"/>
						<input type="submit" name="submit" value="Cập nhật" class="button" style="min-width: 90px;margin: 0 5px;" />
					</div>
				</form>
			</div>
            <div id="detail" align="center"></div>

	<?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>
