<?php
/**
 * @var $this AccountController
 */
$this->pageTitle = "mobiphim - Tặng gói cước";
?>
<style type="text/css">
    .content-items p{
        color: white !important;
    }
</style>
<script type="text/javascript">
    function validateGiftForm() {
        if (document.gift_form.gift_price.value.length == 0) {
            alert("Xin vui lòng chọn gói cước cần tặng");
            return false;
        }
        if (document.gift_form.gift_number.value.length == 0) {
            alert("Số điện thoại không được để trống");
        } else {
            if (document.gift_form.gift_number.value.match(/^\d+/)) {
                if (document.gift_form.gift_number.value.match(/^(84|0)(91|94|123|124|125|127|129)\d{7}$/)) {
                    return confirm("Quý khách đã chọn tặng gói cước với giá " + document.gift_form.gift_price.value + " VNĐ. Đồng ý?");
                } else {
                    alert("Số điện thoại không hợp lệ, vui lòng chọn thuê bao của Vinaphone và thử lại");
                }
            } else {
                alert("Số điện thoại không hợp lệ, vui lòng nhập lại");
            }
        }
        return false;
    }
    function setPrice(price, s_id) {
        document.gift_form.gift_price.value = price;
        $("#service_id_" + s_id).attr('checked', true);
    }
</script>
<?php $this->widget("application.widgets.SearchBox"); ?>
<?php $this->widget("application.widgets.Header", array('noAccountInfo' => true)); ?>
<div  id="main_page" data-theme="a">

    <div id="slide">
        <h3 id="title_slide">TẶNG GÓI CƯỚC</h3>
        <div class="line_c"  align="center" > 
            <div class="line_c1"></div>
        </div>
    </div>

    <div id="detail" align="center">
            <?php if ($this->msisdn != '') { ?>
                    <div class="box_account">
                        <div class="thongbao">
                            Chọn gói cước cần tặng:
                        </div>
                    </div>
                    <form name="gift_form" id="gift_form" method="POST" action="<?php echo $this->createUrl("/account/gift"); ?>" onsubmit="return validateGiftForm();">
                                <div class="box_ticket_1">
                                    <?php foreach ($this->services as $s) { ?>
                                   <a  href="javascript:void(0);" onclick="setPrice(<?php echo intval($s->price); ?>, <?php echo $s->id ?>)" class="box_ticket" title="<?php echo CHtml::encode($s->description); ?>">
                                            <div class="ten_t"><?php echo CHtml::encode($s->display_name); ?></div>
                                            <div class="time_t"><span>Gói</span> <?php echo $s->using_days; ?> Ngày</div>
                                            <div class="cost_t"><?php echo intval($s->price); ?>đ</div>
                                            <div class="select_box"><input type="radio" name="service_id" value="<?php echo $s->id; ?>" id="service_id_<?php echo $s->id; ?>" /></div>
                                   </a>
                                    <?php } ?>
                                </div>
                                <p style="margin: 5px 0; color: white;">Nhập số thuê bao Vinaphone cần tặng:</p>
                            <img src="<?php echo $this->createUrl("/images/phone.png"); ?>" alt="phone">
                            <input type="text" name="gift_number" id="gift_number" value="" />
                            <input type="hidden" name="gift_price" id="gift_price" value=""/>
                            <input type="submit" name="submit" value="Tặng" class="button" />
                     </form>           
            <?php } else { ?>
                        <div class="box_account">
                            <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/dangky_icon.png" />
                            <div class="thongbao">
                                <b >Không nhận diện được thuê bao. Xin vui lòng truy cập dịch vụ bằng 3G/EDGE của Vinaphone, hoặc đăng nhập bằng 
                                        <a style="color: white;" href="<?php echo Yii::app()->request->baseUrl; ?>/account/login">wifi tại đây.</a>
                                </b>
                            </div>
                        </div>
            <?php } ?>
    </div>
    <div id="detail" align="center"></div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>