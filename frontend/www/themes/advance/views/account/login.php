<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Đăng nhập';
$this->breadcrumbs = array(
    'Login',
);
?>
<style>
.radio_select_type {
	margin: 5px;
	display:inline-block;
}
</style>
<script type="text/javascript">

	function validateInput(){
		mobile_number = document.login_form.mobile_number.value;
        email = document.login_form.input_email.value;
        typeGetPass = $("input:radio[name='type_getpass']:checked").val();
        typeGetPass = 1;
        //Validate mobile
        if(typeGetPass == 1){
	        if(!validateMobileNumber(mobile_number)){
	            alert('Số điện thoại không hợp lệ, vui lòng chọn thuê bao của Vinaphone và thử lại');
	            return false;
	        }
        }else{
        	if(email.length == 0){
				alert('Xin vui lòng nhập email của bạn');
				return false;
			}
        }

        return true;
	}

    function validateLoginForm() {
        if(!validateInput()){
			return false;
         }
            if(document.login_form.password_ota.value.length == 0){
                alert('Xin vui lòng nhập mật khẩu nhận từ sms');
                return false;
            }else{
                return true;
            }
        
        return false;
    }
    
    //Valid mobile number
   
    function validateMobileNumber(mobile_number){
        if (mobile_number.length == 0) {
            return false;
        } else {
            if (mobile_number.match(/^\d+/)) {
                if (mobile_number.match(/^(84|0)(91|94|123|124|125|127|129)\d{7}$/)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }
    
    function get_ota(url){
    	if(!validateInput()){
			return false;
         }
    	mobile_number = document.login_form.mobile_number.value;
        email = document.login_form.input_email.value;
    	typeGetPass = $("input:radio[name='type_getpass']:checked").val();
        typeGetPass = 1;
    	url += 'typeGet=' + typeGetPass + '&';
    	if(typeGetPass == 1){
	        window.location = url + 'mobile_number=' + mobile_number;
        }else{
        	window.location = url + 'email=' + email;
        }
    }

    //Type select: 1: input Moible, 2: input email
    function changeSelectType(type){
        var inputMobile = document.getElementById('input_number');
        var inputEmail = document.getElementById('email');
        var btnGetPass = document.getElementById('get_pass');
		if(type == 1){
			inputMobile.style.display = 'inline-block';
			inputEmail.style.display = 'none';
			btnGetPass.innerHTML = 'Gửi mật khẩu qua SMS';
		}else{
			inputMobile.style.display = 'none';
			inputEmail.style.display = 'inline-block';
			btnGetPass.innerHTML = 'Gửi mật khẩu qua Email';
		}
		return true;
    }
    
</script>
<div style="text-shadow:none;" id="main_page" data-theme="a">
    <div id="detail" align="center">
        
        <img width="115" src="<?php echo Yii::app()->theme->baseUrl ?>/images/wifi_logo.png" />
        <?php if($this->accessType == Controller::$ACCESS_VIA_WIFI && $this->msisdn == ''){ ?>
        <form name="login_form" id="login_form" method="POST" action="<?php echo $this->createUrl("/account/login"); ?>" onsubmit="return validateLoginForm();" style="color:white;">
            <p style="color: #8D8D8D;font-size: 14px;margin-bottom: 14px; text-shadow:none;">  Đăng nhập qua wifi để thưởng thức dịch vụ tốt nhất, hệ thống sẽ tự động gửi mật khẩu đến số điện thoại của bạn qua tin nhắn</p>
           



            <div class="clear"></div>
            <div id="input_number" style="width:  50%; text-align: center; " <?php if($typeGet != 1){ ?>style="display:none; " <?php } ?>>
            	<input style="color: black; text-shadow:none;" type="text" name="mobile_number" id="mobile_number" value="<?php echo $model->username; ?>"  />
            </div>
            <div id="email"  <?php if($typeGet != 2){ ?>style="display:none;width: 50%; text-align: center; " <?php } ?>>
            	<input style="color: black; text-shadow:none;" type="text" name="email" id="input_email" value="<?php echo isset($model)?$model->email:''; ?>" />
            </div>
            <div class="clear"></div>
            <p style="margin-top: 10px; color: white; text-shadow: none; ">Mật khẩu nhận qua SMS</p>
            <div style=" width: 50%; text-align: center;">
            <input style="color: black; text-shadow:none;" style="margin-top: 1px;" type="password" name="password_ota" id="password_ota" value=""/>
            </div>
            <div style="margin-top: 10px;  width: 50%;  text-align: center;" >
                <input type="submit" name="submit" value="Đăng nhập" class="button" />
                <div class="clear" style="margin: 4px;"></div>
                <a data-role="button" href="javascript:void(0);"  id="get_pass" style=" text-shadow: none; font-weight: normal;" name="get_pass" onclick="return get_ota('<?php echo $urlGetOTA; ?>');" >Gửi mật khẩu qua SMS</a>
            </div>
            
        </form>
        <?php } else { ?>
        <p style="color: #96a7bb;font-size: 14px;margin-bottom: 14px;">Bạn đang đăng nhập Wifi với số điện thoại: <?php echo $this->msisdn; ?></p>
        <a href="<?php echo $this->createUrl("/account/logout"); ?>"  style="height: 28px; font-weight: normal;" name="get_pass"  class="button" > Đăng xuất </a>
        <?php } ?>
    </div>
    <div id="detail" align="center"></div>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>


