<?php
/** @var $this DefaultController */
$this->pageTitle = "mobiphim - Trang chủ";
// $hasService = count($this->usingServices) > 0 ? true : false;
?>

	<?php $this->widget("application.widgets.Header", array('subscriber' => $this->subscriber, 'msisdn' => $this->msisdn)); ?>

    <div class="slide_content">
        <div class="m-carousel m-fluid m-carousel-photos"
             style="height: 260px; width: 100%">
            <!-- the slider -->
            <div class="m-carousel-inner">
                <!-- the items -->
                <?php
                $posterUrl = '';
                foreach ($newest as $i => $asset) {
                    if(array_key_exists('cover', $asset)) {
						$posterUrl = $asset['cover'];
					}
                    ?>
                    <a class="m-item" href="<?php echo $this->createUrl("/manga/" . $asset['id']); ?>" data-ajax="false">
                        <img src="<?php echo CHtml::encode($posterUrl); ?>" alt="" style="height:240px;" />
                        <p class="m-caption" style="color:white"><?php echo CHtml::encode($asset['display_name']); ?></p>
                    </a>
                <?php } ?>
            </div>
            <div class="m-carousel-controls m-carousel-hud">
                <a class="m-carousel-prev" href="#" data-slide="prev">Previous</a>
                <a class="m-carousel-next" href="#" data-slide="next">Next</a>
            </div>
            <!-- the controls -->

            <div class="m-carousel-controls m-carousel-bulleted">
                <?php
                for ($i = 1; $i <= count($newest); $i++) {
                    $opts = array('data-slide' => $i);
                    if ($i == 1)
                        $opts['class'] = 'm-active';
                    echo CHtml::link($i, "#", $opts);
                }
                ?>
            </div>
        </div>
    </div>
    <!--<a href="<?php echo Yii::app()->baseUrl; ?>/news/khuyenmai"><img style="width: 100%; height: auto; max-height: 100px;" alt="Khuyến mại" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/mobiphim_km_banner.png"></a>-->




    <a href="<?php echo Yii::app()->baseUrl; ?>/manga/browse/order/newest" data-ajax="false">
        <div class="ui-bar ui-bar-d">
            <img style="width: 20px;height: 18px; float: left; margin-right: 10px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_movie.png" >
            <h3>TRUYỆN MỚI NHẤT</h3>      
            <input type="image" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_more.png" style="width: auto; height:20px;float: right; margin-right: 10px;" value="MoreFilm" data-role="none">
        </div>
    </a>

    <div class="ui-grid-b">
        <?php
        $j = 0;
        foreach ($newest as $i => $asset) {
            $ui_block = "";
            switch ($j%3) {
                case 0:
                    $ui_block = 'ui-block-a';
                    break;
                case 1:
                    $ui_block = 'ui-block-b';
                    break;
                case 2:
                    $ui_block = 'ui-block-c';
                    break;
                default:
                    $ui_block = 'ui-block-a';
            }
            $j++;
        
        ?>
        <div class="<?php echo $ui_block; ?>">
            <?php
            $posterUrl = '';
            if(array_key_exists('cover', $asset)) {
            	$posterUrl = $asset['cover'];
            }
        $this->widget("application.widgets.AssetItem", array('asset' => $asset, 'posterUrl' => $posterUrl));
    ?>
        </div>
        <?php } ?>
    </div>

	<a href="<?php echo Yii::app()->baseUrl; ?>/manga/browse/order/most_viewed" data-ajax="false">
	    <div class="ui-bar ui-bar-d">
	        <img style="width: 20px;height: 18px; float: left; margin-right: 10px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_movie.png" >
	        <h3>XEM NHIỀU NHẤT</h3>      
	        <input type="image" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_more.png" style="width: auto; height:20px;float: right; margin-right: 10px;" value="MoreFilm" data-role="none">
	    </div>
	</a>
	
	<div class="ui-grid-b">
        <?php
        $j = 0;
        foreach ($mostViewed as $i => $asset) {
            $ui_block = "";
            switch ($j%3) {
                case 0:
                    $ui_block = 'ui-block-a';
                    break;
                case 1:
                    $ui_block = 'ui-block-b';
                    break;
                case 2:
                    $ui_block = 'ui-block-c';
                    break;
                default:
                    $ui_block = 'ui-block-a';
            }
            $j++;
        
        ?>
        <div class="<?php echo $ui_block; ?>">
            <?php 
        $posterUrl = $asset['cover'];
        $this->widget("application.widgets.AssetItem", array('asset' => $asset, 'posterUrl' => $posterUrl));
    ?>
        </div>
        <?php } ?>
    </div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.m-carousel').carousel();
    });

</script>
<?php $this->widget("application.widgets.Footer", array('categories' => $this->categories, 'dontGoBack' => true, 'showBanner' => false)); ?>

