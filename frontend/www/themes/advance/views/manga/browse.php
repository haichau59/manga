<?php
$this->pageTitle = "Manga - Danh sách tryện";
$requestUrl = "";
$records = count($assets);
if ($type == 'browse') {
    $requestUrl = $this->createUrl('/manga/browse');
    if (isset($category_id))
        $requestUrl .= "/category/$category_id";
    //$requestWithoutOrder = $requestUrl;

    if ($order_by != "")
        $requestUrl .= "/order/$order_by";
} else {
    $requestUrl = $this->createUrl('/manga/search/q/' . $keyword);
}
?>
<script type="text/javascript">
    $(function() {

    });
</script>

<div  style=" text-shadow : none; color: white;"id="main_page" data-theme="a">
    <?php if ($type == 'search') { ?>
        <div class="content-items" style="color:white;">Kết quả tìm kiếm cho từ khóa: <b><?php echo CHtml::encode($keyword) ?></b></div>
        <?php
    }
    if ($type == 'search' && $records == 0) {
        ?>
        <div class="content-items" style="color:white;">Không tìm thấy truyện nào phù hợp với điều kiện tìm kiếm.</div>
        <div class="content-items" style="color:white;">
            <?php echo CHtml::link("Quay về", "javascript:history.go(-1);"); ?> |
        </div>
        <?php } else {  ?>
        <div class="box-film">
            <div class="box-title">
                        <div class="ui-bar ui-bar-d">
                            <img style="width: 20px;height: 18px; float: left; margin-right: 10px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_movie.png" >
                            <h3><?php echo CHtml::encode(mb_strtoupper($category, "UTF-8")); ?></h3>
                            <input type="image" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_more.png" style="width: auto; height:20px;float: right; margin-right: 10px;" value="MoreFilm" data-role="none">
                        </div>
               
            </div>
            <div class="trailer" style="border-bottom: 1px solid #000; padding-bottom: 5px;"></div>
                            <div class="ui-grid-b">
                            <?php
                           
                                $j = 0;
                                foreach ($assets as $i => $asset) {
                                    $ui_block = "";
                                    switch ($j%3) {
                                        case 0:
                                            $ui_block = 'ui-block-a';
                                            break;
                                        case 1:
                                            $ui_block = 'ui-block-b';
                                            break;
                                        case 2:
                                            $ui_block = 'ui-block-c';
                                            break;
                                        default:
                                            $ui_block = 'ui-block-a';
                                    }
                                    $j++;

                                ?>
                                <div class="<?php echo $ui_block; ?>">
                                    <?php
                                $posterUrl = $asset['posterUrl'];
                                $isFree = true;
                                $this->widget("application.widgets.AssetItem", array('asset' => $asset, 'posterUrl' => $posterUrl, 'isFree' => $isFree));
                            ?>
                                </div>
                                <?php } ?>
                        

                </div>
        </div>
    <?php } ?>
        
    <?php

    if ($records > 0) {
        $this->widget("application.widgets.Pager", array('pager' => $pager, 'baseUrl' => $requestUrl));
    }
    ?>
    <?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
        
</div>

