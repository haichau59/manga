<?php
/** @var $asset VodAsset */
/** @var $this VideoController */
$this->pageTitle = "Manga - " . $asset->display_name;
?>
<style type="text/css">
    caption { font-size: larger; margin: 1em auto; }
</style>


<script type="text/javascript">
    var loadMore = true;
    $(document).ready(function() {
        //Cuong them
          $('#half-demo').raty({
              width: 130,
              path    : '<?php echo Yii::app()->theme->baseUrl;?>/images',
               half    : true,
            starOff : 'star3.png',
            starOn  : 'star1.png',
            starHalf: 'star2.png',
            score: <?php echo $asset->rating ?>,
            readOnly: true
          });
        // end
//        $('.confirmation').click(function(e) {
//            if (!confirm("Quý khách muốn đăng ký gói cước " + $(this).text() + ", xin bấm OK để xác nhận?")) {
//                e.preventDefault();
//            }
//        });
//        $('#purchase_film, #purchase_download').click(function(e) {
//            if (!confirm("Quý khách chọn mua nội dung lẻ, xin bấm OK để xác nhận?")) {
//                e.preventDefault();
//            }
//        });
        
        $('#viewmore').click(function(){
           if(loadMore){
                $(".detail_content").css({ "height":"auto"});
     //           $(this).hide();
                $(this).html('Rút gọn');
                loadMore = false;
           }else{
               $(".detail_content").css({ "height":"192px"});
     //           $(this).hide();
                $(this).html('Xem thêm');
                loadMore = true;
           }
           return false;
      });
     
//     $("#tang_bt").fancybox({
//         topRatio:0.2,
//         minHeight:54
//     });
     $('#rate_star').raty({target: '#title_star',
                           path: "<?php echo Yii::app()->theme->baseUrl; ?>/images/",
                           size     : 24,
                            starOff : 'star3_big.png',
                            starOn  : 'star1_big.png',
                            starHalf: 'star2_big.png',
                           targetKeep: true,
                           targetText: 'Chưa đánh giá',
                           hints: ['Dở ẹc', 'Dở', 'Tạm được', 'Hay', 'Rất hay']
                          });
//
//     //INit star vod
//    $('#star_asset').raty({ readOnly: true, score: <?php echo $asset['rating'] ?>,
//                                              path: "<?php echo Yii::app()->theme->baseUrl;?>/images/"
//                                            });
//
    });
    var base_link = $('#play_video_direct').attr('href');
    
    function rateStar(){
        star =  $('#rate_star').raty('score');
        if(star <= 0 || star > 5){
            aler("Xin vui lòng đánh giá truyện!");
            return false;
        }
        url = '<?php echo Yii::app()->baseUrl; ?>/manga/rateManga';
        $.getJSON(url,{manga: '<?php echo $asset['id']; ?>', rate: star})
        .done(function(json){
            alert(json.message);
            
        })
        .fail(function (jqxhr, textStatus, error){
            var err = textStatus + ', ' + error;
            alert( "Request Failed: " + err);
           
        });
        return false;
    }
</script>

<?php $this->widget("application.widgets.Header", array('subscriber' => $this->subscriber, 'msisdn' => $this->msisdn)); ?>

<div style ="text-shadow:none; margin-top: 10px;" id="main_page" data-theme="a">
        <div data-role="content" class="jqm-content" >
            <div class="content-top">
	            <div class="content-top-left"  style=" position: relative;">
<!-- 	                <a class="poster_img" href="#" > -->
				<img style="height: 180px; width: 120px;"src="<?php echo $posterUrl; ?>" alt="">
	          	</div>
	          	
	            <div class ="content-top-right">
	                            <h3 class="name">
					<?php echo CHtml::encode($asset['display_name']); ?>
	                            </h3>
	
	
	                    <a href="#popuprate" data-rel="popup" data-position-to="window"  data-theme="a" data-transition="pop">
	                        <div id="half-demo" style="margin-bottom: 10px; background-color: #30455E;">
	                        </div>
	                    </a>
	                <div data-role="popup" data-theme="b" id="popuprate" data-overlay-theme="a" data-position-to="window" data-corners="true">
	                    <a href="#" data-rel="back" data-role="button" data-theme="a"
	                       data-icon="delete" data-iconpos="notext" class="ui-btn-left">Close</a>
	                    <div data-role="content" style="color:#fff; text-shadow: none;">
	                        <h3 >Đánh giá của bạn</h3>
	                        <div id="rate_star" ></div>
	                        <div id="title_star" ></div>
	                        <form action="" onsubmit="return rateStar();" class="search">
	                            <input id="fnSearchSubmit" type="submit" name="submit" value="Submit" />
	                        </form>
	                    </div>
	                </div>
	                
	                            <div class="space">
					<span class="textlabel">Thời lượng: </span>
	                                <span class="textinfo"><?php echo (isset($asset['duration']) && $asset['duration'] != '') ? CHtml::encode($asset['duration']) . ' phút ': "Đang cập nhật" ?></span>
	                            </div>
	                            <div class="space">
					<span class="textlabel">Lượt xem: </span>
	                                <span class="textinfo"><?php echo (isset($asset['view_count']) && $asset['view_count'] != '') ? CHtml::encode($asset['view_count']) . ' lượt ': "Đang cập nhật" ?></span>
	                            </div>
	                            <div class="space">
					<span class="textlabel">Thể loại: </span>
	                                <span class="textinfo"><?php echo $catenames;?></span>
	                            </div>
	                            <div class="space">
					<span class="textlabel">Đạo Diễn:  </span>
	                                <span class="textinfo"><?php echo (isset($asset['director']) && $asset['director'] != '') ? CHtml::encode($asset['director']) : "Đang cập nhật" ?></span>
	                            </div >
	                            <div class="space">
					<span class="textlabel">Diễn viên: </span>
	                                <span class="textinfo"><?php echo (isset($asset['actors']) && $asset['actors'] != '') ? CHtml::encode($asset['actors']) : "Đang cập nhật" ?></span>
	                            </div>
	        	</div>
	        </div>
	        <div class="trailer" style="border-bottom: 1px solid #000; padding-bottom: 5px;"></div>
             <div class="content-bottom">
                        <p id="" class="textinfo"><?php echo (isset($asset['description']) && $asset['description'] != '') ? CHtml::encode($asset['description']) : "Đang cập nhật"?></p>
            </div>
    </div>
    
    <div id="container">
  <div id="row">

  	<div id="left">
  		<h4>Tên chapter</h4>
  	</div>

  	<div id="middle" align="center">
  		<h4>Thứ tự</h4>
  	</div>

  	<div id="right" align="center">
    	<h4>Số trang</h4>
  	</div>

	</div>
</div>
    <?php 
    	$listChapter = MangaChapter::model()->findAll("t.manga_id = ".$asset['id']." and t.status = 1 order by t.chapter_order desc");
    	foreach($listChapter as $chapter) {
			?>
			<div id="container">
  <div id="row">

  	<div id="left">
  		<a href = "<?php echo Yii::app()->baseUrl."/mangaChapter/getAllPage/id/".$chapter->id;?>"><?php echo $chapter->display_name;?></a>
  	</div>

  	<div id="middle" align="center">
  		<p><?php echo $chapter->chapter_order;?></p>
  	</div>

  	<div id="right" align="center">
    	<p><?php echo $chapter->page_count;?></p>
  	</div>

	</div>
</div>
		<?php }
    ?>
    
    <div id="relateVOD" class="box-film" >
                <div class="box-title">
                    <div class="ui-bar ui-bar-d">
                        <img style="width: 20px;height: 18px; float: left; margin-right: 10px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_movie.png">
                        <h3>TRUYỆN CÙNG THỂ LOẠI</h3>
                        <input type="image" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_more.png" style="width: auto; height:20px;float: right; margin-right: 10px;" value="MoreFilm" data-role="none">
                    </div>
                </div>
        <div class="trailer" style="border-bottom: 1px solid #000; padding-bottom: 5px;"></div>
                <div class="ui-grid-b">
                            <?php
                            if (count($related) > 0) {
                                $j = 0;
                                foreach ($related as $i => $relatedAsset) {
                                    $ui_block = "";
                                    switch ($j%3) {
                                        case 0:
                                            $ui_block = 'ui-block-a';
                                            break;
                                        case 1:
                                            $ui_block = 'ui-block-b';
                                            break;
                                        case 2:
                                            $ui_block = 'ui-block-c';
                                            break;
                                        default:
                                            $ui_block = 'ui-block-a';
                                    }
                                    $j++;

                                ?>
                                <div class="<?php echo $ui_block; ?>">
                                <?php
                                    $cover = MangaCover::model()->findByAttributes(array("manga_id" => $relatedAsset['id']));
                                    $posterUrl = "";
                                    if($cover != NULL) {
                                    	$posterUrl = $cover->url;
                                    }
                                	$this->widget("application.widgets.AssetItem", array('asset' => $relatedAsset, 'posterUrl' => $posterUrl));
                            	?>
                                </div>
                                <?php }
                    } else {
                        ?>
                        <div class="content-items">
                            <p style="color:black;">Không tìm thấy truyện liên quan</p>
                        </div>
                        <?php }  ?>
                   
                </div>
            </div>
    
<?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>


