<?php 
//    $css_video = '<link href="'.Yii::app()->theme->baseUrl.'/css/mediaelementplayer.css" rel="stylesheet">';
//    array_push($this->array_file, $css_video);
//    $js_video = '<script src="'.Yii::app()->theme->baseUrl.'/js/mediaelement-and-player.min.js"></script>';
//    array_push($this->array_file, $js_video);
?>
<?php

/** @var $asset VodAsset */
/** @var $this VideoController */
$this->pageTitle = "mobiphim - " . $asset->display_name;
$requestUrl = $this->createUrl('/video/watch?id=' . $this->crypt->encrypt($asset['id']));
$hasService = count($this->usingServices) > 0 ? true : false;
if ($asset['is_series']){
    $episodes = VodEpisode::model()->findAllByAttributes(array('status' => 1, 'vod_asset_id' => $asset->id), array('order' => 'episode_order'));
    $episode_count = sizeof($episodes);
}
?>


<div  style ="text-shadow: none"id="main_page" data-theme="a">
        <!--<div class="divider header-bg"><h3 class="dtitle"><?php echo CHtml::encode($asset['display_name']) . ($asset['is_series'] ? " - Tập $episode" : ""); ?></h3></div>-->
        <div style ="color: #00ADFF; font-weight: bold;"class="box-title" align="center">
            <h3  class="video_title_header"><?php echo CHtml::encode($asset['display_name']) . ($asset['is_series'] ? " - Tập $episode" : ""); ?></h3>
        </div>
            <div>
                <div id="detail_play" align="center">
                    <video id="videoplayer" controls  preload="auto" width="320" height="240"  poster="<?php echo CHtml::encode($posterLandUrl); ?>"
                           style="min-height:240px;">
                        <source src="<?php echo CHtml::encode($url) ?>" type='video/mp4'>
                    </video>
<!--                    <a  href="<?php echo CHtml::encode($url) ?>">
                        <img class="film_img" src="<?php echo $posterLandUrl; ?>" />
                        <img id="icon_play" border="0" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon_play.png" />
                    </a>-->
                    
                </div>
                <?php if ($asset['is_series']) {  ?>
                    <div style="padding: 10px; width:320px; margin: 10px auto; text-align:center;">
                        <?php if ($episode > 0) { ?>
                            <a href="<?php echo CHtml::encode("$requestUrl&episode=" . ($episode - 1)); ?>" class="pager gradient">&laquo; Tập trước</a><?php } ?>
                            <span class="pager active"><?php echo CHtml::encode("Tập ".$episodes[$episode]->episode_order); ?></span>
                        <?php if($episode < $episode_count - 1) { ?>
                        <a href="<?php echo CHtml::encode("$requestUrl&episode=" . ($episode + 1)); ?>" class="pager gradient">Tập sau &raquo;</a><?php } ?>
                    </div>
                <?php } ?>
            </div>
            <?php
            if ($asset['is_series']) {
                ?>
                <div style ="color: #00ADFF; font-weight: bold;" class="box-title" align="center">
                    <h3 class="video_title_header">Chọn tập</h3>
                </div>
                <div class="pagination" style="text-align:center;">
                    <?php
                    for ($i = 1; $i < $episode_count; $i++) {
                        if ($i == $episode) {
                            echo "<span class=\"pager active\">".$episodes[$i]->episode_order."</span>";
                        } else {
                            echo "<a class=\"pager gradient\" href=\"$requestUrl/episode/$i\">".$episodes[$i]->episode_order."</a>";
                        }
                        ?>
                        <?php }
                    ?>
                </div>
                <?php }
            ?>
            <!--<ul class="info-function">
                    <li><a id="fnLike" class="icon-thumbs-up" href="javascript:void(0);"><span>Thích</span></a></li>
                    <li><a id="fnDislike" class="icon-thumbs-down" href="javascript:void(0);"><span>Không Thích</span></a></li>
                    <li><a id="fnGoBack" class="icon-undo" href="<?php echo CHtml::encode($this->createUrl("video/" . $asset['id'])); ?>"><span>Quay về</span></a></li>
            </ul>-->

            <div class="box-film">
                <div class="box-title">
                            <div class="ui-bar ui-bar-d">
                                <img style="width: 20px;height: 18px; float: left; margin-right: 10px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_movie.png" >
                                <h3>PHIM CÙNG THỂ LOẠI</h3>
                                <input type="image" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ic_more.png" style="width: auto; height:20px;float: right; margin-right: 10px;" value="MoreFilm" data-role="none">
                            </div>
                </div>
                 <div class="trailer"></div>
                   <div class="ui-grid-b">
                            <?php
                            if (count($related) > 0) {
                                $j = 0;
                                foreach ($related as $i => $vod) {
                                    $ui_block = "";
                                    switch ($j%3) {
                                        case 0:
                                            $ui_block = 'ui-block-a';
                                            break;
                                        case 1:
                                            $ui_block = 'ui-block-b';
                                            break;
                                        case 2:
                                            $ui_block = 'ui-block-c';
                                            break;
                                        default:
                                            $ui_block = 'ui-block-a';
                                    }
                                    $j++;

                                ?>
                                <div class="<?php echo $ui_block; ?>">
                                    <?php
                                $rPosterUrl = array_key_exists('poster_land', $vod) ? $vod['poster_land'] : null;
                                $isFree = $hasService || $asset['is_free'] == 1;
                                $this->widget("application.widgets.AssetItem", array('asset' => $vod, 'posterUrl' => $rPosterUrl, 'isFree' => $isFree));
                            ?>
                                </div>
                                <?php }
                    } else {
                        ?>
                        <div class="content-items">
                            <p style="color:black;">Không tìm thấy phim liên quan</p>
                        </div>
                        <?php }  ?>

                </div>
                
            </div>
<?php $this->widget("application.widgets.Footer", array('categories' => $this->categories)); ?>
</div>

