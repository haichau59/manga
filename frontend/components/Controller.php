<?php
/**
 * Controller.php
 *
 * @author: antonio ramirez <antonio@clevertech.biz>
 * Date: 7/23/12
 * Time: 12:55 AM
 */
class Controller extends CController {
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	*/
	public static $ACCESS_VIA_WIFI = 1;
	public static $ACCESS_VIA_3G = 2;

	public $breadcrumbs=array();
	public $detect;
	public $array_file = array();
	public $title="";

	protected $page_id = 'home_page';
	protected $pageSize = 12;

	public $categories;
	public $msisdn;
	public $subscriber;
	public $isHome = false;
	public $inAccount = false;
	public $accessType;
	public $user_session;
	protected $crypt;

// 	protected $purchaseResultDescription = array(
// 			CPS_OK                           => "Lệnh thực hiện thành công",
// 			NOK_NO_MORE_CREDIT_AVAILABLE     => "Tài khoản không đủ để thanh toán",
// 	);

	public function __construct($id, $module) {
		parent::__construct($id, $module);
		$ipRanges = Yii::app()->params['ipRanges'];
		$clientIP = Yii::app()->request->getUserHostAddress();
		$found = false;

		//Dectect access via wifi or 3G
		$headers = getallheaders();
		$this->msisdn = '';
		
		//Check wifi
		if($this->msisdn == ''){
			$this->accessType = self::$ACCESS_VIA_WIFI;
			$this->user_session = Yii::app()->user->getState('session');
			$this->msisdn = (Yii::app()->user->getId() != NULL)?Yii::app()->user->getId():'';
			//Verify account with session id
			$subcriber_id = ClientUser::model()->findByAttributes(array('subscriber_number' => $this->msisdn));
			if($subcriber_id != NULL && $this->user_session != NULL){
				$subSession = SubscriberSession::model()->findByAttributes(array('session_id' => $this->user_session, 'subscriber_id' => $subcriber_id->id, 'status' => 1));
				if($subSession != NULL){
					$found = true;
				}else{
					$this->msisdn = '';
				}
			}else{
				$this->msisdn = '';
			}
		}else{
			$this->accessType = self::$ACCESS_VIA_3G;
			foreach ($ipRanges as $range) {
				if (CUtils::cidrMatch($clientIP, $range)) {
					$found = true;
					break;
				}
			}
			if (!$found) {
				$this->msisdn =  '';
			}
		}
		$this->subscriber = $this->msisdn == '' ? null : ClientUser::model()->findByAttributes(array('subscriber_number' => $this->msisdn));
		if ($this->subscriber == null) {
			$this->subscriber = ClientUser::newSubscriber($this->msisdn);
		}

		$this->categories = MangaCategory::getSubCategories();

		$info = CUtils::getDeviceInfo();

		// check saved msisdn - ip address
		$session = new CHttpSession();
		$session->open();
		$saveSession = false;
		if (!isset($session['subscriber']) || $session['subscriber'] != $this->msisdn) {
			$saveSession = true;
			$session['subscriber'] = $this->msisdn;
			$session->close();
		}

		if ($saveSession && $found) {
			$aDate = new DateTime();
			$accessLog = new MsisdnIp();
			$accessLog->request_time = $aDate->format('Y-m-d H:i:s');
			$accessLog->msisdn = $this->msisdn;
			if ($this->subscriber != null)
				$accessLog->subscriber_id = $this->subscriber->id;
			$accessLog->ip = Yii::app()->request->getUserHostAddress();
			$accessLog->save();
		}

		// crypt object
		$cryptOptions = Yii::app()->params['cryptOptions'];
		$cryptOptions['key'] = $this->msisdn . "_" . Yii::app()->request->getUserHostAddress();
		$this->crypt = new Crypt($cryptOptions);
	}

	public function render($view, $data = null, $return = false) {
		if ($data == null) $data = array();
		$data['categories'] = $this->categories;
		return parent::render($view, $data, $return);
	}
}
