<?php

class MangaController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'search', 'browse', 'rateManga'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if (!isset($id))
            return $this->redirect(array("manga/browse"));

        $model = Manga::model();
        /** @var $asset VodAsset */
        $asset = $model->findByAttributes(array('id' => $id, 'status' => 1));
        if (!$asset)
            return $this->redirect(array("manga/browse"));

        $encrypted_id = $this->crypt->encrypt($id);

        $cover = MangaCover::model()->findByAttributes(array("manga_id" => $id));
        $posterUrl = "";
        if($cover != NULL) {
	        $posterUrl = $cover->url;
        }
        
        $related = $asset->getRelatedManga("rand()", 0, 6);
        $relatedManga = $related['data'];
        for ($i = 0; $i < count($relatedManga); $i++) {
        	$cover = MangaCover::model()->findByAttributes(array("manga_id" => $relatedManga[$i]['id']));
        	$relatedPosterUrl = "";
        	if($cover != NULL) {
	            $relatedManga[$i]['cover'] = $cover->url;
        	}
        }

//         $comments = $asset->getComments(0, 1000);
//         $comments = $comments['data'];

        // xem co dang ky dich vu chua? neu co la dich vu gi?

        $cats = $asset->mangaCategoryMappings;
        $nameCats = " ";
        foreach ($cats as $cat) {
            $catModel = MangaCategory::model()->findByPk($cat->manga_category_id);
            $nameCats .= $catModel->display_name . ',';
        }
        $this->page_id = "manga_index_$id";
        $this->render('index', array(
            'asset' => $asset,
            'posterUrl' => $posterUrl,
            'catenames' => $nameCats,
            'related' => $relatedManga,
//             'comments' => $comments,
            'page_id' => 'manga_index'));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Manga;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manga']))
		{
			$model->attributes=$_POST['Manga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manga']))
		{
			$model->attributes=$_POST['Manga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Manga');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Manga('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manga']))
			$model->attributes=$_GET['Manga'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Manga::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manga-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionSearch($q, $page = 1) {
		if (!isset($q) || $q == "") {
			return $this->redirect(array("manga/browse"));
		}
		if (!isset($page) || $page < 1)
			$page = 1;
	
		$result = Manga::findManga('','', $page - 1, $this->pageSize, $q);
		$assets = $result['data'];
		for ($i = 0; $i < count($assets); $i++) {
			$cover = MangaCover::model()->findByAttributes(array("manga_id" => $assets[$i]['id']));
			$posterUrl = "";
			if($cover != NULL) {
				$posterUrl = $cover->url;
			}
			$assets[$i]['posterUrl'] = $posterUrl;
		}
	
		$pager = array();
		foreach (array('total_result', 'page_number', 'page_size', 'total_page') as $e) {
			if (array_key_exists($e, $result))
				$pager[$e] = $result[$e];
		}
		$this->page_id = "manga_search";
		$this->render('browse', array(
				'type' => 'search',
				'keyword' => $q,
				'category' => "Kết quả tìm kiếm",
				'assets' => $assets,
				'category_id' => "",
				'order_by' => null,
				'pager' => $pager,
				'page_id' => 'manga_search'
		));
	}
	
	public function actionBrowse() {
		$category_id = isset($_REQUEST['category']) && preg_match("/^\d+$/", $_REQUEST['category']) ?
		$_REQUEST['category'] : null;
		$order_by = isset($_REQUEST['order']) && preg_match("/(newest|most_viewed)/", $_REQUEST['order']) ?
		$_REQUEST['order'] : "";
		$page = isset($_REQUEST['page']) && preg_match("/^\d+$/", $_REQUEST['page']) ?
		$_REQUEST['page'] : 0;
		$pageSize = isset($_REQUEST['page_size']) && preg_match("/^\d+$/", $_REQUEST['page_size']) ?
		$_REQUEST['page_size'] : $this->pageSize;
	
		$keyword = null;
		$page = $page > 0 ? $page - 1 : 0;
	
		$db_order = "";
		$categoryName = "";
		switch ($order_by) {
			case "newest":
				$db_order = 't.create_date DESC';
				$categoryName = "Truyện mới nhất";
				break;
			case "most_viewed":
				$db_order = 't.view_count DESC';
				$categoryName = "Xem nhiều nhất";
				break;
			case "top_rated":
				$db_order = 't.rating_count DESC';
				$categoryName = "Truyện bình chọn nhiều";
				break;
			case "most_discussed":
				$db_order = 't.comment_count DESC';
				$categoryName = "Truyện bình luận nhiều";
				break;
			case "featured"://not support now
				// $models = Asset::model()->findAll();
				//break;
			default: //case "default":
				$order = 'default';
				$db_order = "t.create_date DESC, t.display_name_ascii";
				$categoryName = "Truyện đang hot";
				break;
		}
	
		if (isset($category_id)) {
			$catModel = new MangaCategory();
			$category = $catModel->findByPk($category_id);
			if (isset($category)) {
				$categoryName = $category->display_name;
			}
		}
	
		$result = Manga::findManga($category_id, $db_order, $page, $pageSize, $keyword);
		$assets = $result['data'];
		for ($i = 0; $i < count($assets); $i++) {
			$cover = MangaCover::model()->findByAttributes(array("manga_id" => $assets[$i]['id']));
			$posterUrl = "";
			if($cover != NULL) {
				$posterUrl = $cover->url;
			}
			$assets[$i]['posterUrl'] = $posterUrl;
		}
	
		$pager = array();
		foreach (array('total_result', 'page_number', 'page_size', 'total_page') as $e) {
			if (array_key_exists($e, $result))
				$pager[$e] = $result[$e];
		}
		$this->page_id = "manga_browse";
		$this->render('browse', array(
				'type' => 'browse',
				'category' => $categoryName,
				'assets' => $assets,
				'category_id' => $category_id,
				'order_by' => $order_by,
				'pager' => $pager,
				'page_id' => 'manga_browse'
		));
	}
	
	public function actionRateManga() {
		header('Content-Type: application/json; charset="UTF-8"');
		$mangaID = isset($_REQUEST['manga']) ? $_REQUEST['manga'] : 0;
		$manga = Manga::model()->findByPk($mangaID);
		if($manga == NULL) {
			echo json_encode(array('message' => 'Lỗi trong quá trình xử lý, xin vui lòng thử lại sau'));
		}
		$mobileNumber = $this->msisdn;
		$stars = isset($_REQUEST['rate']) ? intval($_REQUEST['rate']) : '0';
		$manga->rating = ($manga->rating * $manga->rating_count + $stars)/($manga->rating_count + 1);
		$manga->rating_count++;
				
		if (!$manga->update()) {
			echo json_encode(array('message' => 'Lỗi trong quá trình xử lý, xin vui lòng thử lại sau'));
			Yii::app()->end();
		} else {
			echo json_encode(array('message' => 'Đánh giá của bạn đã được ghi nhận, cám ơn bạn đã đóng góp ý kiến'));
			Yii::app()->end();
		}
	}
}
