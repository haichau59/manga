<?php

class AccountController extends Controller {
	protected $pageSize = 10;
	protected $haveCookie = true;
	
	public function filters(){
		return array(
			'3GOnly + subscribe, gift, cancel',
			'HaveCookie + login'
		);
	}
	
	public function filter3GOnly($filterChain){
		if($this->accessType == Controller::$ACCESS_VIA_3G){
			$filterChain->run();
		}else{
			Yii::app()->user->setFlash('responseToUser', "Quí khách truy cập 3G của Vinaphone mới sử dụng được tính năng này!");
			$this->redirect(Yii::app()->homeUrl);
		}
	}
	
	public function filterHaveCookie($filterChain){
		if(!isset(Yii::app()->request->cookies['vinaphimSESSID'])){
			Yii::app()->user->setFlash('responseToUser', "Bạn phải bật tính năng cookie của trình duyệt mới đăng nhập được!");
			$this->haveCookie = false;
		}
		$filterChain->run();
	}
	
	public function actionIndex() {
        $this->inAccount = true;
        $this->page_id = "account_index";
		$email = '';
		$notification = true;
		if(isset($_POST['submit'])){
			$notification = false;
			$responseToUser = '';
			if($this->msisdn == ''){
				$responseToUser = 'Không nhận diện được thuê bao. Xin vui lòng truy cập dịch vụ bằng 3G/EDGE của Vinaphone, hoặc đăng nhập bằng  <a style="color: white;" href="'.Yii::app()->request->baseUrl.'/account/login">wifi tại đây.</a>';
				
			}else{
				$email = $_POST['email'];
				$apiService = new APIService();
				$response = $apiService->registerEmail($this->msisdn, $email);
				$responseToUser = $response->error_message;
				if($response->error_no == 0){
					$this->subscriber->email = $email;
				}
			}
			if(trim($responseToUser) != ''){
				Yii::app()->user->setFlash('responseToUser', $responseToUser);
			}
		}
		
		/*if ($this->subscriber != null && $this->msisdn != '') {
			if($this->subscriber->email == '' || $this->subscriber->email == $this->subscriber->subscriber_number.'@mobiphim.vn'){
				$responseToUser = 'Xin vui lòng cập nhật thông tin email của bạn <a href="#" onclick="return updateEmail(true);" style="color:#FFF";>tại đây</a>, để có thể truy cập wifi tiện lợi hơn!';
				if($notification){
					Yii::app()->user->setFlash('responseToUser', $responseToUser);
				}
			}else{
				$email = $this->subscriber->email;
			}
		}*/
		$this->render('account', array(
			'email' => $email,
            'page_id' => $this->page_id,
			'usingServices'=> NULL,
			'services' => array(),
		));
	}
	
	public function actionSubscribe() {
        $this->inAccount = true;
		$service_id = isset($_REQUEST['service']) ? $_REQUEST['service'] : 0;
		#TODO: validate $service_id
		$response = null;
		$responseToUser = '';
		
		//Get ref_id cuar partner
		$ref_id = isset($_REQUEST['ref_id'])?$_REQUEST['ref_id']: 0;	
		$partner = Partner::model()->findByPk($ref_id);
                if($partner == NULL) {
                    $ref_id = NULL;
                }
		if ($service_id == 0) {
			try {
				$id = isset($_REQUEST['id']) ? $this->crypt->decrypt($_REQUEST['id']) : 0;
				if (preg_match('/^(\d+)/', $id, $matches)) {
					$service_id = $matches[1];
				}
			} catch (Exception $e) {
				// do nothing
			}
		}
		
		$subscribeOK = false;
		
		//Thuc hien khi cancel
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'cancel'){
                        return $this->redirect(array('account/subscribe'));
		}
		
		//Thuc hien khi thanh toan thanh cong, vinaphone redirect ve day
		if(isset($_REQUEST['msisdn']) && isset($_REQUEST['requestid'])){
			$msisdn_rediect = $_REQUEST['msisdn'];
			$request_id = $_REQUEST['requestid'];
			$subscription_datetime = $_REQUEST['subscriptiondatetime'];
			$result = $_REQUEST['result'];
			$return_secure_code = $_REQUEST['securecode'];
		
			//Verify return checkout
			if(ChargingController::verifySubscribeReturn($request_id, $subscription_datetime, $result, $return_secure_code)){
				$subscriberRequest = SubscriberRequest::model()->findByPk($request_id);
				if($subscriberRequest == NULL) {
					//TODO: hien thi verify fail
					Yii::app()->user->setFlash('responseToUser', "Request Error!");
                                        return $this->redirect(array('account/subscribe'));
				}
// 				$subscriberRequest->transaction_id = $transaction_id;
				$timestamp = DateTime::createFromFormat('YmdHis', $subscription_datetime)->getTimestamp();
				$subscriberRequest->transaction_date = $timestamp;
				$subscriberRequest->vnp_status = $result; 
				switch($result) {
					case 0: // dang ky thanh cong
                                        case 1: // huy thanh cong
						$subscriberRequest->status = 1;
						break;
                                        case 2: // da ton tai goi
                                        case 3: // khong ton tai goi
                                        case 4: // exception
                                                $subscriberRequest->status = 0;
						break;
				}
				$subscriberRequest->update();
				//TODO: thuc hien tiep khi thanh toan thanh cong
//				header("Content-type: text/plain");
//				var_dump($_REQUEST);
//				echo 'Thanh toan thanh cong. Continue xem phim...';
                                Yii::app()->user->setFlash('responseToUser', "Thông báo: Quý khách đã đăng ký thành công dịch vụ Xem phim trực tuyến của Vinaphone !");
                                return $this->redirect(Yii::app()->homeUrl);
			}else{
				//TODO: hien thi verify fail
                                Yii::app()->user->setFlash('responseToUser', "Request Error!");
                                return $this->redirect(array('account/subscribe'));
			}
			return;
		}
		
		//Block account charging;
		//$accountState = Yii::app()->cache->get($this->msisdn);
// 		if ($this->msisdn != '' && $service_id > 0 && $accountState === false) {
		if ($this->msisdn != '' && $service_id > 0) {
			//Yii::app()->cache->set($this->msisdn, 'block', 3600);
			if (count($this->usingServices) == 0) { // chi cho dang ky toi da 1 dich vu
				if ($this->subscriber == null) {
					$this->subscriber = Subscriber::newSubscriber($this->msisdn);
				}
				
				$service = Service::model()->findByPk($service_id);
				if (isset($service) && $this->subscriber != NULL ) {
					
// 					$subscriberTransaction = $this->subscriber->newTransaction(CHANNEL_TYPE_WAP, USING_TYPE_REGISTER, PURCHASE_TYPE_NEW, $service);
// 					$charging = new ChargingProxy();
// 					$transaction_id = $subscriberTransaction->id;
// 					$response = $charging->debitAccount($transaction_id, $this->msisdn, $this->msisdn, $service->price, $service->category_id, $service->content_id);
					$subscriberRequest = $this->subscriber->newRequest(CHANNEL_TYPE_WAP, USING_TYPE_REGISTER, PURCHASE_TYPE_NEW, $service);
					$order_id = $subscriberRequest->id;
					
					$returnUrl = str_replace('dev.vinaphim', 'mobiphim',Yii::app()->createAbsoluteUrl(Yii::app()->request->url));
					$backUrl = str_replace('dev.vinaphim', 'mobiphim',Yii::app()->createAbsoluteUrl(Yii::app()->request->url)).'&action=cancel';
					$this->redirect(ChargingController::makeSubscriptionUrl($order_id,$service->display_name , date('YmdHis'), $service->code_name, ChargingController::VNP_SUBSCRIPTION_TYPE_REGISTER, $returnUrl, $backUrl),true);
				} else { // khong tim thay service
                                        Yii::app()->user->setFlash('responseToUser', "Dịch vụ không tồn tại!");
					return $this->redirect(array('account/subscribe'));
				}
			} else {
				$responseToUser .= 'Quý khách đã đăng ký gói cước. Để đăng ký gói cước mới, quý khách phải huỷ gói cước cũ.';
			}
// 			Yii::app()->cache->delete($this->msisdn);
		}
                $this->page_id = "account_subscribe";
		$this->render('subscribe', array(
			'subscribeOK' => $subscribeOK,
			'responseToUser' => $responseToUser,
			'services' => $this->services,
			'usingServices' => $this->usingServices,
                        'page_id' => $this->page_id
		));
	}
	
	public function actionCancel() {
            $this->inAccount = true;
            $service_id = isset($_REQUEST['service'])?$_REQUEST['service']:-1;
            //Thuc hien khi cancel
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'cancel'){
			return $this->redirect(array('account/'));
		}
		
		//Thuc hien khi thanh toan thanh cong, vinaphone redirect ve day
		if(isset($_REQUEST['msisdn']) && isset($_REQUEST['requestid'])){
			$msisdn_rediect = $_REQUEST['msisdn'];
			$order_id = $_REQUEST['requestid'];
			$subscription_datetime = $_REQUEST['subscriptiondatetime'];
			$result = $_REQUEST['result'];
			$return_secure_code = $_REQUEST['securecode'];
		
			//Verify return checkout
			if(ChargingController::verifySubscribeReturn($order_id, $subscription_datetime, $result, $return_secure_code)){
				$subscriberRequest = SubscriberRequest::model()->findByPk($order_id);
                                if($subscriberRequest->subscriber_id != $this->subscriber->id) {
                                    Yii::app()->user->setFlash('responseToUser', "subsriber_order nay khong phai cua MSISDN $msisdn_rediect");
                                    return $this->redirect(array('account/'));
                                }
				if($subscriberRequest == NULL) {
					//TODO: hien thi verify fail
                                    Yii::app()->user->setFlash('responseToUser', "Verify fail!");
                                    return $this->redirect(array('account/'));
				}
// 				$subscriberRequest->transaction_id = $transaction_id;
				$timestamp = DateTime::createFromFormat('YmdHis', $subscription_datetime)->getTimestamp();
				$subscriberRequest->transaction_date = $timestamp;
				$subscriberRequest->vnp_status = $result; 
				switch($result) {
					case 0: // dang ky thanh cong
                                        case 1: // huy thanh cong
						$subscriberRequest->status = 1;
						break;
                                        case 2: // da ton tai goi
                                        case 3: // khong ton tai goi
                                        case 4: // exception
                                                $subscriberRequest->status = 0;
						break;
				}
				$subscriberRequest->update();
                                Yii::app()->user->setFlash('responseToUser', 'Quý khách đã hủy thành công gói dịch vụ xem Phim trực tuyến. Cảm ơn bạn đã sử dụng dịch vụ của Vinaphone.');
                                return $this->redirect(Yii::app()->homeUrl);
                              
				//TODO: thuc hien tiep khi thanh toan thanh cong
//				header("Content-type: text/plain");
//				var_dump($_REQUEST);
//				echo 'Thanh toan thanh cong. Continue xem phim...';
			}else{
				//TODO: hien thi verify fail
				Yii::app()->user->setFlash('responseToUser', "Verify fail!");
                                return $this->redirect(array('account/'));
			}
			return;
		}
            
            $service = Service::model()->findByPk($service_id);
            if($service == NULL) {
//                echo "Service $service_id is not existed!"; 
                $ssmap = ServiceSubscriberMapping::model()->findByAttributes(array("subscriber_id"=>$this->subscriber->id, "is_active" => 1));
                if($ssmap == NULL) {
                    Yii::app()->user->setFlash('responseToUser', "Verify fail!");
                    return $this->redirect(array('account/'));
                }
                $service = Service::model()->findByPk($ssmap->service_id);
            }
            $subscriberRequest = $this->subscriber->newRequest(CHANNEL_TYPE_WAP, 0, PURCHASE_TYPE_CANCEL, $service);
            $request_id = $subscriberRequest->id;
            $returnUrl = str_replace('dev.vinaphim', 'mobiphim',Yii::app()->createAbsoluteUrl(Yii::app()->request->url));
            $backUrl = str_replace('dev.vinaphim', 'mobiphim',Yii::app()->createAbsoluteUrl(Yii::app()->request->url)).'&action=cancel';
            $this->redirect(ChargingController::makeSubscriptionUrl($request_id,$service->display_name , date('YmdHis'), $service->code_name, ChargingController::VNP_SUBSCRIPTION_TYPE_CANCEL, $returnUrl, $backUrl),true);

	}
	
	public function actionHistory() {
            $this->inAccount = true;
		$fDate = isset($_REQUEST['from_date']) ? DateTime::createFromFormat('d/m/Y H:i:s', $_REQUEST['from_date'] . " 00:00:00") : null;
		if ($fDate == null || $fDate == FALSE)
			$fDate = DateTime::createFromFormat('d/m/Y H:i:s', date('01/m/Y 00:00:00'));
		
		$tDate = isset($_REQUEST['to_date']) ? DateTime::createFromFormat('d/m/Y H:i:s', $_REQUEST['to_date'] . " 23:59:59") : null;
		if ($tDate == null || $tDate == FALSE)
			$tDate = DateTime::createFromFormat('d/m/Y H:i:s', date('d/m/Y 23:59:59'));
		
		$page = isset($_REQUEST['page']) && preg_match('/^\d+$/', $_REQUEST['page']) ? $_REQUEST['page'] : 1;
		if ($page == 0) $page = 1;
		
		$transactions = array();
		$pager = array();
		if ($this->subscriber != null) {
			$result = $this->subscriber->getSubscriberTransactions(
					$fDate->format('Y-m-d H:i:s'),
					$tDate->format('Y-m-d H:i:s'),
					$page-1, $this->pageSize);
			if (isset($result['data']))
				$transactions = $result['data'];
				
			foreach (array('total_result', 'page_number', 'page_size', 'total_page') as $e) {
				if (array_key_exists($e, $result))
					$pager[$e] = $result[$e];
			}
		}
		$this->page_id = "account_history";
		$this->render('history', array(
				'transaction' => $transactions,
				'pager' => $pager,
				'from_date' => $fDate->format('d/m/Y'),
				'to_date' => $tDate->format('d/m/Y'),
				'usingServices' => $this->usingServices,
                                'page_id' => $this->page_id
		));
	}
	
	public function actionGift() {
            $this->inAccount = true;
		$service_id  = isset($_POST['service_id'])  ? $_POST['service_id']  : "";
		$gift_number = isset($_POST['gift_number']) ? $_POST['gift_number'] : "";
		$responseToUser = '';
		if (preg_match('/^(84|0)(90|93|120|121|122|126|128)\d{7}$/', $gift_number, $matches)) {
			if ($matches[1] == 0) // change 09xxxxxxxx --> 849xxxxxxxx
				$gift_number = preg_replace('/^0/', '84', $gift_number);
			$service = $service_id == "" ? null : Service::model()->findByAttributes(array('id' => $service_id, 'is_deleted' => 0));
			if (isset($service)) {
				// find gift receiver
				$receiver = Subscriber::model()->findByAttributes(array('subscriber_number' => $gift_number));
				$receiverServices = false;
				if ($receiver != null) { // reveiver has a service?
					$receiverServices = ServiceSubscriberMapping::model()->findAllByAttributes(array('subscriber_id' => $receiver->id, 'is_active' => 1));
				} else {
					$receiver = Subscriber::newSubscriber($gift_number);
				}
					
				if ($receiverServices) {
					$responseToUser = "Xin lỗi, yêu cầu tặng gói ".$service->display_name." của quý khách không thành công do thuê bao $gift_number đang sử dụng dịch vu xem Phim trực tuyến của Vinaphone. Xin cảm ơn!";
				} else {
					$confirmExisted = true;
					$confirm_code = '';
					// check cofirmation code
					while ($confirmExisted) {
						$confirm_code = rand(1, 9999);
						$confirmExisted = SubscriberGiftConfirmation::model()->findByAttributes(array('confirmation_code' => $confirm_code));
					}
				
					$giftConfirm = new SubscriberGiftConfirmation();
					$giftConfirm->subscriber_id = $this->subscriber->id;
					$giftConfirm->receiver_id = $receiver->id;
					$giftConfirm->service_id = $service->id;
					$giftConfirm->confirmation_code = $confirm_code;
					$giftConfirm->create_date = new CDbExpression('NOW()');
					$giftConfirm->expiration_date = new CDbExpression('DATE_ADD(NOW(), INTERVAL 1 DAY)');
					$giftConfirm->save();
				
					$mtMessage = "Xin chao quy khach, thue bao ".$this->msisdn." muon tang quy khach goi ".$service->display_name." dich vu xem Phim truc tuyen cua Vinaphone, gia ".intval($service->price)."d. De xac nhan, quy khach soan CO $confirm_code gui 9033 (mien phi) de dong y nhan va xem phim mien phi.";
					SmsController::sendSms($receiver, $mtMessage);
					$responseToUser = "Yêu cầu tặng gói cước của quý khách đã được chuyển đến thuê bao $gift_number. mobiphim sẽ thông báo cho quý khách khi thuê bao $gift_number đồng ý nhận quà tặng.";
				}
			}
		} else {
			$responseToUser = 'Số điện thoại người nhận phải là thuê bao của Vinaphone.';
		}
		
		Yii::app()->user->setFlash('responseToUser', $responseToUser);
		$this->render('gift', array(
			'services' => $this->services,
			'usingServices' => $this->usingServices,
                        'page_id' => $this->page_id
		));
	}
	
	public function actionService() {
            $this->inAccount = true;
            $this->page_id = "account_service";
		$this->render('service', array());
	}
	
	public function actionExtend() {
		$cfg = Yii::app()->params['autoRecur'];
		$protectDuration = 12*60*60; //12h	
		$tmp = Service::model()->findAllByAttributes(array('is_deleted' => 0));
		$services = array();
		foreach ($tmp as $s) {
			$services[$s->code_name] = $s;
		}
		$subcribers = Subscriber::getAutoRecurringSubscribers();
		
		$period_for_recur = $cfg['cronJobPeriod'] * 60; // 2h45p hours
		$tz = new DateTimeZone("Asia/Ho_Chi_Minh");

		$this->layout=false;
		header('Content-type: text/plain');
		echo "Got " . count($subcribers) . " subs\n";

		foreach ($subcribers as $sub) {
			$subscriber = Subscriber::model()->findByPk($sub['subscriber_id']);
			$ssm = ServiceSubscriberMapping::model()->findByPk($sub['id']);

			//Check trang thai active cua service
			if($ssm->is_active == 0) continue;

			$current = time();
			$diff = strtotime($ssm->expiry_date) - $current;
			if($diff > $sub['using_days'] * 24 * 60 * 60) continue;

			error_log("subscriber=" . $subscriber->subscriber_number);
			
			$ext_dt = new DateTime("@" . ($sub['expiry_ts'] + $sub['using_days'] * 24 * 60 * 60));
			$ext_dt->setTimezone($tz);
// 			if (strtoupper($sub['code_name']) != 'PHIM' && $sub['sent_notification'] == 0) {
					
// 				// send SMS notification
// 				$sms_message = "Quy khach con 02 ngay su dung goi cuoc ".$sub['display_name']." cua dich vu xem Phim truc tuyen cua Vinaphone. Goi cuoc se duoc tu dong gia han den ".$ext_dt->format("H:i")." ngay ".$ext_dt->format("d/m/Y")." neu khong yeu cau huy. De huy dv, truy cap http://mobiphim.vn de biet them chi tiet.";
// 				$mt = SmsController::sendSms($subscriber, $sms_message);
				
// 				// notification has been sent
//  				if (preg_match("/^0:/", $mt->mt_status)) {
// 					$ssm->sent_notification = 1;
// 					$ssm->save();
//  				}
// 			}
			
			//Kiem tra transaction gan nhat cua user la bao lau
			/*$sqlTransaction = 'SELECT * FROM `subscriber_transaction` 
							   WHERE `status` = 1 AND subscriber_id=:subs_id AND (purchase_type = 1 OR purchase_type = 2)
							   ORDER BY id DESC LIMIT 1';
			$recentTransaction = SubscriberTransaction::model()->findBySql($sqlTransaction, array(':subs_id' => $subscriber->id));
			
			$duration = $current - strtotime($recentTransaction->create_date);
			
			if($duration < $protectDuration){
				echo "$subscriber->subscriber_number |".$recentTransaction->create_date."| $recentTransaction->service_id | $recentTransaction->purchase_type\n";
				continue;
			}*/

			$diff = $sub['expiry_ts'] - $current;
			error_log("expiry_ts=".$sub['expiry_ts'].", current=$current, diff=$diff, period=$period_for_recur");
			if ($diff < $period_for_recur || $sub['recur_retry_times'] > 0) {
				$service = $services[$sub['code_name']];
				error_log("current service: " . $service->display_name);
				if (isset($service)) {
					// TODO: co can sua channel type?
					$recurTransaction = $subscriber->newTransaction(CHANNEL_TYPE_WAP, USING_TYPE_REGISTER, PURCHASE_TYPE_RECUR, $service);
					
 					$charging = new ChargingProxy();
 					$transaction_id = $recurTransaction->id;
 					error_log("transaction id=$transaction_id");
 					
 					$response = $charging->debitAccount($transaction_id, $sub['subscriber_number'], $sub['subscriber_number'], $service->price, $service->category_id, $service->content_id);
					$smsToUser = "";
					if (isset($response) && isset($response->result) && $response->result == CPS_OK) {
						$exp_ts = $sub['expiry_ts'] > time() ? $sub['expiry_ts'] : time();
						$new_ts = ($exp_ts + $sub['using_days'] * 24 * 60 * 60);
						$newexp = new DateTime("@$new_ts");
						$newexp->setTimezone($tz);
						error_log('new exp date=' . $newexp->format('Y-m-d H:i:s'). "ts=$new_ts, using_days=".$sub['using_days']);
						
						$ssm->expiry_date = $newexp->format('Y-m-d H:i:s');
						$ssm->sent_notification = 0;
						$ssm->recur_retry_times = 0;
						$ssm->save(false);
						foreach ($ssm->getErrors() as $k => $v) {
							error_log("ssm error $k=$v");
						}
						
						$recurTransaction->error_code = CPS_OK;
						$recurTransaction->save();
						
						//THuc hien Promotion extra point
						if(CPromotions::havePromotion(CPromotions::$PROMOTION_TYPE_EXTRA_POINT)){
							$promotion = new CPromotions();
							if($subscriber != NULL){
								if(strtoupper($service->code_name) == 'PHIM'){
									$promotion->PExtraPoint(CPromotions::$PURCHASE_SERVICE, $subscriber,$service->code_name, false);
								}else{
									$promotion->PExtraPoint(CPromotions::$PURCHASE_SERVICE, $subscriber,$service->code_name);
								}
							}
						}
						
						$smsToUser = "Quy khach vua gia han thanh cong goi cuoc ".$service->display_name." dich vu xem Phim truc tuyen cua Vinaphone. Thoi gian su dung den ".$newexp->format("H:i")." ngay ".$newexp->format("d/m/Y").". Truy cap http://mobiphim.vn de xem phim mien phi cac bo Phim HOT nhat hien nay";
					} else {
						
						$recurTransaction->error_code = isset($response->result) ? $response->result : "ERROR";
						$recurTransaction->status = 2;
						$recurTransaction->save();
						
						$ssm->recur_retry_times++;
						$ssm->save();
						
						if ($ssm->recur_retry_times == $cfg['maxRecurRetries']) { // da retry 30 lan, cancel goi cuoc
							$subscriber->cancelService($ssm, CHANNEL_TYPE_WAP);
							// TODO: phan biet tra truoc, tra sau de gui SMS
							$smsToUser = "Goi cuoc ".$service->display_name." da bi huy do tai khoan cua quy khach dang bi khoa nen khong the tiep tuc su dung goi ".$sub['display_name']." dich vu xem Phim truc tuyen cua Vinaphone. Vui long thanh toan cuoc de tiep tuc su dung hoac truy cap http://mobiphim.vn de biet them chi tiet.";
						}
					}
					
					if ($smsToUser != "" && strtoupper($sub['code_name']) != "PHIM") {
						//SmsController::sendSms($subscriber, $smsToUser);
					}
				}
			}
			echo CJavaScript::jsonEncode($sub) . "\n";
		}
		
		//$this->layout=false;
		//header('Content-type: application/json');
		//echo CJavaScript::jsonEncode($subcribers);
		Yii::app()->end();
	}
        
        /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
			$typeGetpass = 1;
                        $this->page_id = "account_login";
            $this->inAccount = true;
                if($this->msisdn === ''){
                	$timeStamp = time();
                	$urlGetOTA = '/account/login?action=getOTA&timestamp='.$timeStamp.'&';
                    $model=new LoginForm;
                    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'getOTA'){
                        $mobiNum = isset($_REQUEST['mobile_number'])?$_REQUEST['mobile_number']:'';
                        $email = isset($_REQUEST['email'])?$_REQUEST['email']:'';
                        $timeStampRequest = isset($_REQUEST['timestamp'])?$_REQUEST['timestamp']:time();
//                         $timeStampCookie = isset(Yii::app()->request->cookies['timestamp']) ? Yii::app()->request->cookies['timestamp']->value : '1';
//                         $timeStampPrev = $this->crypt->decrypt($timeStampCookie);
                        $typeGetpass = isset($_REQUEST['typeGet'])?$_REQUEST['typeGet']:1;
//                         if(intval($timeStampRequest) != intval($timeStampPrev)){
//                         	$this->redirect('login');
//                         }
                        $validMobi = '';
                        if($typeGetpass == 1){
	                        $validMobi = CUtils::validatorMobile($mobiNum);
	                        if($validMobi == ''){
	                            Yii::app()->user->setFlash('responseToUser', 'Số điện thoại không hợp lệ, vui lòng chọn thuê bao của Vinaphone và thử lại');
// 	                            Yii::app()->request->cookies['timestamp'] = new CHttpCookie('timestamp', $this->crypt->encrypt($timeStamp));
	                            $this->render('login',array('model'=>$model, 'typeGet' => $typeGetpass, 'urlGetOTA' => $this->createUrl($urlGetOTA)));
	                            Yii::app()->end();
	                        }
                        }else{
                        	//Validate Email Uer;
                        	$model->email = $email;
                        	$emailValidator = CValidator::createValidator('email', $model, array('email'));
                        	if(!$emailValidator->validateValue($email)){
                        		Yii::app()->user->setFlash('responseToUser', 'Email không hợp lệ, xin vui lòng nhập lại email');
//                         		Yii::app()->request->cookies['timestamp'] = new CHttpCookie('timestamp', $this->crypt->encrypt($timeStamp));
                        		$this->render('login',array('model'=>$model, 'typeGet' => $typeGetpass, 'urlGetOTA' => $this->createUrl($urlGetOTA)));
                        		Yii::app()->end();
                        	}
                        	
                        	//Check exist user with email
                        	$subscriber = Subscriber::model()->findByAttributes(array('email' => $email));
                        	if($subscriber != NULL){
                        		$validMobi = $subscriber->subscriber_number;
                        	}else{
                        		Yii::app()->user->setFlash('responseToUser', 'Email chưa được đăng ký với số điện thoại Mobiphone nào trên hệ thống');
//                         		Yii::app()->request->cookies['timestamp'] = new CHttpCookie('timestamp', $this->crypt->encrypt($timeStamp));
                        		$this->render('login',array('model'=>$model, 'typeGet' => $typeGetpass, 'urlGetOTA' => $this->createUrl($urlGetOTA)));
                        		Yii::app()->end();
                        	}
                        }
                        $model->username = $validMobi;
                        $apiRequest = new APIService();
                        $response = $apiRequest->getWifiPass($model, $typeGetpass);
                        if($response == NULL){
                            Yii::app()->user->setFlash('responseToUser', 'Có lỗi xảy ra trong quá trình gửi mật khẩu, xin quý khách vui lòng thử lại sau');
                        }else{
                            Yii::app()->user->setFlash('responseToUser', $response->error_message);
                        }
                    }
                    // collect user input data

                    if(isset($_POST['submit']) && $this->haveCookie)
                    {
                            $model->username= isset($_POST['mobile_number'])?$_POST['mobile_number']:'';
                            $model->password=isset($_POST['password_ota'])?$_POST['password_ota']:'';
                            $model->email = isset($_POST['email'])?$_POST['email']:'';
                            $typeGetpass = isset($_POST['type_getpass'])?$_POST['type_getpass']:1;
                            if($typeGetpass == 1){
                            	$model->setScenario('loginByMobile');
                            }else{
                            	$model->setScenario('loginByEmail');
                            }
                            // validate user input and redirect to the previous page if valid
                            if(!$model->validate()){
                            	if($typeGetpass == 1){
	                                Yii::app()->user->setFlash('responseToUser', $model->getError('username'));                                
                            	}else{
                            		Yii::app()->user->setFlash('responseToUser', $model->getError('email'));
                            	}
                            }else{
                            	if($typeGetpass == 1){
                            		if($model->loginByMobile()){
                            			$this->redirect(Yii::app()->homeurl);
                            		}
                            	}else{
                            		if($model->loginByEmail()){
                            			$this->redirect(Yii::app()->homeurl);
                            		}
                            	}
                            }
                                    
                    }
                    //Save ma hoa timestamp vao cookie
//                     Yii::app()->request->cookies['timestamp'] = new CHttpCookie('timestamp', $this->crypt->encrypt($timeStamp));
                    // display the login form
                    $this->render('login',array('model'=>$model, 'typeGet' => $typeGetpass, 'urlGetOTA' => $this->createUrl($urlGetOTA)));
                }else{
                    if($this->accessType == Controller::$ACCESS_VIA_WIFI){
                        $this->render('login');
                    }else{
                        $this->redirect(Yii::app()->homeurl);
                    }
                }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
            if($this->accessType == Controller::$ACCESS_VIA_WIFI && $this->msisdn != ''){
                $session = Yii::app()->user->getState('session');
                if($session != NULL){
                    $apiRequest = new APIService();
                    $response = $apiRequest->logout($this->msisdn, $session);
                }
                Yii::app()->user->logout();
            }
            $this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionConfirmEmail($key=''){
		$message = '';
		$status_img = Yii::app()->baseUrl.'/images/';
		if(trim($key) == '' ){
			$message = 'Xác nhận mail bị lỗi';
			$status_img .= 'warning_icon.png';
		}else{
			$encryptInstance = new MCrypt();
			$email = $encryptInstance->decrypt($key);
			//Verify email
			$subscriber = Subscriber::model()->findByAttributes(array ('email' => $email));
			if($subscriber != NULL){
				if(isset($subscriber->verification_code) && $subscriber->verification_code == 0){
					$message = 'Email <span style="color: #8D8D8D;">'.$email.'</span> đã được xác nhận rồi';
					$status_img .= 'verified.png';
				}else{
					$subscriber->verification_code = 0;
					$subscriber->update();
					$message = 'Xác nhận email <span style="color: #8D8D8D;">'. $email.'</span> thành công';
					$status_img .= 'verified.png';
				}
			}else{
				$message = 'Mail <span style="color: #8D8D8D;">'.$email.'</span> không tồn tại';
				$status_img .= 'warning_icon.png';
			}
		}
		$this->render('confirm_email', array('message' => $message, 'status_img' => $status_img));
	}
	
	public function actionGetCache($subscriber_number){
// 		echo Yii::app()->cache->get($subscriber_number);
	}
}
