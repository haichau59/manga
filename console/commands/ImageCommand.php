<?php
define ('USER_ID', '97545205@N03'); //email: letsharemanga1@gmail.com, password: cr1cketland2012
define ('FLICKR_API_KEY', '0faf5ead62528ce3b51ea47e1798f032');
define ('FLICKR_API_SECRET', 'c8daf3cc7363e2d0');
define ('FLICKR_API_TOKEN', '72157634161872972-fdb209653014a6cd');

/**
 * backup_status = 1 : da download ve server
 * backup_status = 2 : da download ve server & upload len flickr (chua co link)
 * backup_status = 4 : da download ve server & upload len flickr & da lay link flickr
 */

class ImageCommand extends CConsoleCommand{
	public function actionDownload($order = 'asc'){
		echo "Start time: ".date('Y-m-d H:i:s', time());
		$startTime = time();
		$allPageId = MangaPage::model()->findAllBySql("select id from manga_page where backup_status = 0 order by id $order limit 50000"); // 1 la da luu tren server cua minh
		$i = 0;
		foreach($allPageId as $objId) {
			$page = MangaPage::model()->findByPk($objId['id']);
			if($page->backup_status > 0) continue;//co 1 tien trinh khac dang chay & download image nay roi
// 			if($i > 3) break; //FIXME xoa dong nay khi chay that
try {
			$downloadStatus = $page->downloadToServer();
}
catch(Exception $e) {
			continue;	
}
			if($downloadStatus == 1) {
				$i++;
			}
		}
		$endTime = time();
		$duration  = $endTime - $startTime;
		echo "Downloaded $i images. End time: ".date('Y-m-d H:i:s', time()).". Duration = $duration";
	}
	
	public function actionUpload($order = 'asc', $id=-1){
		require_once('Phlickr/Uploader.php');
		 
		echo "Start time: ".date('Y-m-d H:i:s', time());
		$startTime = time();
		
		$api = new Phlickr_Api(FLICKR_API_KEY, FLICKR_API_SECRET, FLICKR_API_TOKEN);
		// create an uploader
		$uploader = new Phlickr_Uploader($api);
		$i = 0;
		if($id == -1) {
			$allPageId = MangaPage::model()->findAllBySql("select id from manga_page where backup_status = 1 order by id $order"); // 1 la da luu tren server cua minh
		}
		else {
			$allPageId = MangaPage::model()->findAllBySql("select id from manga_page where id = $id");
		}
		$i = 0;
		foreach($allPageId as $objId) {
			$page = MangaPage::model()->findByPk($objId['id']);
			$page->fix();
			if($page->backup_status != 1) continue;//co 1 tien trinh khac dang chay & upload image nay roi hoac image co size =0 va vua dc fix (set backup_status = 0 again)
// 			if($i > 10) break; //FIXME xoa dong nay khi chay that
try {
			$uploadStatus = $page->uploadToFlickr($uploader);
}
catch(Exception $e) {
	continue;
}
			if($uploadStatus == 0) {
// 				break;
			}
			$i++;
		}
		
		$endTime = time();
		$duration  = $endTime - $startTime;
		echo "Upload to Flickr $i image! End time: ".date('Y-m-d H:i:s', time()).". Duration = $duration";
	}
	
	//update cac thong tin farm_id, server_id, secret cho manga_page
	public function actionUpdateFlickrInfo($id = -1) {
		require_once('Phlickr/Request.php');
		require_once('Phlickr/PhotoList.php');
		require_once('Phlickr/PhotoListIterator.php');
		
		echo "Start time: ".date('Y-m-d H:i:s', time());
		$startTime = time();
		
		$api = new Phlickr_Api(FLICKR_API_KEY, FLICKR_API_SECRET, FLICKR_API_TOKEN);
		//dung flickr.photos.search de browse lan luot cac flickr image. Lay thong tin farm, server, secret de update cho mang_page
		//vong lap browse flickr.photos.search chay ltuc den khi nao gap manga_page da co thong tin farm-server-secret nay thi stop
		$countPageId = MangaPage::model()->countByAttributes(array("backup_status"=>2));// 1 la da luu tren server cua minh
		$i = 0;
		$page = 1;
		$page_size = 500;
		$running = true;
		$iterator = 0;
		$i = 0; //dem tu dau den cuoi
		while($running) {
// 			echo $page."12345";
// 			if($page > 0) return;
			if($i >= $countPageId) {
				break;
			}
			$request = $api->createRequest(
					    'flickr.photos.search',
					    array(
					    		'page' => $page,
					    		'per_page' => $page_size,
	// 						        'tags' => 'person,happy',
	// 						        'tag_mode' => 'all',
						        'user_id' => USER_ID,
						    )
					);
			$page++; 
// 			$buildUrl1 = substr($request->buildUrl(),0, 160);
// 			echo "\n".$request->buildUrl()."\n";
// // 			echo "buildUrl1 = $buildUrl1";
			$photolist = new Phlickr_PhotoList($request);
// 			$buildUrl2 = substr($request->buildUrl(),0, 160);
// 			echo "\n".$request->buildUrl()."\n";
// // 			echo " *** buildUrl2 = $buildUrl2";
// 			if($buildUrl1 != $buildUrl2) {
// 				$running = false;
// 				break;
// 			}
			if($photolist == NULL) {
				$running = false;
				break;
			}
			$iterator = new Phlickr_PhotoListIterator($photolist);
// 			print_r($iterator);
			$j = 0; //dem tung lan gui request. Neu request nao tra lai 0 images thi stop vong while
// 			foreach ($iterator as $photos) {
			$photos = $iterator->getPhotos();
// 			print_r($photos); return;
// 			$photos = $iterator->_pl->_cachedXml;
		    foreach ($photos as $photo) {
		    	$j++;
// 		    	echo $photo->getId()." - "; 
		    	$mangaPage = MangaPage::model()->findByAttributes(array('f_id'=>$photo->getId()));
		    	if($mangaPage != NULL) {
// 			    	echo $mangaPage->f_id." - "; 
			    	if(($mangaPage->f_farm_id != -1) && ($mangaPage->f_farm_id != NULL)) { //da update roi
			    		continue;
// 			    		$running = false;
// 			    		break;
			    	}
			    	$mangaPage->f_farm_id = $photo->getFarm();
			    	$mangaPage->f_server_id = $photo->getServer();
			    	$mangaPage->f_secret = $photo->getSecret();
			    	$mangaPage->backup_status = 4;
			    	$mangaPage->update();
			    	$i++;
		    	}
		    }
// 			}
			if($j == 0) {
				echo "\n Da browse het image tren flickr \n";
				$running = false;
				break;
			}
		}
		
		$endTime = time();
		$duration  = $endTime - $startTime;
		echo "Update flickr meta data for $i manga page! End time: ".date('Y-m-d H:i:s', time()).". Duration = $duration";
	}
	
	//for test only: hien thi link flickr
	public function actionPrintFlickrLink($id) {
		$page = MangaPage::model()->findByPk($id);
		if($page != NULL) {
			echo $page->getFlickrLink();
		}
		else {
			echo '';
		}
	}
	
	/**
	 * quet tat ca page backup_status = 1, kiem tra xem tren server co download dc ko. Neu ko (download bi loi) thi set lai backup_status = 0
	 */
	public function actionFix() {
		$allPageId = MangaPage::model()->findAllBySql("select id from manga_page where backup_status = 1"); // 1 la da luu tren server cua minh
		$count = 0;
		foreach($allPageId as $objId) {
			$page = MangaPage::model()->findByPk($objId['id']);
			$count += $page->fix();
		}
		echo "\n Fix $count images successfully \n";
	}
	
	/*
	 * api nay khong lien quan den flickr status
	 * quet tat ca page link. Neu link nao bi loi thi thay vao day 1 link default & dat status = 3 (link hong)
	 */
	public function actionCheckLink() {
		echo "Start scanning: " . date("Y-m-d H:i:s", time())."\n";
		$countEnable = 0;
		$countDisable = 0;
		$count = 0;
		$minId = 0;
		$maxId = 0;
		$interval = 100000;
		$continue = true;
		while ($continue) {
			$sql = "select id from manga_page where id > $minId order by id asc limit $interval";
			echo "\n $sql \n";
			$allPageId = MangaPage::model()->findAllBySql($sql);
			$arrSize = count($allPageId);
			if($arrSize < 1) {
				$continue = false;
				break;
			}
			else {
				$maxId = $allPageId[$arrSize - 1]['id'];
			}
			echo "$minId - $maxId \n";
			
			foreach($allPageId as $objId) {
				$count++;
				if($count > 3000000) {
					$continue = false;
					break;
				}
				$page = MangaPage::model()->findByPk($objId['id']);
				if($page->checkLink() > 0) {
					$countEnable++;
				}
				else if($page->checkLink() < 0) {
					$countDisable++;
					echo $page->image_url1."\n";
				}
				// 			if($countDisable > 3) {
				// 				break;
				// 			}
			}
			$minId = $maxId;
		}
		
		echo "\n Enabled $countEnable pages, disabled $countDisable pages successfully \n";
		echo "\nFinished scanning: " . date("Y-m-d H:i:s", time())."\n";
	}
	
	public function actionTestCheckLink() {
		$file = "http://4.bp.blogspot.com/-AeIj-timOZU/T-KcrMqXXYI/AAAAAAAAgfQ/0adH_YIJoeo/s0/Sazan-v40-c04-p050-051.jpg?imgmax=1600";
		$file_headers = @get_headers($file);
		echo "$file_headers[0] \n";
		if(strpos($file_headers[0],'200') > 0) {
			echo "good link\n";
		}
		else {
			echo "bad link\n";
		}
		
		$file = "http://14.bp.blogspot.com/-AeIj-timOZU/T-KcrMqXXYI/AAAAAAAAgfQ/0adH_YIJoeo/s0/Sazan-v40-c04-p050-051.jpg?imgmax=1600";
		$file_headers = @get_headers($file);
		echo "$file_headers[0] \n";
		if(strpos($file_headers[0],'200') > 0) {
			echo "good link\n";
		}
		else {
			echo "bad link\n";
		}
	}
}

?>



