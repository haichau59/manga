<?php
define('VNSHARING_URL', 'http://truyen.vnsharing.net');
/**
 * crawl data from web site, insert into database mangaVNsharing
*/

require('phpQuery/phpQuery.php');

class CrawlDataCommand extends CConsoleCommand{
	const RESTART=false;
	
	function getData($url, $vns_cannotread = true) {
		$ch = curl_init();
		$timeout = 15;
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18' );
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		if($vns_cannotread) { //them cookie vns_cannotread=1 de lay image url tren server vnsharing (vns_cannotread=0, hoac ko set thi tu server picasa)
			curl_setopt($ch, CURLOPT_COOKIE, "vns_Adult=yes;vns_cannotread=1");
		}
		else {
			curl_setopt($ch, CURLOPT_COOKIE, "vns_Adult=yes");
		}
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	//chi chay 1 lan de lay danh sach category
	public function actionParseCategoryVnSharing() {
		$url = VNSHARING_URL."/Danhsach";
		$content = $this->getData($url);//FIXME dung dong nay, xoa dong duoi
// 		$content = file_get_contents('vnsharing.html');
		phpQuery::newDocumentHTML($content);
		$categoryList = pq('body div#containerRoot div#container div#rightside div.rightBox div.barContent');
		// 		echo "************************************************\n";
		$i = 0;
		foreach($categoryList['div a'] as $a) {
			$i++;
			if($i < 4) continue;
			$a = pq($a);
			// 			echo $a->text()."\n";
			$displayName = trim($a->text());
			$codeName = CVietnameseTools::makeCodeName($displayName);
			$description = $a->attr('title');
			$link = $a->attr('href');
			$category = MangaCategory::model()->findByAttributes(array('code_name'=>$codeName));
			if($category == NULL) {
				$category = new MangaCategory();
				$category->display_name = $displayName;
				$category->code_name = $codeName;
				$category->description = $description;
				$category->image_url2 = $link;
				$category->create_date = new CDbExpression('NOW()');
				if(!$category->save()) {
					print_r($category->getErrors());
				}
			}
		}
	}

	public function actionParseVnSharing() {
		//bat dau parse tu root trang web
		$url = VNSHARING_URL."/Danhsach";
				$content = $this->getData($url);//FIXME dung dong nay, xoa dong duoi
// 		$content = file_get_contents('vnsharing.html');
		phpQuery::newDocumentHTML($content);
		$paginationList = pq('body div#containerRoot div#container div#leftside div.bigBarContainer div.pagination.pagination-left ul.pager');
		// 		echo $paginationList->text(); return;
		foreach($paginationList['li a'] as $li) {
			$pageIndex = pq($li)->attr('page');
		}
		$pageCount = $pageIndex;
				echo "\n pageCount = $pageCount\n";
// 		for($i = $pageCount; $i > 0; $i--) {
		for($i = 1; $i <= $pageCount; $i++) {
//			if($i < 25) continue; //FIXME: xoa dong nay
			$this->parsePage($i);
			echo " \n parsed page $i \n ";
// 			if($i > 5) { //FIXME chi parse 5 page (tong cong la >=58 page). Chay that thi xoa dong nay
// 				return;
// 			}
		}
	}

	//parse tung trang danh sach truyen. Tim link cua tung manga trong danh sach -> goi parseMangaPage
	public function parsePage($page) {
		$url = VNSHARING_URL."/DanhSach/Moi?page=$page";
		$content = $this->getData($url);//FIXME dung dong nay, xoa dong duoi
		// 		$content = file_get_contents('vnsharing.html');
		phpQuery::newDocumentHTML($content);
		$mangaList = pq('body div#containerRoot div#container div#leftside div.bigBarContainer div.barContent div table.listing');
		$mangaCountPerPage = 0;
		foreach($mangaList['tr'] as $mangaItem) {
			$mangaItem = pq($mangaItem);
			$i = 0;
			$linkManga = $linkTransGroup = $statusStr = "";
			foreach($mangaItem['td'] as $td) {
				$td = pq($td);
				$i++;
				switch($i) {
					case 1:
						$a = pq($td['a']);
						$linkManga = trim($a->attr('href'));
						break;
					case 2:
						$a = pq($td['a']);
						$linkTransGroup = $a->attr('href');
						break;
					case 3:
						$statusStr = $td->text();
						break;
				}
			}
// 			 			echo " - link manga: $linkManga \n";
// 			 			echo " - linkTransGroup: $linkTransGroup";
// 			 			echo " - statusStr: $statusStr";
			if(strpos($linkManga,"Truyen") > 0) {
				$orderNumber = 100*$page + $mangaCountPerPage;
				$this->parseMangaPage($linkManga, $orderNumber);
				$mangaCountPerPage++;
	// 			if($mangaCountPerPage>100) return; //FIXME xoa dong nay
			}
		}
	}

	//parse tung truyen
	public function parseMangaPage($linkManga, $orderNumber) {
		$linkManga = VNSHARING_URL.$linkManga;
				$content = $this->getData($linkManga);//FIXME dung dong nay, xoa dong duoi
// 		$content = file_get_contents('manga_example.html');
		phpQuery::newDocumentHTML($content);
		$mangaDetail = pq('body div#containerRoot div#container div#leftside div.bigBarContainer div.barContent');
		$i = 0;
		$mangaTitle = "".$mangaDetail['div a.bigChar']->text();
// 		echo "\n title : $mangaTitle \n";
		//manga description *** start
		$anotherName = '';
		$category = '';
		$author = '';
		$transGroup = '';
		$transGroupDescription = '';
		$statusStr = '';
		$viewCount = '';
		$followCount = '';
		$mangaDescription = '';

		$startOfIndexJ = 1;
		$source = print_r($mangaDetail, true);
		if(strpos($source, "Tên khác:") > 0) {
			$startOfIndexJ = 0;
		}
		foreach($mangaDetail['div'] as $detailItem) {
			$i++;
			if($i == 2) {
				$detailItem = pq($detailItem);
				$j = $startOfIndexJ;
				foreach($detailItem['p'] as $p) {
					$j++;
					$p = pq($p);
					switch($j) {
						case 1: //ten khac
							$anotherName = trim($p->text());
							break;
						case 2:
							$category = trim($p->text());
							$category = substr($category, strpos($category, ":") + 1);
							break;
						case 3: //author
							foreach($p['a.dotUnder'] as $a) {
								$a = pq($a);
								$author .= trim($a->text()).", ";
							}
							if(strpos($author, ",") > 0) {
								$author = substr($author, 0, strlen($author)-2);
							}
							break;
						case 4: //translator group
							$transGroup = trim($p['a']->text());
							$transGroupDescription = trim($p['a']->attr('title'));
							break;
						case 5:
							$fullStatusStr = trim($p->text());
							$statusStr = $fullStatusStr; //$p['span.info'][0]->text();
							$viewCount = "";  //$p['span.info'][1]->text();
							$followCount = ""; //$p['span.info'][2]->text();
							break;
						case 6:
							break;
						case 7:
							$mangaDescription = trim($p->text()); //$p['span.strong'][0]->text() . $p['span.strong'][1]->text();
							break;
					}
				}
// 								echo $anotherName." - ".$author." - ".$transGroup." - ".$transGroupDescription."\n";//.$statusStr." - ".$viewCount." - ".$followCount;
// 								echo "Description: \n".$mangaDescription;
// 								echo "Title: $mangaTitle\n";
// 								echo "Category: $category\n";
// 								echo "Author: $author\n";
// 								echo "TransGroup: $transGroup\n";
				break;
			}
		}
		//manga description *** end

		//manga cover *** start
		$mangaCover = pq('body div#containerRoot div#container div#rightside div.rightBox div.barContent div img')->attr('src');
		// 		echo "\nCover: ".$mangaCover;
		//manga cover *** end

		
		$manga = $this->createNewManga($mangaTitle, $category, $mangaCover, $mangaDescription, $author, $anotherName, $transGroup, $transGroupDescription, $statusStr, $viewCount, $followCount, $orderNumber);
		if($manga == NULL) {
			return;
		}
		echo "\n created manga: $mangaTitle \n"; 
		//chapter list *** start
		$chapterLink = "";
		$chapterModifyDate = "";
		$chapterDownloadLink = "";
		$chapterListPq = pq('body div#containerRoot div#container div#leftside div.bigBarContainer div.barContent.chapterList div table.listing');
		$k = 0;
		$arrChapter = Array();
		foreach($chapterListPq['tr'] as $tr) {
			$k++;
			// 			echo " - foreach tr $k";
			if($k > 2) { // bat dau tu <tr> thu 3 tro di thi liet ke cac chapter
				$tr = pq($tr);
				$l = 0;
				foreach($tr['td'] as $td) {
					$l++;
					// 					echo " - foreach td $l";
					$td = pq($td);
					switch($l) {
						case 1:
							$chapterLink = trim($td['a']->attr('href'));
							$chapterName = trim($td['a']->attr('title'));
							break;
						case 2:
							$chapterModifyDate = trim($td->text());
							break;
						case 3:
							$chapterDownloadLink = trim($td['a']->attr('href'));
							break;
					}
				}
// 				echo "\n Manga chapter: \n";
// 				echo "$chapterLink *** $chapterModifyDate *** $chapterDownloadLink";
// 				echo " *** \nStart parse chapter *** ";
				$chapterObj['link'] = $chapterLink;
				$chapterObj['chapterName'] = $chapterName;
				$chapterObj['downloadLink'] = $chapterDownloadLink;
				$chapterObj['modifyDate'] = $chapterModifyDate;
				array_push($arrChapter, $chapterObj); //push vao array $arrChapter de thay doi thu tu chapter nguoc lai cho dung
			}
		}
		$chapterCount = MangaChapter::model()->countByAttributes(array('manga_id' => $manga->id, 'status' => 1));
// 		echo "\n chapterCount : $chapterCount \n";
		if($chapterCount == sizeof($arrChapter)) { //truyen nay da parse xong day du chapter roi, kiem tra them neu ko self::RESTART thi thoi
// 		if(($chapterCount == sizeof($arrChapter) && (!self::RESTART))) { //truyen nay da parse xong day du chapter roi, kiem tra them neu ko self::RESTART thi thoi
			//hien tai so chapter trong database == so chapter tren trang VNSharing
			echo "\n full chapter of manga ".$manga->id."\n";
			return;
		}

		$chapterOrder = 0;
		foreach($arrChapter as $chapterObj) {
			$chapterOrder++;
			$chapter = MangaChapter::model()->findByAttributes(array('manga_id' => $manga->id, 'chapter_order' => $chapterOrder));
			if($chapter == NULL) {
				$chapter = new MangaChapter();
				$chapter->code_name = $chapterOrder;
				$chapter->display_name = $chapterObj['chapterName'];
				$chapter->manga_id = $manga->id;
				$chapter->chapter_order = $chapterOrder;
				$chapter->download_url1 = $chapterObj['downloadLink'];
				if(!$chapter->save()) {
					echo "\n save chapter failed \n";
					print_r($chapter->getErrors());
				}
				else {
					$this->parseChapter($chapterObj['link'], $chapter, true);
					$this->parseChapter($chapterObj['link'], $chapter, false);
				}
			}
// 			else if (self::RESTART){
// 				$this->parseChapter($chapterObj['link'], $chapter);
// 			}
			//chapter list *** end
		}
	}

	public function parseChapter($chapterLink, $chapter, $vns_cannotread = true) {
		$linkChapter = VNSHARING_URL.$chapterLink;
		$content = $this->getData($linkChapter, $vns_cannotread);//FIXME dung dong nay, xoa dong duoi
// 		$content = file_get_contents('chapter_example.html');

		phpQuery::newDocumentHTML($content);
		$chapterDetail = pq('body div#containerRoot div div#divImage');
		$source = "";
		$source = print_r($chapterDetail, true);
		$start = strpos($source, "var lstImages = new Array") + 10;
		$len = strpos($source, "var currImage = 0") - 10 - $start;
		$source = substr($source, $start, $len);
		$arrImgStr = explode("lstImages.push", $source);
		$n = sizeof($arrImgStr);
		$delta = 0;
		for($i = 1; $i < $n; $i++) {
			$arrImgStr[$i] = trim($arrImgStr[$i]);
			if(strlen($arrImgStr[$i]) < 10) {
				$delta++;
				continue;
			}
			$url = trim(substr($arrImgStr[$i], 2, strlen($arrImgStr[$i]) - 5));
// 			echo "\n***$url***\n"; continue;
			
			if(strlen($url) < 10) continue;
			
			$mangaPage = MangaPage::model()->findByAttributes(array('manga_chapter_id' => $chapter->id, 'page_number' => $i));
			if($mangaPage != NULL) { //da co page nay roi, chung to chapter nay da dc parse & collect xong, return luon
// 				if(!self::RESTART) {
// 					return;
// 				}
			}
			else {
				$mangaPage = new MangaPage();
				$mangaPage->manga_chapter_id = $chapter->id;
				$mangaPage->page_number = $i - $delta;
			}
			$needToSavePage = false;
			if(strpos($url, 'bp.blogspot.com') > 0) { //link Picasa
				if($mangaPage->image_url1 != $url) {
					$mangaPage->image_url1 = $url;
					$needToSavePage = true;
				}
			}
			else { //link server VNSharing
				if($mangaPage->image_url2 != $url) {
					$mangaPage->image_url2 = $url;
					$needToSavePage = true;
				}
			}
			
			if($needToSavePage) {
				if(!$mangaPage->save()) {
					echo "\n save mangaPage failed \n";
					print_r($mangaPage->getErrors());
				}
			}
		}
	}

	private function createNewManga($mangaTitle, $category, $mangaCover, $mangaDescription, $author, $anotherName, $transGroup, $transGroupDescription, $statusStr, $viewCount, $followCount, $orderNumber) {
		if(strlen($mangaTitle) < 3) return NULL;
// 		echo "\n mangaTitle: $mangaTitle \n";
		$isNewManga = false;
		$newTransGroup = TranslatorGroup::model()->findByAttributes(array('display_name' => $transGroup));
		if($newTransGroup == NULL) {
			$isNewManga = true; //manga co nhom dich moi thi chac chan la new manga roi (co the trung ten, nhung van luu 1 record manga moi)
			$newTransGroup = new TranslatorGroup();
			$newTransGroup->display_name = $transGroup;
			$newTransGroup->short_description = substr($transGroupDescription, 0, 450);
			$newTransGroup->description = $transGroupDescription;
			$newTransGroup->create_date = new CDbExpression('NOW()');
			if(!$newTransGroup->save()) {
				echo "\n save newTransGroup failed \n";
				print_r($newTransGroup->getErrors());
			}
		}
		
		$newManga = Manga::model()->findByAttributes(array('display_name' => $mangaTitle));
		if(($newManga != NULL) && (!$isNewManga)) { //tim xem manga nay cung ten nhung co cung nhom dich hay ko
			if($newTransGroup->display_name != $transGroup) {
				$isNewManga = true;
			}			
		}
		else {
			$isNewManga = true;
		}
		if($isNewManga) {
			$newManga = new Manga();
			$newManga->display_name = $mangaTitle;
			$newManga->display_name_ascii = CVietnameseTools::makeSearchableStr($newManga->display_name);
			// 			$newManga->chapter_count = $allMangaArr[$i]->TongSoChuong;
			$newManga->short_description = substr($mangaDescription, 0, 450);
			$newManga->description = $mangaDescription;
			$newManga->manga_type = 0; //0 la truyen tranh
			$newManga->is_free = 1;
			$newManga->create_date = new CDbExpression('NOW()');
			$newManga->view_count = $viewCount;
			$newManga->like_count = $followCount;
			$newManga->update_date = new CDbExpression('NOW()');
			$newManga->order_number = $orderNumber;
			if(!$newManga->save()) {
				echo "\n save newManga failed \n";
				print_r($newManga->getErrors());
			}
		}
		else {
			$newManga->order_number = $orderNumber;
			$newManga->view_count = $viewCount;
			$newManga->like_count = $followCount;
			$newManga->other_name = $anotherName;
			$newManga->update_date = new CDbExpression('NOW()');
			$newManga->update();
		}

		//     		if($allMangaArr[$i]->ChuongHienTai < $allMangaArr[$i]->TongSoChuong) {
		// 	    		$obj->content_status = Manga::CONTENT_STATUS_RUNNING;
		//     		}
		//     		else {
		// 	    		$obj->content_status = Manga::CONTENT_STATUS_FINISHED;
		//     		}

// 		$isNewManga = true;//FIXME xoa dong nay
		$cover = MangaCover::model()->findByAttributes(array('manga_id' => $newManga->id));
		if($isNewManga && $cover == NULL) {
			// tao 1 cover moi
			$cover = new MangaCover();
			$cover->url = $mangaCover;
			$cover->create_date = new CDbExpression('NOW()');
			$cover->manga_id = $newManga->id;
			if(!$cover->save()) {
				echo "\n save cover failed \n";
				print_r($cover->getErrors());
			}
		}
		if($isNewManga) {
			//link voi author
			$newAuthor = Author::model()->findByAttributes(array('display_name' => $author));
			if($newAuthor == NULL) {
				$newAuthor = new Author();
				if(strlen($author) < 3) {
					$newAuthor = NULL;
				}
				else {
					$newAuthor->display_name = $author;
					if(!$newAuthor->save()) {
						echo "\n save newAuthor failed \n";
						print_r($newAuthor->getErrors());
					}
				}
			}
			if($newAuthor != NULL) {
				$newManga->author_id = $newAuthor->id;
			}

			$newTransMapping = new MangaTranslatorMapping();
			$newTransMapping->manga_id = $newManga->id;
			$newTransMapping->translator_group_id = $newTransGroup->id;
			$newTransMapping->create_date = new CDbException('NOW()');
			if(!$newTransMapping->save()) {
				echo "\n save newTransMapping failed \n";
				print_r($newTransMapping->getErrors());
			}

// 			echo "\n category: $category \n";
			$arrCatName = explode(",", $category);
			foreach($arrCatName as $catName) {
				$catName = trim($catName);
				echo $catName." - ";
				$cat = MangaCategory::model()->findByAttributes(array('display_name' => $catName));
				if($cat == NULL) {
					echo "\nko thuoc category nao\n";
					continue; //category chi tao moi bang command ParseCategoryVnSharing
				}

				$newCategoryMapping = MangaCategoryMapping::model()->findByAttributes(array('manga_id' => $newManga->id, 'manga_category_id' => $cat->id));
				if($newCategoryMapping == NULL) {
					$newCategoryMapping = new MangaCategoryMapping();
					$newCategoryMapping->manga_id = $newManga->id;
					$newCategoryMapping->manga_category_id = $cat->id;
					if(!$newCategoryMapping->save()) {
						echo "\n save newCategoryMapping failed \n";
						print_r($newCategoryMapping->getErrors());
					}
				}
			}

			$newManga->update();
		}

		return $newManga;
	}
	
	public function actionUpdateChapterCount() {
		$allManga = Manga::model()->findAll();
		$i = 0;
		foreach($allManga as $manga) {
			$chapterCount = MangaChapter::model()->countByAttributes(array('manga_id' => $manga->id, 'status' => 1));
			if($manga->chapter_count != $chapterCount) {
				$manga->chapter_count = $chapterCount;
				if(!$manga->update()) {
					print_r($manga->getErrors());					
				}
				else {
					$i++;
				}
			}
		}
		echo "\n update chapter count for $i manga successfully \n";
	}
}
