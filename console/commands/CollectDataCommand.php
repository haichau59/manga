<?php
/**
 * collect data by api from http://services.mobitruyen.com 
 */


//dinh nghia cac url get data from blog truyen
define('GetAllAuthor', 'http://services.mobitruyen.com/ServiceManga.svc/GetAllAuthor');
define('GetBestManga', 'http://services.mobitruyen.com/ServiceManga.svc/GetBestManga');
define('GetListManga', 'http://services.mobitruyen.com/ServiceManga.svc/GetListManga?v=0&lang=vi');
define('GetAllCommentOfManga', 'http://services.mobitruyen.com/ServiceManga.svc/GetAllCommentOfManga'); //?Id=%d 
define('GetChaperOfManga', 'http://services.mobitruyen.com/ServiceManga.svc/GetChaperOfManga'); //?v=%d&Id=%d
define('GetLinkOfChapter', 'http://services.mobitruyen.com/ServiceManga.svc/GetLinkOfChapter'); //?Id=%d&U=%s
define('GetDetailOfManga', 'http://services.mobitruyen.com/ServiceManga.svc/GetDetailOfManga'); //?Id=%d

class CollectDataCommand extends CConsoleCommand{
    public function actionGetAllAuthor() {
    	echo "start get all author \n";
    	$allAuthorJson = file_get_contents(GetAllAuthor);
    	$tmp = json_decode($allAuthorJson);
//     	print_r($tmp); return;
    	$allAuthorArr = $tmp->FEED;
//     	print_r($allAuthorArr[0]->TenTacGiaTruyen); return;
		$n = sizeof($allAuthorArr);
// 		print("count author = $n \n"); return;
		for($i = 0; $i < $n; $i++) {
			$tmpAuthor = Author::model()->findByPk($allAuthorArr[$i]->TacGiaTruyenID);
			if($tmpAuthor != NULL) continue;
    		$obj = new Author();
    		$obj->id = $allAuthorArr[$i]->TacGiaTruyenID;
    		$obj->display_name = $allAuthorArr[$i]->TenTacGiaTruyen;
    		$obj->save();
    	}
    }

    public function actionGetAllManga() {
    	echo "start get all manga \n";
    	$allMangaJson = file_get_contents(GetListManga);
    	$tmp = json_decode($allMangaJson);
//     	print_r($tmp); return;
    	$allMangaArr = $tmp->FEED;
//     	print_r($allMangaArr[0]->HinhGoc); return;
		$n = sizeof($allMangaArr);
// 		$testCount = 5; //dung khi chay test thoi
		for($i = 0; $i < $n; $i++) {
			try {
// 				if($i >= $testCount) {
// 					break;
// 				}
				if($allMangaArr[$i]->TruyenID == 0) continue;
				$obj = Manga::model()->findByPk($allMangaArr[$i]->TruyenID);
				$isNewManga = false;
				if($obj == NULL) {
					$isNewManga = true;
		    		$obj = new Manga();
		    		$obj->id = $allMangaArr[$i]->TruyenID;
		    		$obj->create_date = new CDbExpression('NOW()');
				}
				
				$this->createCategoryMapping($obj, $allMangaArr[$i]->StrDanhSachLoaiTruyen);
				
				if(!$isNewManga) {
					$obj->content_status = $allMangaArr[$i]->TrangThaiID;
					if(!$obj->save()) {
						print_r($obj->getErrors());
						//     			return;
					}
					continue;
// 					$obj->update_date = new CDbExpression('NOW()');
// 					if(!$obj->save()) {
// 						print_r($obj->getErrors());
// 						//     			return;
// 					}
				}
				
	    		$obj->display_name = $allMangaArr[$i]->TenTruyen;
	    		$obj->display_name_ascii = CVietnameseTools::makeSearchableStr($obj->display_name);
	// 	    	echo 'aaaa'; return;
	//     		$obj->save();
	//     		echo $obj->display_name; return;
	    		$obj->chapter_count = $allMangaArr[$i]->TongSoChuong;
	    		$obj->short_description = $allMangaArr[$i]->StrDanhSachLoaiTruyen;
	    		$obj->description = $allMangaArr[$i]->MoTaTruyen;
	    		$obj->view_count = $allMangaArr[$i]->SoNguoiDoc;
	    		$obj->like_count = $allMangaArr[$i]->SoNguoiThich;
	    		$obj->update_date = new CDbExpression('NOW()');
	    		$obj->manga_type = 0; //0 la truyen tranh
	    		$obj->is_free = 1;
 	    		$obj->content_status = $allMangaArr[$i]->TrangThaiID; 

				if(Author::model()->findByPk($allMangaArr[$i]->TacGiaTruyenID) != NULL) {
		    		$obj->author_id = $allMangaArr[$i]->TacGiaTruyenID;
				}
				if(!$obj->save()) {
	    			print_r($obj->getErrors());
	    			continue;
	    		} 

	    		$cover = MangaCover::model()->findByAttributes(array('manga_id' => $obj->id));
	    		if($isNewManga && ($cover == NULL)) {
	    			echo "\ntao 1 cover moi for ".$obj->id ." - ".$obj->display_name."\n";
	    			$cover = new MangaCover();
	    			$cover->url = $allMangaArr[$i]->HinhGoc;
	    			$cover->create_date = new CDbExpression('NOW()');
	    			$cover->manga_id = $obj->id;
	    			if(!$cover->save()) {
	    				print_r($cover->getErrors());
	    			}
	    		}
	    		echo "Manga $obj->display_name \n";
			}
			catch(Exception $e) {
				echo $e->getMessage();
			}
	    }
    }
    
    private function createCategoryMapping($manga, $strCategories) {
    	$arrCatName = explode(",", $strCategories);
    	foreach($arrCatName as $catName) {
    		$catName = trim($catName);
    		if(strlen($catName) < 3) return NULL;
    		$category = MangaCategory::model()->findByAttributes(array('display_name' => $catName));
    		if($category == NULL) {
    			$category = new MangaCategory();
				$category->display_name = $catName;
				$category->code_name = CVietnameseTools::makeCodeName($catName);
				$category->create_date = new CDbExpression('NOW()');
				if(!$category->save()) {
					print_r($category->getErrors());
				}
    		}
    	
    		$newCategoryMapping = MangaCategoryMapping::model()->findByAttributes(array('manga_id' => $manga->id, 'manga_category_id' => $category->id));
    		if($newCategoryMapping == NULL) {
	    		$newCategoryMapping = new MangaCategoryMapping();
	    		$newCategoryMapping->manga_id = $manga->id;
	    		$newCategoryMapping->manga_category_id = $category->id;
	    		if(!$newCategoryMapping->save()) {
	    			echo "\n save newCategoryMapping failed \n";
	    			print_r($newCategoryMapping->getErrors());
	    		}
    		}
    	}
    }
    
    public function actionGetAllChapter() {
    	echo "start get all chapter \n";
		$allManga = Manga::model()->findAllByAttributes(array('status'=>1));
		$count = 0;
		foreach($allManga as $manga) {
	    	$count++;
// 	    	if($count > 3) {
// 	    		break;
// 	    	}
			$countChapter = MangaChapter::model()->countByAttributes(array("manga_id"=>$manga->id));
			if(($countChapter == $manga->chapter_count) && ($countChapter > 0)) {
				continue;
			}
	    	try {	
				$urlGetListChapter = GetChaperOfManga."?v=0&Id=$manga->id";
				$listChapterJson = file_get_contents($urlGetListChapter);
				
		    	$tmp = json_decode($listChapterJson);
		    	//     	print_r($tmp); return;
		    	$arrChapter = $tmp->FEED;
	//     	    print_r($arrChapter[0]->LinkNguon); return;
		    	$n = sizeof($arrChapter);
	// 	    	echo $n; return;
		    	for($i = 0; $i < $n; $i++) {
		    		try {
		    			echo " i = $i \n";
						$chapter = MangaChapter::model()->findByPk($arrChapter[$i]->ChuongTruyenID);
						if($chapter == NULL) {
			    			$chapter = new MangaChapter();    			
							$chapter->id = $arrChapter[$i]->ChuongTruyenID;
						}
						else { //FIXME xoa doan nay de update lai chapter
							if($chapter->manga_id != $manga->id) {
								$chapter->manga_id = $manga->id;
								$chapter->update();
							}
							else {
								echo "\nthis chapter ".$arrChapter[$i]->ChuongTruyenID." is existed\n"; 
								continue;
							}
						}
						
						$chapter->create_date = $arrChapter[$i]->NgayTao;
						$chapter->modify_date = $arrChapter[$i]->NgayCapNhat;
						$chapter->page_count = $arrChapter[$i]->SoTrangHienTai;
						$chapter->display_name = $arrChapter[$i]->TenChuongTruyen;
						$chapter->code_name = CVietnameseTools::makeCodeName($arrChapter[$i]->TenChuongTruyen);
						$chapter->chapter_order = $arrChapter[$i]->ThuTuChuong;
						$chapter->page_count = $arrChapter[$i]->TongSoTrang;
						$chapter->manga_id = $arrChapter[$i]->TruyenID;
						$chapter->download_url1 = $arrChapter[$i]->LinkNguon;
						echo $chapter->manga_id;
						if(!$chapter->save()) {
							print_r($chapter->getErrors());
						}
		    		}
		    		catch(Exception $e) {
		    			
		    		}
		    	}
	    	}
    		catch(Exception $e) {
    			
    		}
		}
		echo "collect all chapter of $count manga";
    }

    public function actionGetAllPage() {
    	echo "start get all page \n";
    	$criteria=new CDbCriteria;
    	$criteria->distinct = true;
    	$criteria->select = 'id';
    	$criteria->addCondition('t.status = 1 and t.collect_status = 0 order by id desc ');
    	
		$allChapter = MangaChapter::model()->findAll($criteria);
		$count = 0;
		foreach($allChapter as $chapter) {
			$chapterId = $chapter->id;
			$countPageOfChapter = MangaPage::model()->countByAttributes(array("manga_chapter_id"=>$chapterId));
			if($countPageOfChapter > 10) {
				$chapter->collect_status = 1;
				$chapter->update();
				continue;
			}
	    	$count++;
// 	    	if($count > 3) {
// 	    		break;
// 	    	}

	    	
			$urlGetLinkOfChapter = GetLinkOfChapter."?Id=$chapterId&U=1";
// 			echo "$urlGetLinkOfChapter \n"; 
			try {
				$listPageJson = file_get_contents($urlGetLinkOfChapter);
				
		    	$tmp = json_decode($listPageJson);
		    	if($tmp == NULL) {
		    		continue;
		    	}
		    	//     	print_r($tmp); return;
		    	$arrPageOfChapter = $tmp->FEED;
	//     	    print_r($arrChapter[0]->LinkNguon); return;
		    	$n = sizeof($arrPageOfChapter);
		    	if($n > 10) { //chapter co hon 10 trang roi thi chac chan la da hoan thanh update image
		    		 $chapter->collect_status = 1;
		    		$chapter->update();
		    	}
	// 	    	echo $n; return;
		    	for($i = 0; $i < $n; $i++) {
		    		try {
	// 	    			echo " i = $i \n";
						$page = MangaPage::model()->findByPk($arrPageOfChapter[$i]->HinhID);
						if($page == NULL) {
			    			$page = new MangaPage();    			
							$page->id = $arrPageOfChapter[$i]->HinhID;
						}
						else {
							continue;
						}
						
						$page->manga_chapter_id = $chapterId;
						$page->image_url1 = $arrPageOfChapter[$i]->HinhGoc;
						$page->image_url2 = $arrPageOfChapter[$i]->LinkGoc;
						$page->page_number = $arrPageOfChapter[$i]->SoHienTai;
						if(!$page->save()) {
							print_r($page->getErrors());
						}
		    		}
		    		catch(Exception $e) {
		    			
		    		}
		    	}
			}
	   		catch(Exception $e) {
	   			
	   		}
		}
		echo "collect all page of $count chapter";
    }
    
    public function actionFixChapterCount() { //duyet tat ca manga, update lai chapter_count cua manga. Manga nao co chapter_count = 0 thi disable manga ay di
    	echo "start chapter count\n";
    	$allManga = Manga::model()->findAllByAttributes(array('status'=>1));
    	$countDisabledManga = 0;
    	$countFixChapterCount = 0;
    	foreach($allManga as $manga) {
    		$chapterCount = MangaChapter::model()->countByAttributes(array("manga_id" => $manga->id));
    		if($chapterCount == 0) {
    			$manga->status = 2;
    			$manga->update();
    			$countDisabledManga++;
    		}
    		else {
    			if($manga->chapter_count != $chapterCount) {
	    			$manga->chapter_count = $chapterCount;
	    			$manga->update();
	    			$countFixChapterCount++;
    			}
    		}
    	}    	
    	echo "\ncountDisabledManga = $countDisabledManga ; countFixChapterCount = $countFixChapterCount\n";
    }
    
    public function actionFixPageCount() {
    	echo "start fix page count\n";
    	$criteria=new CDbCriteria;
    	$criteria->distinct = true;
    	$criteria->select = 'id, page_count';
    	$criteria->addCondition('t.status = 1 and t.collect_status = 0 order by id desc ');
    	$allChapter = MangaChapter::model()->findAll($criteria);
    	$countFixChapter = 0;
    	$countDisableChapter = 0;
		foreach($allChapter as $chapter) {
			$chapterId = $chapter->id;
			$countPageOfChapter = MangaPage::model()->countByAttributes(array("manga_chapter_id"=>$chapterId));
			if($countPageOfChapter == 0) {
				$chapter->status = 2;
				$chapter->update();
				$countDisableChapter++;
			}
			if($countPageOfChapter != $chapter->page_count) {
				$chapter->page_count = $countPageOfChapter;
				$chapter->update();
				$countFixChapter++;
			}
		}
		echo "\ncountFixChapter = $countFixChapter; countDisableChapter = $countDisableChapter\n";
    }
    
    public function actionFixPageUrl() {
    	$criteria=new CDbCriteria;
    	$criteria->distinct = true;
    	$criteria->select = 'id';
    	$criteria->addCondition("t.status = 1 and t.image_url1 like '%*&url=%'");
    	$allPageId = MangaPage::model()->findAll($criteria);
    	foreach($allPageId as $page) {
    		$mangaPage = MangaPage::model()->findByPk($page['id']);
    		$mangaPage->image_url1 = substr($mangaPage->image_url1, strpos($mangaPage->image_url1, "*&url=") + 6);
    		$mangaPage->image_url2 = substr($mangaPage->image_url2, strpos($mangaPage->image_url2, "*&url=") + 6);
    		$mangaPage->image_url3 = substr($mangaPage->image_url3, strpos($mangaPage->image_url3, "*&url=") + 6);
    		$mangaPage->update();
    	}
    	echo "\nfix done\n";
    }
}

?>



