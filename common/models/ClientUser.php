<?php

/**
 * This is the model class for table "client_user".
 *
 * The followings are the available columns in table 'client_user':
 * @property integer $id
 * @property string $subscriber_number
 * @property string $user_name
 * @property integer $status
 * @property string $email
 * @property string $full_name
 * @property string $password
 * @property string $last_login_time
 * @property integer $last_login_session
 * @property string $birthday
 * @property integer $sex
 * @property string $avatar_url
 * @property string $yahoo_id
 * @property string $skype_id
 * @property string $google_id
 * @property string $zing_id
 * @property string $facebook_id
 * @property string $create_date
 * @property integer $account_type
 *
 * The followings are the available model relations:
 * @property ApplicationSubscriberMapping[] $applicationSubscriberMappings
 * @property SubscriberSession $lastLoginSession
 * @property Device[] $devices
 * @property LiveSearchHistory[] $liveSearchHistories
 * @property Manga[] $mangas
 * @property MangaChapter[] $mangaChapters
 * @property MangaComment[] $mangaComments
 * @property MangaLikeDislike[] $mangaLikeDislikes
 * @property MangaRating[] $mangaRatings
 * @property MangaSearchHistory[] $mangaSearchHistories
 * @property MangaUserBookmark[] $mangaUserBookmarks
 * @property MangaUserSubscription[] $mangaUserSubscriptions
 * @property SubscriberContentAccessLog[] $subscriberContentAccessLogs
 * @property SubscriberSession[] $subscriberSessions
 * @property UserFeedback[] $userFeedbacks
 */
class ClientUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ClientUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name', 'required'),
			array('status, last_login_session, sex, account_type', 'numerical', 'integerOnly'=>true),
			array('subscriber_number', 'length', 'max'=>45),
			array('user_name', 'length', 'max'=>100),
			array('email', 'length', 'max'=>300),
			array('full_name, password', 'length', 'max'=>200),
			array('avatar_url, yahoo_id, skype_id, google_id, zing_id, facebook_id', 'length', 'max'=>255),
			array('last_login_time, birthday, create_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, subscriber_number, user_name, status, email, full_name, password, last_login_time, last_login_session, birthday, sex, avatar_url, yahoo_id, skype_id, google_id, zing_id, facebook_id, create_date, account_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applicationSubscriberMappings' => array(self::HAS_MANY, 'ApplicationSubscriberMapping', 'subscriber_id'),
			'lastLoginSession' => array(self::BELONGS_TO, 'SubscriberSession', 'last_login_session'),
			'devices' => array(self::HAS_MANY, 'Device', 'subscriber_id'),
			'liveSearchHistories' => array(self::HAS_MANY, 'LiveSearchHistory', 'subscriber_id'),
			'mangas' => array(self::HAS_MANY, 'Manga', 'uploader_id'),
			'mangaChapters' => array(self::HAS_MANY, 'MangaChapter', 'uploader_id'),
			'mangaComments' => array(self::HAS_MANY, 'MangaComment', 'user_id'),
			'mangaLikeDislikes' => array(self::HAS_MANY, 'MangaLikeDislike', 'user_id'),
			'mangaRatings' => array(self::HAS_MANY, 'MangaRating', 'user_id'),
			'mangaSearchHistories' => array(self::HAS_MANY, 'MangaSearchHistory', 'subscriber_id'),
			'mangaUserBookmarks' => array(self::HAS_MANY, 'MangaUserBookmark', 'user_id'),
			'mangaUserSubscriptions' => array(self::HAS_MANY, 'MangaUserSubscription', 'user_id'),
			'subscriberContentAccessLogs' => array(self::HAS_MANY, 'SubscriberContentAccessLog', 'subscriber_id'),
			'subscriberSessions' => array(self::HAS_MANY, 'SubscriberSession', 'subscriber_id'),
			'userFeedbacks' => array(self::HAS_MANY, 'UserFeedback', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subscriber_number' => 'Subscriber Number',
			'user_name' => 'User Name',
			'status' => 'Status',
			'email' => 'Email',
			'full_name' => 'Full Name',
			'password' => 'Password',
			'last_login_time' => 'Last Login Time',
			'last_login_session' => 'Last Login Session',
			'birthday' => 'Birthday',
			'sex' => 'Sex',
			'avatar_url' => 'Avatar Url',
			'yahoo_id' => 'Yahoo',
			'skype_id' => 'Skype',
			'google_id' => 'Google',
			'zing_id' => 'Zing',
			'facebook_id' => 'Facebook',
			'create_date' => 'Create Date',
			'account_type' => 'Account Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subscriber_number',$this->subscriber_number,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('last_login_session',$this->last_login_session);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('avatar_url',$this->avatar_url,true);
		$criteria->compare('yahoo_id',$this->yahoo_id,true);
		$criteria->compare('skype_id',$this->skype_id,true);
		$criteria->compare('google_id',$this->google_id,true);
		$criteria->compare('zing_id',$this->zing_id,true);
		$criteria->compare('facebook_id',$this->facebook_id,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('account_type',$this->account_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function newSubscriber($msisdn) {
		$aDate = new DateTime();
		$subscriber = new ClientUser();
	
		$subscriber->subscriber_number = $msisdn;
		$subscriber->user_name = $msisdn;
		$subscriber->status = 1;
		$subscriber->create_date = $aDate->format('Y-m-d H:i:s');
	
		if($subscriber->save()) {
			return $subscriber;
		}
		return null;
	}
	
	public function newTransaction($channel_type, $using_type, $purchase_type, $service = null, $asset = null) {
		$aDate = new DateTime();
		$subscriberTransaction = new SubscriberTransaction;
	
		if ($service != null) {
			$subscriberTransaction->service_id = $service->id;
		}
		if ($asset != null) {
			$subscriberTransaction->vod_asset_id = $asset->id;
		}
	
		$subscriberTransaction->subscriber_id = $this->id;
		$subscriberTransaction->create_date = $aDate->format("Y-m-d H:i:s");
		$subscriberTransaction->status = 1; // status dung lam gi khi co error_code?
	
		if ($using_type == 1) { // dang ky dich vu
			if ($purchase_type == 2) { // gia han
				$subscriberTransaction->description = isset($service) ? $service->display_name : "";
			} else {
				$subscriberTransaction->description = isset($service) ? $service->display_name : "";
			}
			$subscriberTransaction->cost = isset($service) ? $service->price : 0;
		} else if ($using_type == 2) { // mua Truyện
			$subscriberTransaction->description = "Truyện" . (isset($asset) ? " " . $asset->display_name . " [".$asset->id."]" : "");
			$subscriberTransaction->cost = isset($asset) ? $asset->price : 0;
		} else if ($using_type == 3) { // tai Truyện
			$subscriberTransaction->description = "Truyện" . (isset($asset) ? " " . $asset->display_name . " [".$asset->id."]" : "");
			$subscriberTransaction->cost = isset($asset) ? $asset->price_download : 0;
		} else if ($using_type == 4) { // tang Truyện
			$subscriberTransaction->description = "Truyện" . (isset($asset) ? " " . $asset->display_name . " [".$asset->id."]" : "");
			$subscriberTransaction->cost = isset($asset) ? $asset->price_gift : 0;
		} else if ($using_type == 4) { // duoc tang Truyện
			$subscriberTransaction->description = "Truyện" . (isset($asset) ? " " . $asset->display_name . " [".$asset->id."]" : "");
			$subscriberTransaction->cost = 0;
		} else {
			$subscriberTransaction->cost = 0;
		}
	
		$subscriberTransaction->channel_type = $channel_type; // WEB, WAP, SMS, APP, ...
	
		//TODO:event_id dung lam gi?
		//$subscriberTransaction->event_id = '';
		$subscriberTransaction->using_type = $using_type;//0: mua dich vu, 1: mua Truyện, 2: tai Truyện, 3: tang Truyện
		$subscriberTransaction->purchase_type = $purchase_type; //0: mua moi, 1: gia han, 2: chu dong huy, 3: bi huy
		$subscriberTransaction->error_code = "UNKNOWN";
		$subscriberTransaction->save();
	
		return $subscriberTransaction;
	}
	
	public function addSubscription($manga, $sub_type) {
		if($manga == NULL) return NULL;
		$newSubscription = MangaUserSubscription::model()->findByAttributes(array('manga_id'=>$manga->id, 'user_id'=>$this->id, 'sub_type' => $sub_type));
		if($newSubscription == NULL){
			$newSubscription = new MangaUserSubscription();
			$newSubscription->manga_id = $manga->id;
			$newSubscription->user_id = $this->id;
			$newSubscription->sub_type = $sub_type;
			$newSubscription->create_date = new CDbExpression('NOW()');
			$newSubscription->save();
		}
		return $newSubscription;
	}
	
	public static function getByUsername($username) {
		$clientUser = ClientUser::model()->findByAttributes(array('user_name'=>$username));
		if($clientUser == NULL) {
			$clientUser = new ClientUser();
			$clientUser->user_name = $username;
			$clientUser->password = md5('123456');
			$clientUser->status = 1;
			$clientUser->create_date = new CDbExpression("NOW()");
			if(!$clientUser->save()) {
				return NULL;
			}
		} 
		return $clientUser;
	}
}