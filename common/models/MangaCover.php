<?php

/**
 * This is the model class for table "manga_cover".
 *
 * The followings are the available columns in table 'manga_cover':
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property integer $width
 * @property integer $height
 * @property integer $file_size
 * @property string $format
 * @property string $create_date
 * @property integer $status
 * @property integer $image_type
 * @property integer $image_size_type
 * @property integer $device_type
 * @property integer $manga_id
 *
 * The followings are the available model relations:
 * @property Manga $manga
 */
class MangaCover extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaCover the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_cover';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manga_id', 'required'),
			array('width, height, file_size, status, image_type, image_size_type, device_type, manga_id', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>500),
			array('title', 'length', 'max'=>300),
			array('format', 'length', 'max'=>40),
			array('create_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, url, title, width, height, file_size, format, create_date, status, image_type, image_size_type, device_type, manga_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'Url',
			'title' => 'Title',
			'width' => 'Width',
			'height' => 'Height',
			'file_size' => 'File Size',
			'format' => 'Format',
			'create_date' => 'Create Date',
			'status' => 'Status',
			'image_type' => 'Image Type',
			'image_size_type' => 'Image Size Type',
			'device_type' => 'Device Type',
			'manga_id' => 'Manga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('file_size',$this->file_size);
		$criteria->compare('format',$this->format,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('image_type',$this->image_type);
		$criteria->compare('image_size_type',$this->image_size_type);
		$criteria->compare('device_type',$this->device_type);
		$criteria->compare('manga_id',$this->manga_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}