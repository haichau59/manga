<?php

/**
 * This is the model class for table "manga_user_subscription".
 *
 * The followings are the available columns in table 'manga_user_subscription':
 * @property integer $id
 * @property string $create_date
 * @property integer $manga_id
 * @property integer $user_id
 * @property integer $sub_type
 *
 * The followings are the available model relations:
 * @property Manga $manga
 * @property ClientUser $user
 */
class MangaUserSubscription extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaUserSubscription the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_user_subscription';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_date, manga_id, user_id', 'required'),
			array('manga_id, user_id, sub_type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_date, manga_id, user_id, sub_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
			'user' => array(self::BELONGS_TO, 'ClientUser', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_date' => 'Create Date',
			'manga_id' => 'Manga',
			'user_id' => 'User',
			'sub_type' => 'Sub Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('manga_id',$this->manga_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('sub_type',$this->sub_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}