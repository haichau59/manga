<?php

/**
 * This is the model class for table "manga_category_mapping".
 *
 * The followings are the available columns in table 'manga_category_mapping':
 * @property integer $id
 * @property string $description
 * @property string $create_date
 * @property integer $manga_category_id
 * @property integer $manga_id
 *
 * The followings are the available model relations:
 * @property Manga $manga
 * @property MangaCategory $mangaCategory
 */
class MangaCategoryMapping extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaCategoryMapping the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_category_mapping';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manga_category_id, manga_id', 'required'),
			array('manga_category_id, manga_id', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>255),
			array('create_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, description, create_date, manga_category_id, manga_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
			'mangaCategory' => array(self::BELONGS_TO, 'MangaCategory', 'manga_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'create_date' => 'Create Date',
			'manga_category_id' => 'Manga Category',
			'manga_id' => 'Manga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('manga_category_id',$this->manga_category_id);
		$criteria->compare('manga_id',$this->manga_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}