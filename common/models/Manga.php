<?php

/**
 * This is the model class for table "manga".
 *
 * The followings are the available columns in table 'manga':
 * @property integer $id
 * @property string $display_name
 * @property string $display_name_ascii
 * @property string $tags
 * @property string $short_description
 * @property string $description
 * @property integer $view_count
 * @property integer $like_count
 * @property integer $dislike_count
 * @property double $rating
 * @property integer $rating_count
 * @property integer $comment_count
 * @property integer $image_count
 * @property integer $status
 * @property string $create_date
 * @property integer $type
 * @property integer $order_number
 * @property integer $uploader_id
 * @property integer $is_series
 * @property integer $chapter_count
 * @property string $other_name
 * @property integer $subscription_count
 * @property string $update_date
 * @property integer $content_status
 * @property string $other_translation
 * @property integer $author_id
 * @property integer $is_free
 * @property double $price
 * @property integer $manga_type
 *
 * The followings are the available model relations:
 * @property ApplicationVodAssetMapping[] $applicationVodAssetMappings
 * @property Author $author
 * @property ClientUser $uploader
 * @property MangaCategoryMapping[] $mangaCategoryMappings
 * @property MangaChapter[] $mangaChapters
 * @property MangaComment[] $mangaComments
 * @property MangaCover[] $mangaCovers
 * @property MangaLikeDislike[] $mangaLikeDislikes
 * @property MangaRating[] $mangaRatings
 * @property MangaRelatedLink[] $mangaRelatedLinks
 * @property MangaTranslatorMapping[] $mangaTranslatorMappings
 * @property MangaUserBookmark[] $mangaUserBookmarks
 * @property MangaUserSubscription[] $mangaUserSubscriptions
 * @property SubscriberContentAccessLog[] $subscriberContentAccessLogs
 */
class Manga extends CActiveRecord
{
	const CONTENT_FINISHED = 1;
	const CONTENT_PENDING = 2;
	const CONTENT_UPDATING = 3;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Manga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('display_name, create_date', 'required'),
			array('view_count, like_count, dislike_count, rating_count, comment_count, image_count, status, type, order_number, uploader_id, is_series, chapter_count, subscription_count, content_status, author_id, is_free, manga_type', 'numerical', 'integerOnly'=>true),
			array('rating, price', 'numerical'),
			array('display_name, display_name_ascii, other_name, other_translation', 'length', 'max'=>200),
			array('tags, short_description', 'length', 'max'=>500),
			array('description, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, display_name, display_name_ascii, tags, short_description, description, view_count, like_count, dislike_count, rating, rating_count, comment_count, image_count, status, create_date, type, order_number, uploader_id, is_series, chapter_count, other_name, subscription_count, update_date, content_status, other_translation, author_id, is_free, price, manga_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applicationVodAssetMappings' => array(self::HAS_MANY, 'ApplicationVodAssetMapping', 'vod_asset_id'),
			'author' => array(self::BELONGS_TO, 'Author', 'author_id'),
			'uploader' => array(self::BELONGS_TO, 'ClientUser', 'uploader_id'),
			'mangaCategoryMappings' => array(self::HAS_MANY, 'MangaCategoryMapping', 'manga_id'),
			'mangaChapters' => array(self::HAS_MANY, 'MangaChapter', 'manga_id'),
			'mangaComments' => array(self::HAS_MANY, 'MangaComment', 'manga_id'),
			'mangaCovers' => array(self::HAS_MANY, 'MangaCover', 'manga_id'),
			'mangaLikeDislikes' => array(self::HAS_MANY, 'MangaLikeDislike', 'manga_id'),
			'mangaRatings' => array(self::HAS_MANY, 'MangaRating', 'manga_id'),
			'mangaRelatedLinks' => array(self::HAS_MANY, 'MangaRelatedLink', 'manga_id'),
			'mangaTranslatorMappings' => array(self::HAS_MANY, 'MangaTranslatorMapping', 'manga_id'),
			'mangaUserBookmarks' => array(self::HAS_MANY, 'MangaUserBookmark', 'manga_id'),
			'mangaUserSubscriptions' => array(self::HAS_MANY, 'MangaUserSubscription', 'manga_id'),
			'subscriberContentAccessLogs' => array(self::HAS_MANY, 'SubscriberContentAccessLog', 'vod_asset_id'),
			'mangaCategories' => array(self::MANY_MANY, 'MangaCategory', 'manga_category_mapping(manga_id, manga_category_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'display_name' => 'Display Name',
			'display_name_ascii' => 'Display Name Ascii',
			'tags' => 'Tags',
			'short_description' => 'Short Description',
			'description' => 'Description',
			'view_count' => 'View Count',
			'like_count' => 'Like Count',
			'dislike_count' => 'Dislike Count',
			'rating' => 'Rating',
			'rating_count' => 'Rating Count',
			'comment_count' => 'Comment Count',
			'image_count' => 'Image Count',
			'status' => 'Status',
			'create_date' => 'Create Date',
			'type' => 'Type',
			'order_number' => 'Order Number',
			'uploader_id' => 'Uploader',
			'is_series' => 'Is Series',
			'chapter_count' => 'Chapter Count',
			'other_name' => 'Other Name',
			'subscription_count' => 'Subscription Count',
			'update_date' => 'Update Date',
			'content_status' => 'Content Status',
			'other_translation' => 'Other Translation',
			'author_id' => 'Author',
			'is_free' => 'Is Free',
			'price' => 'Price',
			'manga_type' => 'Manga Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('display_name_ascii',$this->display_name_ascii,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('like_count',$this->like_count);
		$criteria->compare('dislike_count',$this->dislike_count);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('rating_count',$this->rating_count);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('image_count',$this->image_count);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('order_number',$this->order_number);
		$criteria->compare('uploader_id',$this->uploader_id);
		$criteria->compare('is_series',$this->is_series);
		$criteria->compare('chapter_count',$this->chapter_count);
		$criteria->compare('other_name',$this->other_name,true);
		$criteria->compare('subscription_count',$this->subscription_count);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('content_status',$this->content_status);
		$criteria->compare('other_translation',$this->other_translation,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('is_free',$this->is_free);
		$criteria->compare('price',$this->price);
		$criteria->compare('manga_type',$this->manga_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function findManga($category_id = -1, $db_order, $page, $page_size, $keyword, $clientUserId = -1) {
		$where = "(t.status = 1)";
		if($category_id == -1) {
			$category_id = NULL;
		}
		else if($category_id == -2) {
			if($clientUserId != -1) {
				$where .= " AND id in (select manga_id from manga_user_subscription where user_id = $clientUserId and sub_type = 2)";
			}
		}
		else {
			$where .= " AND id in (select manga_id from  manga_category_mapping where manga_category_id = $category_id) ";
		}
		if (!empty ($keyword)) {
			$where .= "AND (t.display_name_ascii LIKE '%$keyword%' OR t.other_name LIKE '%$keyword%')";
		}
		
		//TODO: co the dung ActiveRecord o day ko??
		
		$limit = $page * $page_size;
		$result = Yii::app()->db->createCommand()
		->selectDistinct('t.id,t.create_date,t.view_count,t.like_count,t.dislike_count,t.rating,t.rating_count,t.comment_count,t.display_name, t.author_id, t.short_description, t.description, t.chapter_count, t.update_date, t.content_status')
		->from('manga as t')
		->where($where)
		->limit($page_size, $limit)
		->order($db_order)
		->queryAll();
		$total_result = Yii::app()->db->createCommand()
		->select('count(id)')
		->from('manga as t')
		->where($where)
		->queryScalar();
		//echo count($result);
		//CVarDumper::dump($result);
		
		//Yii::app()->end();
		
		return array(
				'total_result'=>$total_result,
				'keyword'=>$keyword,
				'page_number'=>$page,
				'page_size'=>$page_size,
				'total_page'=>($total_result - ($total_result % $page_size))/$page_size + (($total_result % $page_size ===0)?0:1),
				'data'=>$result
		);
	}
	
	public function getRelatedManga($order, $page_no, $page_size) {
        $where = '(status = 1) ';
        if ($this->id != null) {
			$where .= ' AND id !=' . $this->id . ' ';
        }
        
//        if (!empty ($keyword)) {
//            $where .= "AND (t.display_name_ascii LIKE '%$keyword%' OR t.tags_ascii LIKE '%$keyword%') ";
//        }
        
        $catids = '';
        foreach ($this->mangaCategories as $cat) {
            $catids .= $cat->id . ",";
        }
        if (!empty($catids)) {
            $catids = substr($catids, 0, strlen($catids)-1);
            $where .= " AND id in (select manga_id from  manga_category_mapping where manga_category_id in (" .
                    "$catids)) ";
        }


        //TODO: co the dung ActiveRecord o day ko??
        
        $result = Yii::app()->db->createCommand()
                ->selectDistinct('t.id,t.create_date,t.short_description,t.description,t.view_count,t.like_count,t.dislike_count,t.rating,t.rating_count,t.comment_count,t.subscription_count,t.display_name,t.is_free, t.chapter_count, t.content_status')
                ->from('manga t')
                ->where($where) 
                ->limit($page_size, $page_no * $page_size)
                ->order($order)
                ->queryAll();
        $total_result = Yii::app()->db->createCommand()
                ->select('count(id)')
                ->from('manga as t')
                ->where($where) 
                ->queryScalar();
        //echo count($result);
        //CVarDumper::dump($result);
        
        //Yii::app()->end();
        
        return array(
            'total_result'=>$total_result,
            'page_number'=>$page_no,
            'page_size'=>$page_size,
            'manga'=>$this->id,
            'total_page'=>($total_result - ($total_result % $page_size))/$page_size + (($total_result % $page_size ===0)?0:1),
            'data'=>$result
        );
    }
}