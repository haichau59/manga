<?php

/**
 * This is the model class for table "manga_page".
 *
 * The followings are the available columns in table 'manga_page':
 * @property integer $id
 * @property integer $manga_chapter_id
 * @property integer $width
 * @property integer $height
 * @property integer $file_size
 * @property string $create_date
 * @property integer $status
 * @property integer $page_number
 * @property integer $type
 * @property string $page_content
 * @property string $image_url3
 * @property string $image_url2
 * @property string $image_url1
 * @property string $f_id
 * @property integer $f_farm_id
 * @property integer $f_server_id
 * @property string $f_secret
 * @property integer $backup_status
 *
 * The followings are the available model relations:
 * @property MangaChapter $mangaChapter
 */
class MangaPage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manga_chapter_id', 'required'),
			array('manga_chapter_id, width, height, file_size, status, page_number, type, f_farm_id, f_server_id, backup_status', 'numerical', 'integerOnly'=>true),
			array('image_url3, image_url2, image_url1', 'length', 'max'=>500),
			array('f_id, f_secret', 'length', 'max'=>20),
			array('create_date, page_content', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, manga_chapter_id, width, height, file_size, create_date, status, page_number, type, page_content, image_url3, image_url2, image_url1, f_id, f_farm_id, f_server_id, f_secret, backup_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mangaChapter' => array(self::BELONGS_TO, 'MangaChapter', 'manga_chapter_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'manga_chapter_id' => 'Manga Chapter',
			'width' => 'Width',
			'height' => 'Height',
			'file_size' => 'File Size',
			'create_date' => 'Create Date',
			'status' => 'Status',
			'page_number' => 'Page Number',
			'type' => 'Type',
			'page_content' => 'Page Content',
			'image_url3' => 'Image Url3',
			'image_url2' => 'Image Url2',
			'image_url1' => 'Image Url1',
			'f_id' => 'F',
			'f_farm_id' => 'F Farm',
			'f_server_id' => 'F Server',
			'f_secret' => 'F Secret',
			'backup_status' => 'Backup Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('manga_chapter_id',$this->manga_chapter_id);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('file_size',$this->file_size);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('page_number',$this->page_number);
		$criteria->compare('type',$this->type);
		$criteria->compare('page_content',$this->page_content,true);
		$criteria->compare('image_url3',$this->image_url3,true);
		$criteria->compare('image_url2',$this->image_url2,true);
		$criteria->compare('image_url1',$this->image_url1,true);
		$criteria->compare('f_id',$this->f_id,true);
		$criteria->compare('f_farm_id',$this->f_farm_id);
		$criteria->compare('f_server_id',$this->f_server_id);
		$criteria->compare('f_secret',$this->f_secret,true);
		$criteria->compare('backup_status',$this->backup_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function url_exists($url) {
		if (!$fp = curl_init($url)) return false;
		return true;
	}

	function url_exists1($url=NULL)
	{
		if($url == NULL) return false;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch); 
		if($httpcode>=200 && $httpcode<300){
			return true;
		} else {
			return false;
		}
	}

	public function downloadToServer() {
		if((strpos($this->image_url1, 'truyentranhso') > 0) || (strpos($this->image_url1, '.upanh.com') > 0)) {
			$this->backup_status = -2; //dead link
			$this->update();
			return 0;
		}
		try {
// 			if(!$this->url_exists($this->image_url1)) {
// 				echo " - ".$this->image_url1." is not exist";
// 				return 0;
// 			}
// 			file_put_contents("images/page_".$this->id, file_get_contents($this->image_url1));
//			$command = "wget -q -O -t 1 --timeout=5 images/page_".$this->id." ".$this->image_url1;
 			$command = "wget -q -t 1 --timeout=5 -O /data/videos/images/page_".$this->id." ".$this->image_url1;
			exec($command);
			$this->image_url3 = "page_".$this->id;
			$this->backup_status = 1;
			if(!$this->update()) {
				print_r($this->getErrors());
				return 0;
			}
			return 1;
		}
		catch (Exception $e) {
			return 0;
		}
	}
	
	public function uploadToFlickr($uploader) {
		try {
			$imageUrl = '/data/videos/images/'.$this->image_url3;
			if(!file_exists($imageUrl)) return 0;
			if(filesize($imageUrl) < 10) {
				echo "size of file $imageUrl is 0";
				return 0;
			}
			// 		echo " - imageUrlOnServer : ". $imageUrl;
			$id = $uploader->upload($imageUrl, $this->id, '', '');
			// 		echo " - photoid = $id";
			$this->f_id = $id;
			$this->backup_status = 2;
			if(!$this->update()) {
				print_r($this->getErrors());
				return 0;
			}
			return 1;
		}
		catch(Exception $e) {
			return 0;
		}
	}
	
	/**
	 * $sizeStr :
	 *
	 s	small square 75x75
	 q	large square 150x150
	 t	thumbnail, 100 on longest side
	 m	small, 240 on longest side
	 n	small, 320 on longest side
	 -	medium, 500 on longest side
	 z	medium 640, 640 on longest side
	 c	medium 800, 800 on longest side†
	 b	large, 1024 on longest side*
	 o	original image, either a jpg, gif or png, depending on source format
	
	 * $type : jpg, png
	 */
	public function getFlickrLink($sizeStr = 'm', $type = 'jpg') {
// 		http://farm3.staticflickr.com/2829/9056418633_6ddcf4c8de.jpg
		$url = sprintf("http://farm%d.staticflickr.com/%d/%s_%s_%s.%s",
				$this->f_farm_id, $this->f_server_id, $this->f_id, $this->f_secret, $sizeStr, $type);
		return $url;
	}
	
	public function fix() {
		$imageUrl = '/data/videos/images/'.$this->image_url3;
		if(!file_exists($imageUrl)) return 0;
		if(filesize($imageUrl) < 10) {
			$this->backup_status = 0;
			$this->update();
			return 1;
		}
		return 0;
	}

	public function checkLink() {
		$file_headers = @get_headers($this->image_url1);
		if(strpos($file_headers[0],'200') > 0) { //good link
			if($this->status == 3) { //neu dang o trang thai link hong -> set lai = 1 
				$this->status = 1;
				$this->update();
				return 1;
			}
		}
		else {
			if($this->status == 1) { //neu dang o trang thai link ok -> set lai = 3
				$this->status = 3;
				$this->image_url3 = 'http://farm3.staticflickr.com/2892/9612816550_26c903b850_m.jpg';
				$this->update();
				return -1;
			}
		}
		return 0;
	}
}
