<?php

/**
 * This is the model class for table "manga_user_bookmark".
 *
 * The followings are the available columns in table 'manga_user_bookmark':
 * @property integer $id
 * @property string $create_date
 * @property integer $user_id
 * @property integer $manga_page_id
 * @property integer $manga_chapter_id
 * @property integer $manga_id
 *
 * The followings are the available model relations:
 * @property Manga $manga
 * @property ClientUser $user
 * @property MangaChapter $mangaChapter
 */
class MangaUserBookmark extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaUserBookmark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_user_bookmark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_date, user_id', 'required'),
			array('user_id, manga_page_id, manga_chapter_id, manga_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_date, user_id, manga_page_id, manga_chapter_id, manga_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
			'user' => array(self::BELONGS_TO, 'ClientUser', 'user_id'),
			'mangaChapter' => array(self::BELONGS_TO, 'MangaChapter', 'manga_chapter_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_date' => 'Create Date',
			'user_id' => 'User',
			'manga_page_id' => 'Manga Page',
			'manga_chapter_id' => 'Manga Chapter',
			'manga_id' => 'Manga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('manga_page_id',$this->manga_page_id);
		$criteria->compare('manga_chapter_id',$this->manga_chapter_id);
		$criteria->compare('manga_id',$this->manga_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}