<?php

/**
 * This is the model class for table "content_token".
 *
 * The followings are the available columns in table 'content_token':
 * @property integer $id
 * @property string $token
 * @property string $content_hash
 * @property integer $subscriber_session_id
 * @property integer $subscriber_id
 * @property string $create_date
 * @property string $use_date
 * @property integer $status
 * @property string $content_type
 * @property integer $content_id
 * @property string $expiry_date
 *
 * The followings are the available model relations:
 * @property ClientUser $subscriber
 * @property SubscriberSession $subscriberSession
 */
class ContentToken extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ContentToken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'content_token';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('token', 'required'),
			array('subscriber_session_id, subscriber_id, status, content_id', 'numerical', 'integerOnly'=>true),
			array('token', 'length', 'max'=>100),
			array('content_hash, content_type', 'length', 'max'=>45),
			array('create_date, use_date, expiry_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, token, content_hash, subscriber_session_id, subscriber_id, create_date, use_date, status, content_type, content_id, expiry_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subscriber' => array(self::BELONGS_TO, 'ClientUser', 'subscriber_id'),
			'subscriberSession' => array(self::BELONGS_TO, 'SubscriberSession', 'subscriber_session_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'token' => 'Token',
			'content_hash' => 'Content Hash',
			'subscriber_session_id' => 'Subscriber Session',
			'subscriber_id' => 'Subscriber',
			'create_date' => 'Create Date',
			'use_date' => 'Use Date',
			'status' => 'Status',
			'content_type' => 'Content Type',
			'content_id' => 'Content',
			'expiry_date' => 'Expiry Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('content_hash',$this->content_hash,true);
		$criteria->compare('subscriber_session_id',$this->subscriber_session_id);
		$criteria->compare('subscriber_id',$this->subscriber_id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('use_date',$this->use_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('content_type',$this->content_type,true);
		$criteria->compare('content_id',$this->content_id);
		$criteria->compare('expiry_date',$this->expiry_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}