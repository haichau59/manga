<?php

/**
 * This is the model class for table "manga_chapter".
 *
 * The followings are the available columns in table 'manga_chapter':
 * @property integer $id
 * @property string $code_name
 * @property string $display_name
 * @property string $tags
 * @property string $short_description
 * @property string $description
 * @property integer $chapter_order
 * @property string $create_date
 * @property string $modify_date
 * @property integer $view_count
 * @property integer $like_count
 * @property integer $dislike_count
 * @property integer $comment_count
 * @property integer $page_count
 * @property double $rating
 * @property integer $rating_count
 * @property integer $status
 * @property integer $manga_id
 * @property integer $uploader_id
 * @property string $download_url1
 * @property string $download_url2
 * @property string $download_url3
 *
 * The followings are the available model relations:
 * @property Manga $manga
 * @property ClientUser $uploader
 * @property MangaComment[] $mangaComments
 * @property MangaPage[] $mangaPages
 * @property MangaUserBookmark[] $mangaUserBookmarks
 */
class MangaChapter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaChapter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_chapter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code_name, manga_id', 'required'),
			array('chapter_order, view_count, like_count, dislike_count, comment_count, page_count, rating_count, status, manga_id, uploader_id', 'numerical', 'integerOnly'=>true),
			array('rating', 'numerical'),
			array('code_name, display_name', 'length', 'max'=>200),
			array('tags', 'length', 'max'=>300),
			array('short_description, download_url1, download_url2, download_url3', 'length', 'max'=>500),
			array('description, create_date, modify_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, code_name, display_name, tags, short_description, description, chapter_order, create_date, modify_date, view_count, like_count, dislike_count, comment_count, page_count, rating, rating_count, status, manga_id, uploader_id, download_url1, download_url2, download_url3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
			'uploader' => array(self::BELONGS_TO, 'ClientUser', 'uploader_id'),
			'mangaComments' => array(self::HAS_MANY, 'MangaComment', 'manga_chapter_id'),
			'mangaPages' => array(self::HAS_MANY, 'MangaPage', 'manga_chapter_id'),
			'mangaUserBookmarks' => array(self::HAS_MANY, 'MangaUserBookmark', 'manga_chapter_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code_name' => 'Code Name',
			'display_name' => 'Display Name',
			'tags' => 'Tags',
			'short_description' => 'Short Description',
			'description' => 'Description',
			'chapter_order' => 'Chapter Order',
			'create_date' => 'Create Date',
			'modify_date' => 'Modify Date',
			'view_count' => 'View Count',
			'like_count' => 'Like Count',
			'dislike_count' => 'Dislike Count',
			'comment_count' => 'Comment Count',
			'page_count' => 'Page Count',
			'rating' => 'Rating',
			'rating_count' => 'Rating Count',
			'status' => 'Status',
			'manga_id' => 'Manga',
			'uploader_id' => 'Uploader',
			'download_url1' => 'Download Url1',
			'download_url2' => 'Download Url2',
			'download_url3' => 'Download Url3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code_name',$this->code_name,true);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('chapter_order',$this->chapter_order);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('modify_date',$this->modify_date,true);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('like_count',$this->like_count);
		$criteria->compare('dislike_count',$this->dislike_count);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('page_count',$this->page_count);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('rating_count',$this->rating_count);
		$criteria->compare('status',$this->status);
		$criteria->compare('manga_id',$this->manga_id);
		$criteria->compare('uploader_id',$this->uploader_id);
		$criteria->compare('download_url1',$this->download_url1,true);
		$criteria->compare('download_url2',$this->download_url2,true);
		$criteria->compare('download_url3',$this->download_url3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}