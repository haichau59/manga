<?php

/**
 * This is the model class for table "manga_category".
 *
 * The followings are the available columns in table 'manga_category':
 * @property integer $id
 * @property string $code_name
 * @property string $display_name
 * @property string $description
 * @property integer $status
 * @property integer $order_number
 * @property integer $parent_id
 * @property string $path
 * @property integer $level
 * @property integer $child_count
 * @property string $image_url1
 * @property string $image_url2
 * @property string $create_date
 *
 * The followings are the available model relations:
 * @property ApplicationMangaCategoryMapping[] $applicationMangaCategoryMappings
 * @property MangaCategory $parent
 * @property MangaCategory[] $mangaCategories
 * @property MangaCategoryMapping[] $mangaCategoryMappings
 * @property MangaSearchHistory[] $mangaSearchHistories
 */
class MangaCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code_name, display_name, create_date', 'required'),
			array('status, order_number, parent_id, level, child_count', 'numerical', 'integerOnly'=>true),
			array('code_name, display_name, path', 'length', 'max'=>200),
			array('image_url1, image_url2', 'length', 'max'=>500),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, code_name, display_name, description, status, order_number, parent_id, path, level, child_count, image_url1, image_url2, create_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applicationMangaCategoryMappings' => array(self::HAS_MANY, 'ApplicationMangaCategoryMapping', 'manga_category_id'),
			'parent' => array(self::BELONGS_TO, 'MangaCategory', 'parent_id'),
			'mangaCategories' => array(self::HAS_MANY, 'MangaCategory', 'parent_id'),
			'mangaCategoryMappings' => array(self::HAS_MANY, 'MangaCategoryMapping', 'manga_category_id'),
			'mangaSearchHistories' => array(self::HAS_MANY, 'MangaSearchHistory', 'vod_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code_name' => 'Code Name',
			'display_name' => 'Display Name',
			'description' => 'Description',
			'status' => 'Status',
			'order_number' => 'Order Number',
			'parent_id' => 'Parent',
			'path' => 'Path',
			'level' => 'Level',
			'child_count' => 'Child Count',
			'image_url1' => 'Image Url1',
			'image_url2' => 'Image Url2',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code_name',$this->code_name,true);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('order_number',$this->order_number);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('child_count',$this->child_count);
		$criteria->compare('image_url1',$this->image_url1,true);
		$criteria->compare('image_url2',$this->image_url2,true);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * lay tat ca cac categories con cua category co id = $_cat_id
	 * @param boolean $_recursive = false neu chi tim trong 1 level tiep theo, = true nếu goi de quy
	 * @param integer $_cat_id, = null neu la toan bo category tree
	 * @return type array
	 */
	public static function getSubCategories($_cat_id=null, $_recursive=false) {
		$res = array();
		if (($_cat_id)) {
			$model = MangaCategory::model()->findByPk($_cat_id);
			/* @var $model MangaCategory */
			if ($model === null)
				throw new CHttpException(404, "The requested Manga Category (#$_cat_id) does not exist.");
	
			//Thuc: them 'order'=>'order_number ASC' trong ham relations
			$chidren = $model->mangaCategories;
			if ($chidren) {
				foreach ($chidren as $child) {
					if($child->status!=1) {
						continue;
					}
					/* @var $child MangaCategory */
					for ($i = 0; $i < $child->level; $i++) {
						$child->path .= "|___ ";
					}
					$child->path .= $child->display_name;
					$res = array_merge($res, array($child));
					if ($_recursive) {
						$res = array_merge($res, MangaCategory::getSubCategories($child->id,$_recursive));
					}
				}
			}
		}
		else {
			$_root_cats = MangaCategory::model()->findAllByAttributes(array("level"=>0),array('order'=>'order_number ASC'));
			if (!is_null($_root_cats)) {
				foreach ($_root_cats as $_root_cat) {
					/* @var $_root_cat MangaCategory */
					if($_root_cat->status!=1) {
						continue;
					}
					$_root_cat->path = $_root_cat->display_name;
					$res = array_merge($res, array($_root_cat));
					if ($_recursive) {
						$res = array_merge($res, MangaCategory::getSubCategories($_root_cat->id,$_recursive));
					}
				}
			}
		}
	
		return $res;
	}
}