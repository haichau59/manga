<?php

/**
 * This is the model class for table "manga_comment".
 *
 * The followings are the available columns in table 'manga_comment':
 * @property integer $id
 * @property string $title
 * @property string $comment
 * @property string $create_date
 * @property integer $status
 * @property integer $manga_id
 * @property integer $manga_chapter_id
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property Manga $manga
 * @property MangaChapter $mangaChapter
 * @property ClientUser $user
 */
class MangaComment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MangaComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manga_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment, create_date', 'required'),
			array('status, manga_id, manga_chapter_id, user_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, comment, create_date, status, manga_id, manga_chapter_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manga' => array(self::BELONGS_TO, 'Manga', 'manga_id'),
			'mangaChapter' => array(self::BELONGS_TO, 'MangaChapter', 'manga_chapter_id'),
			'user' => array(self::BELONGS_TO, 'ClientUser', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'comment' => 'Comment',
			'create_date' => 'Create Date',
			'status' => 'Status',
			'manga_id' => 'Manga',
			'manga_chapter_id' => 'Manga Chapter',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('manga_id',$this->manga_id);
		$criteria->compare('manga_chapter_id',$this->manga_chapter_id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}