    <?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
        public static $ACCESS_VIA_WIFI = 1;
        public static $ACCESS_VIA_3G = 2;
        
	public $breadcrumbs=array();
	public $detect;
        public $array_file = array();
	public $title="";
	
        protected $page_id = 'home_page';

        public $categories;
	public $msisdn;
	public $subscriber;
	public $services;
	public $usingServices;
	public $isHome = false;
        public $inAccount = false;
        public $accessType;
	public $user_session;	
	protected $crypt;
	
	protected $purchaseResultDescription = array(
			CPS_OK                           => "Lệnh thực hiện thành công",
			NOK_NO_MORE_CREDIT_AVAILABLE     => "Tài khoản không đủ để thanh toán",
			NOK_BAD_CREDIT_VALUE             => "Giá trị tài khoản không hợp lệ",
			NOK_BAD_PROFILE                  => "Profile không hợp lệ",
			NOK_BAD_ACCOUNT_STATUS           => "Trạng thái thuê bao không hợp lệ",
			NOK_EMPTY_VALUE                  => "Tham số không được để trống",
			NOK_IN_TIMED_OUT                 => "Thực hiện lệnh timeout trên hệ thống IN",
			NOK_ACCOUNT_NOT_EXISTED          => "Thuê bao không tồn tại",
			NOK_COMMAND_NOT_EXISTED          => "Lệnh không tồn tại",
			NOK_DENIED_CREDIT_VALUE          => "Giá trị tài khoản không được phép",
			NOK_DENIED_PROFILE               => "Profile không được phép sử dụng",
			NOK_IN_COMMAND_NOT_AVAILABLE     => "Lệnh không được hỗ trợ trên IN",
			NOK_BUNDLE_NOT_EXISTED           => "Tài khoản không tồn tại",
			NOK_BAD_SYNTAX                   => "Cấu trúc lệnh không đúng",
			NOK_MISSING_PARAMETER            => "Tham số lệnh thiếu",
			NOK_NO_COMMAND_PERMISSION        => "User không có quyền thực hiện lênh",
			NOK_NO_IN_COMMAND_PERMISION      => "User không có quyền thực hiện lệnh trên IN",
			NOK_NO_USER_PERMISSION           => "User không có quyền truy nhập hệ thống",
			NOK_CPS_TIMED_OUT                => "Thực hiện lệnh timeout trên Charging Proxy",
			NOK_NO_IN_AVAILABLE              => "Kết nối đến hệ thống IN không sẵn sàng",
			NOK_USER_DOES_NOT_EXISTED        => "User không tồn tại",
			NOK_BAD_PASSWORD                 => "Mật khẩu không đúng",
			NOK_EXPIRED_PASSWORD             => "Mật khẩu đã hết hạn sử dụng",
			NOK_BAD_IPADDRESS                => "IP truy nhập không hợp lệ",
			NOK_NO_USER_PERMISSION_AT_MOMENT => "User không được phép truy nhập tại thời điểm này",
			NOK_INVALID_SESSION_ID           => "Mã phiên giao dịch không hợp lệ",
			NOK_LOGIN_DENIED_BY_CPS          => "Không được phép truy nhập vào hệ thống CPS",
			NOK_MESSAGE_TIMED_OUT            => "Message timed out ",
			NOK_NO_CPS_AVAILABLE             => "Hệ thống CPS chưa sẵn sàng",
			NOK_EXCEEDED_MAX_CONNECTION      => "Số lượng kết nối đã vượt qua số kết nối cho phép",
			NOK_UNDEFINED_ERROR              => "Lỗi không xác định",
			NOK_IN_ERROR                     => "Lỗi từ hệ thống IN",
			NOK_CPS_ERROR                    => "Lỗi từ hệ thống Charging Proxy",
	
			NOK_CONNECTION_ERROR             => "Không thể kết nối đến Charging Proxy",
			NOK_XML_ERROR                    => "Không phân tích được tài liệu XML trả về từ Charging Proxy",
	);
	
	public function __construct($id, $module) {
		parent::__construct($id, $module);
		$ipRanges = Yii::app()->params['ipRanges'];
		$clientIP = Yii::app()->request->getUserHostAddress();
                $found = false;
                
                //Dectect access via wifi or 3G
                $headers = getallheaders();
                $this->msisdn = '';
                if(isset($headers['MSISDN'])){
                	$this->msisdn = $headers['MSISDN'];
                }else if (isset($headers['msisdn'])){
                	$this->msisdn = $headers['msisdn'];
                }else if (isset($headers['X-Wap-MSISDN'])){
                	$this->msisdn = $headers['X-Wap-MSISDN'];
                }else if (isset($headers['X-WAP-MSISDN'])){
                	$this->msisdn = $headers['X-WAP-MSISDN'];
                }
                  //Check wifi
                if($this->msisdn == ''){
                    $this->accessType = self::$ACCESS_VIA_WIFI;
                    $this->user_session = Yii::app()->user->getState('session');
                    $this->msisdn = (Yii::app()->user->getId() != NULL)?Yii::app()->user->getId():''; 
                    //Verify account with session id
                    $subcriber_id = Subscriber::model()->findByAttributes(array('subscriber_number' => $this->msisdn));
                    if($subcriber_id != NULL && $this->user_session != NULL){                    
                        $subSession = SubscriberSession::model()->findByAttributes(array('session_id' => $this->user_session, 'subscriber_id' => $subcriber_id->id, 'status' => 1));
                        if($subSession != NULL){
                            $found = true;
                        }else{
                            $this->msisdn = '';
                        }
                    }else{
                        $this->msisdn = '';
                    }
                }else{
                    $this->accessType = self::$ACCESS_VIA_3G;
                    foreach ($ipRanges as $range) {
                            if (CUtils::cidrMatch($clientIP, $range)) {
                                    $found = true;
                                    break;
                            }
                    }
                    if (!$found) {
                            $this->msisdn =  '';
                    }
                }
		$this->subscriber = $this->msisdn == '' ? null : Subscriber::model()->findByAttributes(array('subscriber_number' => $this->msisdn));
		if ($this->subscriber == null) {
			$this->subscriber = Subscriber::newSubscriber($this->msisdn);
		}
		
		$this->categories = VodCategory::getSubCategories();
		$this->services = Service::model()->findAllByAttributes(array('is_deleted' => 0));
		$this->usingServices = $this->subscriber != null ?
			ServiceSubscriberMapping::model()->findAllByAttributes(array('subscriber_id' => $this->subscriber->id, 'is_active' => 1)) :
			null;
		
		$info = CUtils::getDeviceInfo();
                
		$this->detect = Yii::app()->mobileDetect;
		if ($this->detect->isMobile() || $this->detect->isTablet()) {
			if ($this->detect->is('AndroidOS')) {
				if ($this->detect->version('Android') < 3.0) {
					Yii::app()->theme = 'basic';
				} else {
					Yii::app()->theme = 'advance';
				}
			} else if ($this->detect->is('iOS')) {
				if ($this->detect->getIOSGrade() === 'B') {
					Yii::app()->theme = 'basic';
				} else {
					Yii::app()->theme = 'advance';
				}
			} else {
                                if($this->detect->mobileGrade() === 'A'){
                                    Yii::app()->theme = 'basic';
                                }else{
                                    Yii::app()->theme = 'basic';
                                }
			}
		} else {
			Yii::app()->theme = 'basic';
		}
		
		// check saved msisdn - ip address
		$session = new CHttpSession();
		$session->open();
		$saveSession = false;
		if (!isset($session['subscriber']) || $session['subscriber'] != $this->msisdn) {
			$saveSession = true;
			$session['subscriber'] = $this->msisdn;
			$session->close();
		}
		
		if ($saveSession && $found) {
			$aDate = new DateTime();
			$accessLog = new MsisdnIp();
			$accessLog->request_time = $aDate->format('Y-m-d H:i:s');
			$accessLog->msisdn = $this->msisdn;
			if ($this->subscriber != null)
				$accessLog->subscriber_id = $this->subscriber->id;
			$accessLog->ip = Yii::app()->request->getUserHostAddress();
			$accessLog->save();
		}
		
		// crypt object
		$cryptOptions = Yii::app()->params['cryptOptions'];
		$cryptOptions['key'] = $this->msisdn . "_" . Yii::app()->request->getUserHostAddress();
		$this->crypt = new Crypt($cryptOptions);
	}
	
	public function render($view, $data = null, $return = false) {
		if ($data == null) $data = array();
		$data['categories'] = $this->categories;
		return parent::render($view, $data, $return);
	}
}
