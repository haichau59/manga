<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
    
        public static $ERROR_LOGIN = 100;
        public $session_id;


        /*
         * $typeAuth
         * 1: Authentication via Mobile Number
         * 2: Authentication via Email
         */
        public function authenticate($typeAuth = 1)
		{
            
            $apiRequest = new APIService();
            $response = $apiRequest->login($this->username, $this->password, $typeAuth);
            if($response == NULL){
                $this->errorCode=self::$ERROR_LOGIN;
                $this->errorMessage = 'Có lỗi xảy ra trong quá trình đăng nhập, xin vui lòng thử lại';
            }else{
                if($response->error_no > 0){
                    $this->errorCode=self::$ERROR_LOGIN;
                    $this->errorMessage = $response->error_message;
                }else{
                    $this->errorCode=self::ERROR_NONE;
                    $this->errorMessage = $response->error_message;
                    $this->session_id = $response->session;
                }
            }
            
            return $this->errorCode;
	}
}